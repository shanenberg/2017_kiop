/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package tests;

import junit.framework.TestCase;
import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.parser.LambdaParsing;
import lambdaST.parser.LambdaParsing.LambdaParser;
import lambdaST.terms.LTerm;
import lambdaST.types.Type;
import slope.Parsing;
import slope.ResultObject;

public class LangTests extends TestCase {

	public LTerm run(String s) {
		LambdaParsing tokens = new LambdaParsing();

		ResultObject<LTerm> result = new ResultObject<LTerm>();
		
		Parsing parsing = new Parsing(tokens, s); 
		parsing.parseWithParseTreeConstruction(tokens.new LambdaParser().Term(result.setter));

		Type t = result.result.typeOf(new Environment());
		
		return result.result.reduceAll();
		
	}
	
	public void test06Seq() {
		String program = 
			"(1;(2;3))";
		
		LTerm result = run(program);
		assertEquals("3", result.toString());
	}	
	
	
	public void test05Case() {
		String program = 
			"(case (inl(NUM+BOOL) 1) inl(NUM+BOOL) y => true inr(NUM+BOOL) z => false)";
		
		LTerm result = run(program);
		assertEquals("true", result.toString());
	}	
	
	public void test04Record() {
		String program = 
			"({x=1, y=2, z=42}.x)";
		
		LTerm result = run(program);
		assertEquals("1", result.toString());
	}	
	
	public void test03FixSumme() {
		String program = 
			"(fix (                                " +
			"  (  λf:(NUM->NUM).                   " +
			"   (  λn:NUM.                         " +
			"        (if ((= 1) n) then 1 else ((+ n) (f ((- n) 1))))  " +
			")                                     " +
			")                                     " +
			") 3)                                     ";
		
		LTerm result = run(program);
		assertEquals("6", result.toString());
	}		
	
	public void test02IfTrue() {
		String program = "(if true then ((+ 1) 2) else ((+ 2) 3))";
		
		LTerm result = run(program);
		assertEquals("3", result.toString());
	}
	
	public void test01Plus() {
		String program = "((+ 1) 2)";
		
		LTerm result = run(program);
		assertEquals("3", result.toString());
	}
}
