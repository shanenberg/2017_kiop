/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package tests;

import java.util.HashMap;

import junit.framework.TestCase;
import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.terms.Abstraction;
import lambdaST.terms.Application;
import lambdaST.terms.BoolTerm;
import lambdaST.terms.Case;
import lambdaST.terms.Fix;
import lambdaST.terms.IfTerm;
import lambdaST.terms.InL;
import lambdaST.terms.LTerm;
import lambdaST.terms.NumTerm;
import lambdaST.terms.Variable;
import lambdaST.terms.buildInFunctions.AND;
import lambdaST.terms.buildInFunctions.EQNUM;
import lambdaST.terms.buildInFunctions.MINUS;
import lambdaST.terms.buildInFunctions.PLUS;
import lambdaST.terms.buildInFunctions.SUCC;
import lambdaST.types.Bool;
import lambdaST.types.FunctionType;
import lambdaST.types.Num;
import lambdaST.types.SumType;
import lambdaST.types.Type;

public class SimpleTests extends TestCase {

	static final Fix fix = new Fix(
			new Abstraction(
					"f",
					new FunctionType(new Num(), new Num()),
					new Abstraction(
						"x",
						new Num(),
						new IfTerm(
							new Application(
								new Application(
									new EQNUM(),
									new Variable("x")),
								new NumTerm(1)
								),
							new NumTerm(1), 
							new Application(
								new Application(
										new PLUS(),
										new Variable("x")
								),
								new Application(
									new Variable("f"),
									new Application(
										new Application(
											new MINUS(),
											new Variable("x")), 
										new NumTerm(1))
								)))))
				);

	
	public void test_ÜbungSummeEvaluation05() {
		
		Type retType = fix.typeOf(Environment.empty());
		assertEquals(
			new FunctionType(new Num(), new Num()),
			retType);
		
		Application app = new Application(
			fix,
			new NumTerm(100));
		
		LTerm t = app.reduceAll();
		
		assertEquals(new NumTerm(5050), t);
		
	}	
	
	public void test_ÜbungSummeEvaluation04() {
		
		Type retType = fix.typeOf(Environment.empty());
		assertEquals(
			new FunctionType(new Num(), new Num()),
			retType);
		
		Application app = new Application(
			fix,
			new NumTerm(10));
		
		LTerm t = app.reduceAll();
		
		assertEquals(new NumTerm(55), t);
		
	}		
	
	public void test_ÜbungSummeEvaluation03() {
		
		Type retType = fix.typeOf(Environment.empty());
		assertEquals(
			new FunctionType(new Num(), new Num()),
			retType);
		
		Application app = new Application(
			fix,
			new NumTerm(3));
		
		LTerm t = app.reduceAll();
		
		assertEquals(new NumTerm(6), t);
		
	}			
	
	public void test_ÜbungSummeEvaluation02() {
		
		Type retType = fix.typeOf(Environment.empty());
		assertEquals(
			new FunctionType(new Num(), new Num()),
			retType);
		
		Application app = new Application(
			fix,
			new NumTerm(2));
		
		LTerm t = app.reduceAll();
		
		assertEquals(new NumTerm(3), t);
		
	}		

	public void test_ÜbungSummeEvaluation01() {
		
		Type retType = fix.typeOf(Environment.empty());
		assertEquals(
			new FunctionType(new Num(), new Num()),
			retType);
		
		Application app = new Application(
			fix,
			new NumTerm(1));
		
		LTerm t = app.reduceAll();
		
		assertEquals(new NumTerm(1), t);
		
	}		
	
	
	public void test_ÜbungSummeType() {
		
		Type retType = fix.typeOf(Environment.empty());
		assertEquals(
			new FunctionType(new Num(), new Num()),
			retType);
		
	}		
	
	
	
	public void test_EQNUM_02() {
		Application app = new Application(
				new Application(
					new EQNUM(),
					new NumTerm(3)),
				new NumTerm(3));
		
		assertEquals(new BoolTerm(true), app.reduce(new Storage()).reduce(new Storage()));
		
	}		

	
	public void test_EQNUM_01() {
		Application app = new Application(
				new Application(
					new EQNUM(),
					new NumTerm(3)),
				new NumTerm(2));
		
		assertEquals(new BoolTerm(false), app.reduce(new Storage()).reduce(new Storage()));
		
	}		
	
	public void test_MINUS() {
		Application app = new Application(
				new Application(
					new MINUS(),
					new NumTerm(3)),
				new NumTerm(2));
		
		assertEquals(1, ((NumTerm) app.reduce(new Storage()).reduce(new Storage())).value);
		
	}	
	
	public void test_add() {
		Application app = new Application(
				new Application(
					new PLUS(),
					new NumTerm(1)),
				new NumTerm(2));
		
		assertEquals(3, ((NumTerm) app.reduce(new Storage()).reduce(new Storage())).value);
		
	}
	
	
	// fix(λf:N->N.λx:N. succ f(x))
	
	public void test_recursion01() {
		Fix f = new Fix(
			new Abstraction(
				"f", 
				new FunctionType(new Num(), new Num()),
				new Abstraction(
						"x",
						new Num(),
						new Application(
								new SUCC(),
								new Application(
										new Variable("f"),
										new Variable("x")))
						)
				));

		assertEquals(new FunctionType(new Num(), new Num()),
				f.typeOf(Environment.empty()));
		
		assertEquals(Abstraction.class, f.reduce(new Storage()).getClass());
		Abstraction abs = (Abstraction) f.reduce(new Storage());
		
		assertEquals("x", abs.paramName);
		
		
	}
	
	
	// (and true) (if (false) then 1 else false): X
	// (and Bool) (Num+Bool) => Fehler
	public void test_iFTermWithSumReturnType02() {
		IfTerm ifterm = new IfTerm(
				new BoolTerm(false), new NumTerm(1), new BoolTerm(false));
		AND  and = new AND();
		Application app = new Application(and, new BoolTerm(true));
		Application app2 = new Application(app, ifterm);
		
		try {
			app2.typeOf(Environment.empty());
			assertTrue(false);
		} catch (Exception ex) {
			assertTrue(true);
		}
		
	}
	
	public void test_case01() {
		Case c = new Case(
				new Variable("x"), 
				new SumType(new Num(), new Bool()),
				"y", new NumTerm(42),
				new SumType(new Num(), new Bool()),
				"z", new NumTerm(23));

		Environment e = Environment.empty();
		e.addVarType("x", new SumType(new Num(), new Bool()));
		
		assertEquals(new Num(), c.typeOf(e));
		
		Abstraction abs = new Abstraction("x", new SumType(new Num(), new Bool()), c);
		Application app = new Application(abs, 
				new InL(new SumType(new Num(), new Bool()), new NumTerm(1)));
		
		
	}

	

	// if (true) then 1 else false; 
	public void test_iFTermWithSumReturnType01() {
		IfTerm ifterm = new IfTerm(
				new BoolTerm(true), new NumTerm(1), new BoolTerm(false));
		assertEquals(new SumType(new Num(), new Bool()), ifterm.typeOf(Environment.empty()));
	}
	
//	public void test_AND03() {
//		AND_InsKloGegriffen and = new AND_InsKloGegriffen(new BoolTerm(true), new BoolTerm(true));
//		BoolTerm reducedAnd = (BoolTerm) and.reduce();
//		 
//		assertEquals(new BoolTerm(true), reducedAnd);
//	}		
//	
//	public void test_AND02() {
//		AND_InsKloGegriffen and = new AND_InsKloGegriffen(new BoolTerm(false), new BoolTerm(false));
//		BoolTerm reducedAnd = (BoolTerm) and.reduce();
//		 
//		assertEquals(new BoolTerm(false), reducedAnd);
//	}		
//	
//	public void test_AND01() {
//		AND_InsKloGegriffen and = new AND_InsKloGegriffen(new BoolTerm(true), new BoolTerm(false));
//		BoolTerm reducedAnd = (BoolTerm) and.reduce();
//		 
//		assertEquals(new BoolTerm(false), reducedAnd);
//	}	
	
	
	
	
	// (plus 3) ((plus 1) 2) -> 6 
	public void test_plus02() {
		Application app = new Application(new PLUS(), new NumTerm(1));
		Application app2 = new Application(app, new NumTerm(2));
		Application app3 = new Application(new PLUS(), new NumTerm(3));
		Application result = new Application(app3, app2);
		assertEquals(new NumTerm(6), result.reduce(new Storage()).reduce(new Storage()).reduce(new Storage()).reduce(new Storage()));
	}		
	
	// (plus 1) 2 -> 3 
	public void test_plus01() {
		Application app = new Application(new PLUS(), new NumTerm(1));
		Application app2 = new Application(app, new NumTerm(2));
		assertEquals(new NumTerm(3), app2.reduce(new Storage()).reduce(new Storage()));
	}		

	// succ (succ 1) -> 3 
	public void test_succ02() {
		Application app = new Application(new SUCC(), new NumTerm(1));
		Application app2 = new Application(new SUCC(), app);
		assertEquals(new NumTerm(3), app2.reduce(new Storage()).reduce(new Storage()));
	}	
	
	
	// succ 1 -> 2 
	public void test_succ01() {
		Application app = new Application(new SUCC(), new NumTerm(1));
		assertEquals(new NumTerm(2), app.reduce(new Storage()));
	}	
	
	
	// and true -> ~and true~ 
	public void test_and01() {
		Application app = new Application(new AND(), new BoolTerm(true));
		Abstraction newTerm = (Abstraction) app.reduce(new Storage());
		Application app2 = new Application(newTerm, new BoolTerm(true));
		assertEquals(new BoolTerm(true), app2.reduce(new Storage()));
	}
	
	// and true -> ~and true~ 
	public void test_and02() {
		Application app = new Application(new AND(), new BoolTerm(true));
		Abstraction newTerm = (Abstraction) app.reduce(new Storage());
		Application app2 = new Application(newTerm, new BoolTerm(false));
		assertEquals(new BoolTerm(false), app2.reduce(new Storage()));
	}	
	
	// and true -> ~and true~ 
	public void test_and03() {
		Application app = new Application(new AND(), new BoolTerm(false));
		Abstraction newTerm = (Abstraction) app.reduce(new Storage());
		Application app2 = new Application(newTerm, new BoolTerm(true));
		assertEquals(new BoolTerm(false), app2.reduce(new Storage()));
	}		
	
	// (x x) ((λx:Bool.x) true) -> ((x x) true) 
	public void test_reduceComplexApplication() {
		Application app1 = new Application(new Variable("x"), new Variable("x"));
		Abstraction abs = new Abstraction("x", new Bool(), new Variable("x"));
		BoolTerm boolTerm = new BoolTerm(true);
		Application app = new Application(abs, boolTerm);
		Application app2 = new Application(app1, app);

		assertEquals(
				new Application(
						new Application(
							new Variable("x"), new Variable("x")), new BoolTerm(true)),
				app2.reduce(new Storage())
		);
	}	
	
	
	// (λa:Bool.λc:Bool.λb:Bool.a) (b b)
	public void test_missingAlphaConversion03() {
		Abstraction abs1 = 
				new Abstraction("a", new Bool(), 
						new Abstraction("c", new Bool(),
								new Abstraction("b", new Bool(), new Variable("a"))));
		
		Application app = new Application(abs1, 
				new Application(new Variable("b"), new Variable("b")));

		assertEquals("(λc:Bool.(λb':Bool.(b)(b)))", app.reduce(new Storage()).toString());
		
		
	}	
	
	
	// (λa:Bool.λb:Bool.a) (b b)
	public void test_missingAlphaConversion02() {
		Abstraction abs1 = new Abstraction("a", new Bool(), 
				new Abstraction("b", new Bool(), new Variable("a")));
		
		Application app = new Application(abs1, 
				new Application(new Variable("b"), new Variable("b")));
		
		assertEquals("(λb':Bool.(b)(b))", app.reduce(new Storage()).toString());
		
		
	}
	
	// CLiffhanger!!!
	// (λa:Bool.λb:Bool.a) b
	public void test_missingAlphaConversion01() {
		Abstraction abs1 = new Abstraction("a", new Bool(), 
				new Abstraction("b", new Bool(), new Variable("a")));
		Application app = new Application(abs1, new Variable("b"));
		
		assert(!(new Abstraction("b", new Bool(), new Variable("b")).equals(app.reduce(new Storage()))));
		
	}
	

	
	// (λx:Bool.x) true -> true 
	public void test_reduceApplication01() {
		Abstraction abs = new Abstraction("x", new Bool(), new Variable("x"));
		BoolTerm boolTerm = new BoolTerm(true);
		Application app = new Application(abs, boolTerm);
		
		assertEquals(
				new BoolTerm(true),
				app.reduce(new Storage())
		);
	}		
	
	// [y:=true] x x -> x x
	public void test_replaceApplicationNum() {
		Application app = new Application(new Variable("x"), new Variable("x"));
		
		assertEquals(
			app.clone(),
			app.replaceFreeOccurancesOfVariable("y", new BoolTerm(true)));
	}	
	
	// [y:=true] (λx:Bool.y) -> (λx:Bool.true)
	public void test_replaceAbstractionWithNum02() {
		Abstraction abs = new Abstraction("x", new Bool(), new Variable("y"));
		
		assertEquals(
			new Abstraction("x", new Bool(), new BoolTerm(true)),
			abs.replaceFreeOccurancesOfVariable("y", new BoolTerm(true)));
	}	
	
	
	// [x:=true] (λx:Bool.x) -> (λx:Bool.x)
	public void test_replaceAbstractionWithNum01() {
		Abstraction abs = new Abstraction("x", new Bool(), new Variable("x"));
		
		assertEquals(
			abs.clone(), 
			abs.replaceFreeOccurancesOfVariable("x", new BoolTerm(true)));
	}		
	
	// [x:=1] true -> true
	public void test_replaceVarWithTrue() {
		assertEquals(
			new BoolTerm(true), 
			new Variable("x").replaceFreeOccurancesOfVariable("x", new BoolTerm(true)));
	}	
	
	// [x:=1] x -> 1
	public void test_replaceVarWithNum() {
		assertEquals(
			new NumTerm(1), 
			new Variable("x").replaceFreeOccurancesOfVariable("x", new NumTerm(1)));
	}
	
	
	// {z:Bool} |- (λx:Bool.x) z: Bool
	public void test_typeOfApplication01() {
		HashMap<String, Type> typeMapping = new HashMap<String, Type>();
		Environment e = new Environment(typeMapping);
		e.addVarType("z", new Bool());
		
		Application app = new Application(
			new Abstraction("x", new Bool(), new Variable("x")),
			new Variable("z")
		);
		
		assertEquals(new Bool(), app.typeOf(e));
				
	}		
	
	// λx:Bool.x: Bool->Bool
	public void test_typeOfAbstraction01() {
		HashMap<String, Type> typeMapping = new HashMap<String, Type>();
		Environment e = new Environment(typeMapping);
		Abstraction abs = new Abstraction("x", new Bool(), new Variable("x"));
		
		assertEquals(new FunctionType(new Bool(), new Bool()), abs.typeOf(e));
				
	}

	// 1:Num
	public void test_typeOf1() {
		HashMap<String, Type> typeMapping = new HashMap<String, Type>();
		Environment e = new Environment(typeMapping);
		
		assertEquals(new Num(), new NumTerm(1).typeOf(e));
				
	}
	
	// true:Bool
	public void test_typeOfTrue() {
		HashMap<String, Type> typeMapping = new HashMap<String, Type>();
		Environment e = new Environment(typeMapping);
		
		assertEquals(new Bool(), new BoolTerm(true).typeOf(e));
				
	}
	
}
