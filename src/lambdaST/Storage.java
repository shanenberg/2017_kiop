/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST;

import java.util.ArrayList;

import lambdaST.terms.Address;
import lambdaST.terms.LTerm;

public class Storage {
	ArrayList<LTerm> storedTerms = new ArrayList<LTerm>();

	public LTerm storeValue(LTerm term) {
		Address ret = new Address(term.typeOf(
				Environment.empty()), 
				storedTerms.size());
		storedTerms.add(term);
		return ret;
	}
	
	public void assign(Address address, LTerm term) {
		storedTerms.set(address.inStorage, term);
	}

	public LTerm valueOf(Address address) {
		return storedTerms.get(address.inStorage);
	}
	
}
