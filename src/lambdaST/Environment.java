/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST;

import java.util.HashMap;

import lambdaST.types.Type;

public class Environment {
	HashMap<String, Type> typeMappings = new HashMap<String, Type>();
	
	
	public Environment() {
		super();
		this.typeMappings = new HashMap<String, Type>();
	}
	
	public Environment(HashMap<String, Type> typeMappings) {
		super();
		this.typeMappings = typeMappings;
	}

	public Type getTypeOf(String varName) {
		return typeMappings.get(varName);
	}

	public void addVarType(String varName, Type type) {
		typeMappings.put(varName, type);
	}
	
	public Environment clone() {
		Environment e = new Environment((HashMap<String, Type>) typeMappings.clone());
		return e;
	}
	
	public static Environment empty() {
		HashMap<String, Type> typeMappings = new HashMap<String, Type> ();
		return new Environment(typeMappings);
	}

}
