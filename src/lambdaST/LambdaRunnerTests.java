/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST;

import java.io.File;
import java.io.FileReader;
import java.util.List;

import lambdaST.parser.LambdaParsing;
import lambdaST.parser.LambdaParsing.LambdaParser;
import lambdaST.parser.LambdaTest;
import lambdaST.parser.LambdaTestsParsing;
import lambdaST.terms.LTerm;
import slope.Parsing;
import slope.ResultObject;

public class LambdaRunnerTests {

	public static void main(String[] testFiles) {
		if (testFiles.length==0) {
			String currentDir = System.getProperty("user.dir");
			
			File f = new File(currentDir + File.separator + "examples" + File.separator);
			System.out.println(f);
			for (String s: f.list()) {
				new LambdaRunnerTests().runTestFile(currentDir + File.separator + "examples" + File.separator + s);
			}
		}
	}
	
	
	public void runTestFile(String fileName) {
		System.out.println("####################################################################################################");
		System.out.println("#########" +  fileName);
		System.out.println("####################################################################################################");

		String allTests = shapuc.IO.IO.readFileIntoString(fileName);
		LambdaTestsParsing tokens = new LambdaTestsParsing();

		ResultObject<List<LambdaTest>> result = new ResultObject<List<LambdaTest>>();
		
		Parsing parsing = new Parsing(tokens, allTests); 
		parsing.parseWithParseTreeConstruction(tokens.new LambdaTestsParser().Tests(result.setter));

		for(LambdaTest aTest: result.getResult()) {
			aTest.run();
		}
		System.out.println();
	}
	
}
