/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.parser;

import java.util.ArrayList;
import java.util.List;

import lambdaST.terms.LTerm;
import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;

public class LambdaTestsParsing extends LambdaParsing {

	static TokenReader<?> TESTPREFIX = new KeywordScanner(">>>>>");
	static TokenReader<?> TESTResult = new KeywordScanner("#####Result:");

	@TokenLevel(value=1) public TokenReader<?> TESTPreFix() { return TESTPREFIX; }
	@TokenLevel(value=1) public TokenReader<?> TESTResult() { return TESTResult; }
	
	public class LambdaTestsParser extends LambdaParser {
		
		public Rule Tests(final SetCmd<List<LambdaTest>> resultSetter) { return new Rule() { public PC pc() { 
			
			final List<LambdaTest> results = new ArrayList<LambdaTest>();
			
			SetCmd<LambdaTest> addResult = new SetCmd<LambdaTest>() {
				@Override
				public void set(LambdaTest value) {
					results.add(value);
				}
			};
			
			return 
				AND(
					NFOLD(
							Test(addResult)
					),
					result(resultSetter, results)
				);
		}};}			
		
		/*
		 Test -> ">>>>> Identifier"
		 */
		public Rule Test(final SetCmd<LambdaTest> resultSetter) { return new Rule() { public PC pc() { 
			final LambdaTest test = new LambdaTest();
			SetCmd<String> setTestName = new SetCmd<String>() {	public void set(String value) {test.testName = value;}};
			SetCmd<LTerm> setProgram = new SetCmd<LTerm>() {	public void set(LTerm value) {test.testProgram = value;}};
			SetCmd<LTerm> setResult = new SetCmd<LTerm>() {	public void set(LTerm value) {test.result = value;}};
			
			return 
				AND(
					TOKEN(TESTPreFix()),
					TOKEN(JAVA_IDENTIFIER(), setTestName),
					Term(setProgram),
					TOKEN(TESTResult()),
					Term(setResult),
					result(resultSetter, test)
				);
		}};}		
		
		
	}
	
	
}
