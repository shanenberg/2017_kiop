/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.parser;

import junit.framework.TestCase;
import lambdaST.terms.Abstraction;
import lambdaST.terms.Application;
import lambdaST.terms.IfTerm;
import lambdaST.terms.LTerm;
import lambdaST.terms.NumTerm;
import lambdaST.terms.Variable;
import lambdaST.terms.buildInFunctions.PLUS;
import lambdaST.types.Num;
import slope.Parsing;
import slope.ResultObject;

public class ParserTesting extends TestCase {

	
	public ResultObject<LTerm> parse(String s) {
		LambdaParsing tokens = new LambdaParsing();

		ResultObject<LTerm> result = new ResultObject<LTerm>();
		
		Parsing parsing = new Parsing(tokens, s); 
		parsing.parseWithParseTreeConstruction(tokens.new LambdaParser().Term(result.setter));
		return result;
		
	}
	
	
	public void test_FunctionType() {
		String term = "(λx:(NUM->NUM).x)";
		ResultObject<LTerm> result = parse(term);
		assertTrue(result.getResult() instanceof Abstraction);
	}			
	
	
	public void test_NumTerm() {
		String term = "123";
		ResultObject<LTerm> result = parse(term);
		assertTrue(result.getResult() instanceof NumTerm);
	}			

	public void test_Plus() {
		String term = "((+ x) x)";
		ResultObject<LTerm> result = parse(term);
		assertTrue(result.getResult() instanceof Application);
	}		
	
	public void test_boolExpression() {
		String term = "((and true) false)";
		ResultObject<LTerm> result = parse(term);
		assertTrue(result.getResult() instanceof Application);
	}	
	
	public void test_plus() {
		String term = "+";
		ResultObject<LTerm> result = parse(term);
		assertTrue(result.getResult() instanceof PLUS);
	}	
	
	
	
	public void test_if() {
		String term = "(if x then x else y)";
		ResultObject<LTerm> result = parse(term);
		
		assertTrue(result.getResult() instanceof IfTerm);
	}	
	
	
	
	public void test_App() {
		
		String term = "(x y)";
		ResultObject<LTerm> result = parse(term);
		
		assertTrue(result.getResult() instanceof Application);
	}	
	
	public void test_Abs() {
		
		String term = "(λy:NUM.x)";
		ResultObject<LTerm> result = parse(term);
		
		assertTrue(result.getResult() instanceof Abstraction);
		Abstraction a = (Abstraction) result.getResult();
		assertEquals("y", a.paramName);
		assertEquals(new Num(), a.paramType);
		assertEquals("x", ((Variable) a.body).varName);
	}	
	
	public void test_Var() {
		
		String term = "x";
		ResultObject<LTerm> result = parse(term);
		
		assertTrue(result.getResult() instanceof Variable);
		assertEquals("x", ((Variable) result.getResult()).varName);
	}
	
	
}
