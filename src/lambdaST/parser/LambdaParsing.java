/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.parser;

import lambdaST.terms.Abstraction;
import lambdaST.terms.Application;
import lambdaST.terms.Case;
import lambdaST.terms.DeRef;
import lambdaST.terms.Fix;
import lambdaST.terms.IfTerm;
import lambdaST.terms.InL;
import lambdaST.terms.InR;
import lambdaST.terms.LTerm;
import lambdaST.terms.NumTerm;
import lambdaST.terms.RecordProjection;
import lambdaST.terms.RecordTerm;
import lambdaST.terms.RefTerm;
import lambdaST.terms.Sequence;
import lambdaST.terms.UnitTerm;
import lambdaST.terms.Variable;
import lambdaST.terms.buildInFunctions.AND;
import lambdaST.terms.buildInFunctions.PLUS;
import lambdaST.types.Bool;
import lambdaST.types.FunctionType;
import lambdaST.types.Num;
import lambdaST.types.SumType;
import lambdaST.types.Type;
import lambdaST.types.Unit;
import shapuc.util.Pair;
import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.Cmd;
//import slope.lib.lambda.v01.Lambda01ParserTokens;
//import slope.lib.lambda.v01.parsetree.Abstraction;
//import slope.lib.lambda.v01.parsetree.Application;
//import slope.lib.lambda.v01.parsetree.BracketExpression;
//import slope.lib.lambda.v01.parsetree.LambdaExpression;
//import slope.lib.lambda.v01.parsetree.Variable;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;

public class LambdaParsing extends Tokens {

	static TokenReader<?> IF = new KeywordScanner("if");
	static TokenReader<?> THEN = new KeywordScanner("then");
	static TokenReader<?> ELSE = new KeywordScanner("else");
	static TokenReader<?> BOOL = new KeywordScanner("BOOL");
	static TokenReader<?> NUM = new KeywordScanner("NUM");
	static TokenReader<?> AND = new KeywordScanner("and");
	static TokenReader<?> TRUE = new KeywordScanner("true");
	static TokenReader<?> FALSE = new KeywordScanner("false");
	static TokenReader<?> FIX = new KeywordScanner("fix");
	static TokenReader<?> NOT = new KeywordScanner("not");
	static TokenReader<?> unit = new KeywordScanner("unit");
	static TokenReader<?> UNIT = new KeywordScanner("UNIT");
	static TokenReader<?> inl = new KeywordScanner("inl");
	static TokenReader<?> inr = new KeywordScanner("inr");
	static TokenReader<?> CASE = new KeywordScanner("case");
	static TokenReader<?> ref = new KeywordScanner("ref");
	static TokenReader<?> REF = new KeywordScanner("REF");
	
	@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
	@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; }

	@TokenLevel(value=1) public TokenReader<?> ARROW() { return lib.KEYWORD_SHORT_ARROW; }
	@TokenLevel(value=1) public TokenReader<?> EQUALARROW() { return lib.KEYWORD_SHORT_EQUAL_ARROW; }
	@Keyword public TokenReader<?> LAMBDA() { return lib.LAMBDA; }
	@Keyword public TokenReader<?> BOOL() { return BOOL; }
	@Keyword public TokenReader<?> NUM() { return NUM; }
	@Keyword public TokenReader<?> IF() { return IF; }
	@Keyword public TokenReader<?> THEN() { return THEN; }
	@Keyword public TokenReader<?> ELSE() { return ELSE; }
	@Keyword public TokenReader<?> PLUS() { return lib.PLUS; }
	@Keyword public TokenReader<?> MINUS() { return lib.MINUS; }
	@Keyword public TokenReader<?> AND_() { return AND; }
	@Keyword public TokenReader<?> TRUE_() { return TRUE; }
	@Keyword public TokenReader<?> FALSE_() { return FALSE; }
	@Keyword public TokenReader<?> FIX() { return FIX; }
	@Keyword public TokenReader<?> EQUNUM() { return lib.EQUALS; }
	@Keyword public TokenReader<?> NOT() { return NOT; }
	@Keyword public TokenReader<?> unit() { return unit; }
	@Keyword public TokenReader<?> UNIT() { return UNIT; }
	@Keyword public TokenReader<?> INL() { return inl; }
	@Keyword public TokenReader<?> INR() { return inr; }
	@Keyword public TokenReader<?> CASE() { return CASE; }
	@Keyword public TokenReader<?> ref() { return ref; }
	@Keyword public TokenReader<?> REF() { return REF; }
	
	
	@Token public TokenReader<?> DOT() { return lib.DOT; }
	@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
	@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
	@Token public TokenReader<?> OCBRACKET() { return lib.OPEN_CURELY_BRACKET; }
	@Token public TokenReader<?> CCBRACKET() { return lib.CLOSED_CURLY_BRACKET; }
	@Token public TokenReader<?> COMMA() { return lib.COMMA; }
	@Token public TokenReader<String> JAVA_IDENTIFIER() {	return lib.JAVA_IDENTIFIER; }
	@Token public TokenReader<Integer> JAVA_INTEGER() {	return lib.JAVA_INTEGER_LITERAL; }
	@Token public TokenReader<?> COLON() { return lib.COLON; }
	@Token public TokenReader<?> EXPLAMATIONMARK() { return lib.EXCLMARK; }
	@Token public TokenReader<?> SEMICOLON() { return lib.SEMICOLON; }

	
	public class LambdaParser extends Parser<LambdaParsing> {
		
		/*
		 LambdaExpression -> BracketExpression | Variable | Abstraction | Application
		 */
		public Rule Term(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			return 
				OR(
					ABSTRACTION(resultSetter),
					APPLICATION(resultSetter),
					EXPRESSION_CONSTANTS(resultSetter),
					IFterm(resultSetter),
					FIX(resultSetter),
					INL(resultSetter),
					INR(resultSetter),
					CASE(resultSetter),
					RECORD(resultSetter),
					PROJECTION(resultSetter),
					REF(resultSetter),
					SEQUENCE(resultSetter),
					DEREF(resultSetter)
					
				);
		}};}		
	
		
		public Rule EXPRESSION_CONSTANTS(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			Cmd cmdPLUS = new Cmd(){public void doCmd() {resultSetter.set(new PLUS());}};
			Cmd cmdMINUS = new Cmd(){public void doCmd() {resultSetter.set(new lambdaST.terms.buildInFunctions.MINUS());}};
			Cmd cmdAND = new Cmd(){public void doCmd() {resultSetter.set(new lambdaST.terms.buildInFunctions.AND());}};
			Cmd cmdTRUE = new Cmd(){public void doCmd() {resultSetter.set(new lambdaST.terms.BoolTerm(true));}};
			Cmd cmdFALSE = new Cmd(){public void doCmd() {resultSetter.set(new lambdaST.terms.BoolTerm(false));}};
			Cmd cmdEQUNUM = new Cmd(){public void doCmd() {resultSetter.set(new lambdaST.terms.buildInFunctions.EQNUM());}};
			Cmd cmdUNIT = new Cmd(){public void doCmd() {resultSetter.set(new UnitTerm());}};
			
			SetCmd<String> setIdentifier = new SetCmd<String>() { public void set(String value) {
				resultSetter.set(new Variable(value));}};
				
			SetCmd<Integer> setInteger = new SetCmd<Integer>() { public void set(Integer value) {
					resultSetter.set(new NumTerm(value));}};

			
				return 
					OR(
						TOKEN(PLUS(), cmdPLUS),
						TOKEN(MINUS(), cmdMINUS),
						TOKEN(AND_(), cmdAND),
						TOKEN(TRUE_(), cmdTRUE),
						TOKEN(FALSE_(), cmdFALSE),
						TOKEN(JAVA_INTEGER(), setInteger),
						TOKEN(JAVA_IDENTIFIER(), setIdentifier),				
						TOKEN(JAVA_IDENTIFIER(), setIdentifier),				
						TOKEN(EQUNUM(), cmdEQUNUM),
						TOKEN(unit(), cmdUNIT)
					);
		}};}	
		
		public Rule FIX(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final Fix fix = new Fix(null);

			final SetCmd<LTerm> set = new SetCmd<LTerm>() { public void set(LTerm value) {
				fix.term = value;
			}};

			return 
				AND(
					TOKEN("fix"),  
					TOKEN('('),
					Term(set),
					TOKEN(')'),
					result(resultSetter, fix)
				);
		}};}	
		
		public Rule RECORD(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final RecordTerm record = new RecordTerm();

			final Pair<String, LTerm> field = new Pair<String, LTerm>();
			
			
			final SetCmd<String> setFieldName = new SetCmd<String>() { public void set(String value) {field.setLeft(value);}};
			final SetCmd<LTerm> setFieldTerm = new SetCmd<LTerm>() { public void set(LTerm value) {field.setRight(value);}};
			final SetCmd< Pair<String, LTerm>> addField = new SetCmd< Pair<String, LTerm>>() { public void set( Pair<String, LTerm> value) {
				record.addField(value.getLeft(), value.getRight());
			}};

			return 
				AND(
					TOKEN('{'),  
					TOKEN(JAVA_IDENTIFIER(), setFieldName),
					TOKEN('='),
					Term(setFieldTerm),
					result(addField, field),
					NFOLD0(
					  AND(
					    TOKEN(','),
						TOKEN(JAVA_IDENTIFIER(), setFieldName),
						TOKEN('='),
						Term(setFieldTerm),
						result(addField, field)
					  )
					),
					
					TOKEN('}'),
					result(resultSetter, record)
				);
		}};}		
		
		public Rule PROJECTION(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final RecordProjection projection = new RecordProjection(null, null);

			final SetCmd<LTerm> setTerm = new SetCmd<LTerm>() { public void set(LTerm value) {projection.term = value;}};
			final SetCmd<String> setLabel = new SetCmd<String>() { public void set(String value) {projection.label = value;}};

			return 
				AND(
					TOKEN('('),  
					Term(setTerm),
					TOKEN('.'),
					TOKEN(JAVA_IDENTIFIER(), setLabel),  
					TOKEN(')'),
					result(resultSetter, projection)
				);
		}};}		
		
		public Rule ABSTRACTION(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final Abstraction abst = new Abstraction(null, null, null);

			final SetCmd<String> setHead = new SetCmd<String>() { public void set(String value) {
				abst.paramName = value;
			}};
			
			final SetCmd<LTerm> setBody = new SetCmd<LTerm>() {
				public void set(LTerm value) {
					abst.body = value;
			}};
			
			final SetCmd<Type> setType = new SetCmd<Type>() {
				public void set(Type value) {
					abst.paramType = value;
			}};

			return 
				AND(
					TOKEN('('), TOKEN(LAMBDA()), 
					TOKEN(JAVA_IDENTIFIER(), setHead),
					TOKEN(':'), 
					TYPE(setType),
					TOKEN('.'), Term(setBody), TOKEN(')'),
					result(resultSetter, abst)
				);
		}};}		
		
		
		public Rule IFterm(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final IfTerm ifTerm = new IfTerm(null, null, null);

			final SetCmd<LTerm> setCondition = new SetCmd<LTerm>() { public void set(LTerm value) {
				ifTerm.condition = value;
			}};
			
			final SetCmd<LTerm> setThen = new SetCmd<LTerm>() {
				public void set(LTerm value) {
					ifTerm.thenBranch = value;
			}};
			
			final SetCmd<LTerm> setElse = new SetCmd<LTerm>() {
				public void set(LTerm value) {
					ifTerm.elseBranch = value;
			}};

			return 
				AND(
					TOKEN('('), 
					TOKEN(IF()), 
					Term(setCondition),
					TOKEN(THEN()), 
					Term(setThen),
					TOKEN(ELSE()), 
					Term(setElse),
					TOKEN(')'),
					result(resultSetter, ifTerm)
				);
		}};}			
		
		public Rule TYPE(final SetCmd<Type> resultSetter) { return new Rule() { public PC pc() { 
			
			SetCmd<String> setBool = new SetCmd<String>() { public void set(String value) {
					resultSetter.set(new Bool());}};

			SetCmd<String> setNum = new SetCmd<String>() { public void set(String value) {
				resultSetter.set(new Num());}};

			SetCmd<String> setUnit = new SetCmd<String>() { public void set(String value) {
				resultSetter.set(new Unit());}};
				
				
			return 
				OR(
					TOKEN("NUM", setNum),
					TOKEN("BOOL", setBool),
					TOKEN("UNIT", setUnit),
					SUM_TYPE(resultSetter),
					FUNCTION_TYPE(resultSetter)
				);
		}};}	
		
		
		public Rule SUM_TYPE(final SetCmd<Type> resultSetter) { return new Rule() { public PC pc() { 
			
			final SumType ft = new SumType(null, null);
			
			SetCmd<Type> setLeft = new SetCmd<Type>() { public void set(Type value) {
					ft.leftType=value;}};

			SetCmd<Type> setRight = new SetCmd<Type>() { public void set(Type value) {
					ft.rightType=value;}};

			return 
				AND(
					TOKEN('('),
					TYPE(setLeft),
					TOKEN('+'),
					TYPE(setRight),
					TOKEN(')'),
					result(resultSetter, ft)
				);
		}};}			
		
		public Rule FUNCTION_TYPE(final SetCmd<Type> resultSetter) { return new Rule() { public PC pc() { 
			
			final FunctionType ft = new FunctionType(null, null);
			
			SetCmd<Type> setLeft = new SetCmd<Type>() { public void set(Type value) {
					ft.leftType=value;}};

			SetCmd<Type> setRight = new SetCmd<Type>() { public void set(Type value) {
					ft.rightType=value;}};

			return 
				AND(
					TOKEN('('),
					TYPE(setLeft),
					TOKEN(ARROW()),
					TYPE(setRight),
					TOKEN(')'),
					result(resultSetter, ft)
				);
		}};}		

	
		public Rule REF(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final RefTerm ref = new RefTerm(null);

			final SetCmd<LTerm> setTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				ref.term = value;
			}};			
			
			return 
				AND(
					TOKEN('('), 
					TOKEN("ref"), 
					Term(setTerm), 
					TOKEN(')'), 
					result(resultSetter, ref)
				);
		}};}	
			
		
		public Rule SEQUENCE(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final Sequence sequence = new Sequence(null, null);

			final SetCmd<LTerm> setLeftTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				sequence.leftTerm = value;
			}};			

			final SetCmd<LTerm> setRightTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				sequence.rightTerm = value;
			}};			
			
			return 
				AND(
					TOKEN('('), 
					Term(setLeftTerm), 
					TOKEN(';'), 
					Term(setRightTerm), 
					TOKEN(')'), 
					result(resultSetter, sequence)
				);
		}};}		
		
		public Rule DEREF(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final DeRef deref = new DeRef(null);

			final SetCmd<LTerm> setTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				deref.term = value;
			}};			
			
			return 
				AND(
					TOKEN('('), 
					TOKEN('!'), 
					Term(setTerm), 
					TOKEN(')'), 
					result(resultSetter, deref)
				);
		}};}		
		
		public Rule INL(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final InL inl = new InL(null, null);

			final SetCmd<Type> setSumType = new SetCmd<Type>() { public void set(Type value) {
				inl.sumType = (SumType) value;
			}};
			
			final SetCmd<LTerm> setTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				inl.term = value;
			}};			
			
			return 
				AND(
					TOKEN('('), 
					TOKEN("inl"), 
					SUM_TYPE(setSumType),
					Term(setTerm), 
					TOKEN(')'), 
					result(resultSetter, inl)
				);
		}};}	
		
		public Rule CASE(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final Case aCase = new Case(null, null, null, null, null, null, null);

			final SetCmd<LTerm> setTestTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				aCase.testTerm =  value;
			}};
			
			final SetCmd<Type> setSumTypeLeft = new SetCmd<Type>() { public void set(Type value) {
				aCase.sumTypeLeft = (SumType) value;
			}};
			
			final SetCmd<Type> setSumTypeRight = new SetCmd<Type>() { public void set(Type value) {
				aCase.sumTypeRight = (SumType) value;
			}};

			final SetCmd<LTerm> setLeftTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				aCase.leftTerm = value;
			}};			
			
			final SetCmd<String> setLeftVar = new SetCmd<String>() { public void set(String value) {
				aCase.leftVar = value;
			}};			
			
			final SetCmd<LTerm> setRightTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				aCase.rightTerm = value;
			}};		
			
			final SetCmd<String> setRightVar = new SetCmd<String>() { public void set(String value) {
				aCase.rightVar = value;
			}};						

			return 
				AND(
					TOKEN('('), 
					TOKEN("case"), 
					Term(setTestTerm), 
					TOKEN("inl"),
					SUM_TYPE(setSumTypeLeft),
					TOKEN(JAVA_IDENTIFIER(), setLeftVar),
					TOKEN("=>"),
					Term(setLeftTerm),
					TOKEN("inr"),
					SUM_TYPE(setSumTypeRight),
					TOKEN(JAVA_IDENTIFIER(), setRightVar),
					TOKEN("=>"),
					Term(setRightTerm),
					TOKEN(')'), 
					result(resultSetter, aCase)
				);
		}};}			
		
		public Rule INR(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final InR inl = new InR(null, null);

			final SetCmd<Type> setSumType = new SetCmd<Type>() { public void set(Type value) {
				inl.sumType = (SumType) value;
			}};
			
			final SetCmd<LTerm> setTerm = new SetCmd<LTerm>() { public void set(LTerm value) {
				inl.term = value;
			}};			
			
			return 
				AND(
					TOKEN('('), 
					TOKEN("inr"), 
					SUM_TYPE(setSumType),
					Term(setTerm), 
					TOKEN(')'), 
					result(resultSetter, inl)
				);
		}};}			
		
		
		public Rule APPLICATION(final SetCmd<LTerm> resultSetter) { return new Rule() { public PC pc() { 
			
			final Application appl = new Application(null, null);

			final SetCmd<LTerm> setLeft = new SetCmd<LTerm>() { public void set(LTerm le) {
				appl.left = le;
			}};
			
			final SetCmd<LTerm> setRight = new SetCmd<LTerm>() { public void set(LTerm le) {
				appl.right = le;
			}};			
			
			return 
				AND(
					TOKEN('('), 
					Term(setLeft), 
					Term(setRight), 
					TOKEN(')'), 
					result(resultSetter, appl)
				);
		}};}	
	
	}			

	
}
