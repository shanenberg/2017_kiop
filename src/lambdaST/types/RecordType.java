/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.types;

import java.util.Hashtable;

import shapuc.util.Pair;

public class RecordType extends Type {

	Pair<String, Type>[] fieldTypes;
	
	public RecordType(Pair<String, Type>[] fieldTypes) {
		super();
		this.fieldTypes = fieldTypes;
	}
	
	public RecordType() {
		super();
	}	

	@Override
	public Type clone() {
		
		Pair<String, Type>[] newFieldTypes = new Pair[fieldTypes.length];
		
		for(int i=0; i < fieldTypes.length; i++) {
			newFieldTypes[i] = new Pair<String, Type>(fieldTypes[i].getLeft(), fieldTypes[i].getRight());
		}
		
		return new RecordType(newFieldTypes);
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("{");
		
		ret.append(fieldTypes[0].getLeft() + ":" + fieldTypes[0].getLeft().toString());

		for(int i=1; i < fieldTypes.length; i++) {
			ret.append(", " + fieldTypes[i].getLeft() + ":" + fieldTypes[i].getLeft().toString());
			
		}
		
		ret.append("}");
		return ret.toString();
	}

	@Override
	public boolean equals(Object something) {
		if (!(something instanceof RecordType)) return false;
		
		RecordType that = (RecordType) something;
		
		if (this.fieldTypes.length != that.fieldTypes.length) return false;
		
		for(int i=0; i < fieldTypes.length; i++) {
			if (!this.fieldTypes[i].equals(that.fieldTypes[i])) return false;
		}

		return true;
	}
	
	
	public Type typeOfLabel(String label) {
		for(int i=0; i < fieldTypes.length; i++) {
			if (fieldTypes[i].getLeft().equals(label)) {
				return fieldTypes[i].getRight();
			}
		}		
		throw new RuntimeException("The type does not have appropriate label");
	}

}
