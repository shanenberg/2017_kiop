/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.RecordType;
import lambdaST.types.Type;

public class RecordProjection extends LTerm {

	public String label;
	public LTerm term;
	
	public RecordProjection(String label, LTerm term) {
		super();
		this.label = label;
		this.term = term;
	}

	@Override
	public Type typeOf(Environment environment) {
		Type rt = term.typeOf(environment);
		if (!(rt instanceof RecordType))	{
			throw new RuntimeException("Projection requires RecordTypes");
		}
		RecordType recordType = (RecordType) rt;
		return recordType.typeOfLabel(label);
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage storage) {
		
		if (term.isReducible()) {
			return new RecordProjection(label, term.reduce(storage));
		} 
		
		return ((RecordTerm) term).termAtLabel(label);
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return new RecordProjection(label, term.replaceFreeOccurancesOfVariable(paramName, right));
	}

	@Override
	public LTerm clone() {
		return new RecordProjection(label, term.clone());
	}

	@Override
	public String toString() {
		return term.toString() + "." + label;
	}

	@Override
	public Set<String> freeVariables() {
		return term.freeVariables();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof RecordProjection)) return false;
		RecordProjection that = (RecordProjection) o;
		return (this.label.equals(that.label) && this.term.equals(that.term));
	}

}
