/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.SumType;
import lambdaST.types.Type;

public class InL extends LTerm {

	public SumType sumType;
	public LTerm term; 
	

	public InL(SumType sumType, LTerm term) {
		super();
		this.sumType = sumType;
		this.term = term;
	}

	/**
	 *        E |- t: T1
	 * ========================
	 * E |- inl[T1+T2] t: T1+T2
	 */
	@Override
	public Type typeOf(Environment environment) {
		if (!term.typeOf(environment).equals(sumType.leftType))
			throw new RuntimeException("Falscher Typ bei Inl");
		return sumType;
	}

	@Override
	public boolean isReducible() {
		return term.isReducible();
	}

	@Override
	public LTerm reduce(Storage s) {
		return new InL(sumType, term.reduce(s));
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return new InL(sumType, term.replaceFreeOccurancesOfVariable(paramName, right));
	}

	@Override
	public LTerm clone() {
		return new InL(sumType.clone(), term.clone());
	}

	@Override
	public String toString() {
		return "Inl[" + sumType + "]" + term.toString();
	}

	@Override
	public Set<String> freeVariables() {
		return term.freeVariables();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof InL)) return false;
		InL that = (InL)obj;
		return that.term.equals(term);
	}



}
