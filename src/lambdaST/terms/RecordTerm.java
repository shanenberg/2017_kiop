/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.RecordType;
import lambdaST.types.Type;
import shapuc.util.Pair;

public class RecordTerm extends LTerm {

	public Pair<String, LTerm>[] fields = new Pair[0];
	
	public RecordTerm() {
		super();
	}
	
	
	public RecordTerm(Pair<String, LTerm>[] fields) {
		super();
		this.fields = fields;
	}

	public void addField(String fieldName, LTerm value) {
		fields = Arrays.copyOf(fields, fields.length + 1);
		fields[fields.length-1] = new Pair<String, LTerm>(fieldName, value);
	}
	
	@Override
	public Type typeOf(Environment environment) {
		Pair<String, Type>[] fieldTypes = new Pair[fields.length];
		
		for(int i=0; i<fields.length;i++) {
			Pair<String, Type> fieldType = new Pair<String, Type>(fields[i].getLeft(), fields[i].getRight().typeOf(environment));
			fieldTypes[i] = fieldType;
		}

		return new RecordType(fieldTypes);
	}

	@Override
	public boolean isReducible() {
		for(int i=0; i<fields.length;i++) {
			if (fields[i].getRight().isReducible())
				return true;
		}
		return false;
	}

	@Override
	public LTerm reduce(Storage storage) {
		RecordTerm ret = this.clone();
		for(int i=0; i<fields.length;i++) {
			if (fields[i].getRight().isReducible()) {
				fields[i] = new Pair<String, LTerm>(
						fields[i].getLeft(), 
						fields[i].getRight().reduce(storage));
				return ret;

			}
		}
		throw new RuntimeException("RecordTerm " + this.toString() + " cannot be reduced ");
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		for(int i=0; i<fields.length;i++) {
			fields[i].setRight(fields[i].getRight().replaceFreeOccurancesOfVariable(paramName, right));
		}
		return this;
	}

	@Override
	public RecordTerm clone() {
		Pair<String, LTerm>[] clonedFields = new Pair[fields.length]; 
		for(int i=0; i<fields.length;i++) {
			clonedFields[i] = new Pair<String, LTerm>(fields[i].getLeft(), fields[i].getRight().clone());
		}
		return new RecordTerm(clonedFields);
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("{");
		
		ret.append(fields[0].getLeft() + ":" + fields[0].getLeft().toString());

		for(int i=1; i < fields.length; i++) {
			ret.append(", " + fields[i].getLeft() + "=" + fields[i].getLeft().toString());
			
		}
		
		ret.append("}");
		return ret.toString();
	}

	@Override
	public Set<String> freeVariables() {
		HashSet<String> ret = new HashSet<String>();
		for(int i=1; i < fields.length; i++) {
			ret.addAll(fields[i].getRight().freeVariables());
		}
		return ret;
	}

	@Override
	public boolean equals(Object o) {
		if (! (o instanceof RecordTerm)) return false;
		RecordTerm that = (RecordTerm) o;
		
		if (this.fields.length != that.fields.length) return false;
		for(int i=1; i < fields.length; i++) {
			if (!this.fields[i].equals(that.fields[i])) return false;
		}
		
		return true;
		
	}
	
	public LTerm termAtLabel(String label) {
		for(int i=0; i < fields.length; i++) {
			if (fields[i].getLeft().equals(label))
				return fields[i].getRight(); 
		}
		throw new RuntimeException("Unknown Field");
	}

}
