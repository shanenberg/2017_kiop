package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.Type;

/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

public abstract class LTerm {
	public abstract Type typeOf(Environment environment);
	public abstract boolean isReducible();
	public abstract LTerm reduce(Storage storage);
	
	public boolean isAbstraction() {
		return false;
	}
	
	public abstract LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right);
	public abstract LTerm clone();
	public abstract String toString();
	
	public abstract Set<String> freeVariables();
	
	public abstract boolean equals(Object o);
	
	public LTerm reduceAll(Storage s) {
		LTerm ret = this;
		while(this.isReducible()) {
			ret = this.reduce(s);
		}
		return ret;
	}
	
	public LTerm reduceAll() {
		LTerm ret = this;
		Storage s = new Storage();
		while(ret.isReducible()) {
			ret = ret.reduce(s);
		}
		return ret;
	}	
	
}
