/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms.buildInFunctions;

import java.util.HashSet;
import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.terms.LTerm;
import lambdaST.terms.NumTerm;
import lambdaST.types.Bool;
import lambdaST.types.FunctionType;
import lambdaST.types.Num;
import lambdaST.types.Type;

public class EQNUM extends BuiltInFunction {

	@Override
	public Type typeOf(Environment environment) {
		return new FunctionType(new Num(), new FunctionType(new Num(), new Bool()));
	}

	@Override
	public boolean isReducible() {
		return false;
	}

	@Override
	public LTerm reduce(Storage s) {
		throw new RuntimeException("Add cannot be reduced");
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return this.clone();		
	}

	@Override
	public LTerm clone() {
		return new EQNUM();
	}

	@Override
	public String toString() {
		return " EQNUM ";
	}

	@Override
	public Set<String> freeVariables() {
		return new HashSet<String>();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof EQNUM;
	}

	@Override
	public LTerm apply(LTerm parameter) {
		NumTerm p = (NumTerm) parameter;
		return new EQNUM1(p.value);
	}

}
