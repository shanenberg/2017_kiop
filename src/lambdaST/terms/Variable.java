/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.HashSet;
import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.Type;

public class Variable extends LTerm {

	public String varName;
	
	public Variable(String varName) {
		super();
		this.varName = varName;
	}

	@Override
	/* T-Var
	 *     x:T element E
	 *     =============
	 *       E |- x:T
	 */
	public Type typeOf(Environment environment) {
		return environment.getTypeOf(varName);
	}

	@Override
	public boolean isReducible() {
		return false;
	}

//	@Override
//	public LTerm reduce() {
//		throw new RuntimeException("Mach das nicht");
//	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		if (varName.equals(paramName)) {
			return right.clone();
		}
		return this.clone();
	}

	@Override
	public LTerm clone() {
		return new Variable(varName);
	}
	
	public String toString() {
		return varName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((varName == null) ? 0 : varName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (varName == null) {
			if (other.varName != null)
				return false;
		} else if (!varName.equals(other.varName))
			return false;
		return true;
	}

	@Override
	public Set<String> freeVariables() {
		HashSet<String> ret = new HashSet<String>();
		ret.add(this.varName);
		return ret;
	}

	@Override
	public LTerm reduce(Storage storage) {
		throw new RuntimeException("Cannot reduce variable");
	}		
	
	

}
