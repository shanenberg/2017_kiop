/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.HashSet;
import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.Type;
import lambdaST.types.Unit;

public class Assign extends LTerm {

	LTerm left;
	LTerm right;
	
	
	public Assign(LTerm left, LTerm right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public Type typeOf(Environment environment) {
		return new Unit();
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage storage) {
		if (left.isReducible()) {
			return new Assign(left.reduce(storage), right.clone());
		}
		
		if (right.isReducible()) {
			return new Assign(left.clone(), right.reduce(storage));
		}
		
		Address address = (Address) left;
		storage.assign(address, right);
		return new UnitTerm();
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return new Assign(
				left.replaceFreeOccurancesOfVariable(paramName, right),
				right.replaceFreeOccurancesOfVariable(paramName, right));
	}

	@Override
	public LTerm clone() {
		return new Assign(left.clone(), right.clone());
	}

	@Override
	public String toString() {
		return left.toString() + " := " + right.toString();
	}

	@Override
	public Set<String> freeVariables() {
		Set<String> leftSet = left.freeVariables();
		Set<String> rightSet = right.freeVariables();
		HashSet<String> ret = new HashSet<String>();
		ret.addAll(leftSet);
		ret.addAll(rightSet);
		return ret;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Assign)) return false;
		
		Assign other = (Assign) o;
		
		return this.left.equals(other.left) && this.right.equals(other.right);
	}

}
