/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.HashSet;
import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.Bool;
import lambdaST.types.SumType;
import lambdaST.types.Type;

public class IfTerm extends LTerm {

	public LTerm condition;
	public LTerm thenBranch;
	public LTerm elseBranch;
		
	public IfTerm(LTerm condition, LTerm thenBranch, LTerm elseBranch) {
		super();
		this.condition = condition;
		this.thenBranch = thenBranch;
		this.elseBranch = elseBranch;
	}

	/**
	 *             E |- t1: Bool    E|-t2:T     E|-t3:T 
	 *            =======================================
	 *                 E |- if t1 then t2 else t3: T
     *
	 *             E |- t1: Bool    E|-t2:T1     E|-t3:T2  T1!=T2
	 *            ===============================================
	 *                 E |- if t1 then t2 else t3: T1+T2
	 *
	 */
	@Override
	public Type typeOf(Environment environment) {
		if (!condition.typeOf(environment).equals(new Bool())) {
			throw new RuntimeException("puttematt");
		}
		
		Type thenType = thenBranch.typeOf(environment);
		Type elseType = elseBranch.typeOf(environment);
		
		if (!(thenType.equals(elseType))) {
				// SummenTyp
				return new SumType(thenType, elseType);
		}
		
		return thenType;
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage s) {
		if(condition.isReducible()) {
			return new IfTerm(condition.reduce(s), thenBranch.clone(), elseBranch.clone());
		}
		
		if (condition.equals(new BoolTerm(true))) {
			return thenBranch.clone();
		} else {
			return elseBranch.clone();
		}
		
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return new IfTerm(
				condition.replaceFreeOccurancesOfVariable(paramName, right),
				thenBranch.replaceFreeOccurancesOfVariable(paramName, right),
				elseBranch.replaceFreeOccurancesOfVariable(paramName, right))
			;
	}

	@Override
	public LTerm clone() {
		return new IfTerm(condition.clone(), thenBranch.clone(), elseBranch.clone());
	} 

	@Override
	public String toString() {
		return "(if ("  + condition.toString() + 
				") then (" + thenBranch.toString() + ") else (" +
		elseBranch.toString() + "))";
	}

	@Override
	public Set<String> freeVariables() {
		Set<String> cf = condition.freeVariables();
		Set<String> tf = thenBranch.freeVariables();
		Set<String> ef = elseBranch.freeVariables();
		
		Set<String> ret = new HashSet<String>();
		ret.addAll(cf);
		ret.addAll(tf);
		ret.addAll(ef);

		return ret;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof IfTerm)) return false;
		IfTerm _ifTerm = (IfTerm) o;
		return 
			this.condition.equals(_ifTerm.condition) &&
			this.thenBranch.equals(_ifTerm.thenBranch) &&
			this.elseBranch.equals(_ifTerm.elseBranch);
	}

}
