/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.SumType;
import lambdaST.types.Type;

public class Case extends LTerm {

	public LTerm testTerm;

	public SumType sumTypeLeft;
	public String leftVar;
	public LTerm leftTerm;
	
	public SumType sumTypeRight;
	public String rightVar;
	public LTerm rightTerm;
	
	public Case(LTerm testTerm, SumType sumLeftType, String leftVar, LTerm leftTerm, SumType sumLeftRight, String rightVar, LTerm rightTerm) {
		super();
		this.testTerm = testTerm;
		this.sumTypeLeft = sumTypeLeft;
		this.leftVar = leftVar;
		this.leftTerm = leftTerm;
		this.sumTypeRight = sumTypeRight;
		this.rightVar = rightVar;
		this.rightTerm = rightTerm;
	}

	/**
	 * 
	 * E|- x:T1+T2  E, (y:T1)|- t1: retType   E, (z.T2)|- t2: retType    
	 * =========================================================
	 * E|- case x inl[T1+T2] y=> t1 || inr[T1+T2] z=>t2: retType
	 * 
	 */
	@Override
	public Type typeOf(Environment environment) {
		
		Type testType = testTerm.typeOf(environment);
		if (!(sumTypeLeft.equals(testType) && sumTypeRight.equals(testType)))
			throw new RuntimeException("Falscher Typ");
		
		Environment e1 = environment.clone();
		e1.addVarType(leftVar, sumTypeLeft.leftType);
		Type retType = leftTerm.typeOf(e1);
		
		Environment e2 = environment.clone();
		e2.addVarType(rightVar, sumTypeRight.rightType);
		Type retType2 = rightTerm.typeOf(e2);

		if (!retType.equals(retType2))
			throw new RuntimeException("Fehler in Vorbedingung");
		
		return retType;
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage s) {
		if (testTerm.isReducible())
			return new Case(testTerm.reduce(s), sumTypeLeft, leftVar, leftTerm, sumTypeRight, rightVar, rightTerm);
		
		if (testTerm instanceof InL) {
			InL lTerm = (InL) testTerm;
			LTerm lTermValue = lTerm.term;
			return leftTerm.replaceFreeOccurancesOfVariable(leftVar, lTermValue);
		}
		
		if (testTerm instanceof InR) {
			InR rTerm = (InR) testTerm;
			LTerm rTermValue = rTerm.term;
			return rightTerm.replaceFreeOccurancesOfVariable(rightVar, rTermValue);
		}		
		
		throw new RuntimeException("Just another type error");
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm parameter) {
		return new Case(
				testTerm.clone(),
				sumTypeLeft.clone(),
				leftVar,
				leftTerm.replaceFreeOccurancesOfVariable(paramName, parameter),
				sumTypeRight.clone(),
				rightVar,
				rightTerm.replaceFreeOccurancesOfVariable(paramName, parameter));
	}

	@Override
	public LTerm clone() {
		return new Case(
				testTerm.clone(),
				sumTypeLeft.clone(),
				leftVar,
				leftTerm.clone(),
				sumTypeRight.clone(),
				rightVar,
				rightTerm.clone());
	}

	@Override
	public String toString() {
		return " case .....";
	}

	@Override
	public Set<String> freeVariables() {
		// etas komplizierter
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// 2-3 Zeilen
		return false;
	}

}
