/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.FunctionType;
import lambdaST.types.Type;

public class Abstraction extends LTerm {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((paramName == null) ? 0 : paramName.hashCode());
		result = prime * result + ((paramType == null) ? 0 : paramType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Abstraction other = (Abstraction) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (paramName == null) {
			if (other.paramName != null)
				return false;
		} else if (!paramName.equals(other.paramName))
			return false;
		if (paramType == null) {
			if (other.paramType != null)
				return false;
		} else if (!paramType.equals(other.paramType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "(λ" + paramName + ":" + paramType.toString() + "." +  body.toString() + ")";
	}

	public String paramName;
	public Type paramType;
	public LTerm body;
	
	public Abstraction(String paramName, Type paramType, LTerm body) {
		super();
		this.paramName = paramName;
		this.paramType = paramType;
		this.body = body;
	}
	
	public boolean isAbstraction() {
		return true;
	}	

	@Override
	/* T-Abs
	 *     
	 *           E, x:T1 |- body: T2
	 *     ============================
	 *       E |- λx:T1.body : T1 -> T2
	 */	
	public Type typeOf(Environment environment) {
		Environment e1 = environment.clone();
		e1.addVarType(paramName, paramType);

		Type T2 = body.typeOf(e1);
		
		return new FunctionType(paramType, T2);
	}

	@Override
	public boolean isReducible() {
		return false;
	}

	@Override
	public LTerm reduce(Storage s) {
		throw new RuntimeException("Abstractions cannot be reduced");
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String replacedVar, LTerm right) {
		if (paramName.equals(replacedVar)) {
			return this.clone();
		} else {
			if (right.freeVariables().contains(this.paramName)) {
			// Problem: wir brauchen AlphaConversion
				String newIdentifier = this.paramName;
				while(right.freeVariables().contains(newIdentifier)) {
					newIdentifier = newIdentifier + "'";
				}
				// newIdentifier kommt nicht mehr als freie Variable rechts vor
				LTerm alphaConvertedTerm = doAlphaConversionTo(newIdentifier);
				return alphaConvertedTerm.replaceFreeOccurancesOfVariable(replacedVar, right);
			}
			
			LTerm replacedBody = body.replaceFreeOccurancesOfVariable(replacedVar, right);
			return new Abstraction(paramName, paramType, replacedBody);
		}
	}

	private LTerm doAlphaConversionTo(String newIdentifier) {
		LTerm newBody = body.replaceFreeOccurancesOfVariable(this.paramName, new Variable(newIdentifier));
		return new Abstraction(newIdentifier, this.paramType.clone(), newBody);
		
	}

	@Override
	public LTerm clone() {
		return new Abstraction(paramName, paramType.clone(), body.clone());
	}

	@Override
	public Set<String> freeVariables() {
		Set<String> bodyVars = body.freeVariables();
		bodyVars.remove(this.paramName);
		return bodyVars;
	}
	
	public LTerm apply(LTerm parameter) {
		return body.replaceFreeOccurancesOfVariable( paramName , parameter);		
	}

}
