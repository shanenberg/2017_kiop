/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.Type;

public class Sequence extends LTerm {

	public LTerm leftTerm;
	public LTerm rightTerm;
	
	public Sequence(LTerm leftTerm, LTerm rightTerm) {
		super();
		this.leftTerm = leftTerm;
		this.rightTerm = rightTerm;
	}

	@Override
	public Type typeOf(Environment environment) {
		leftTerm.typeOf(environment);
		Type rightType = rightTerm.typeOf(environment);
		return rightType;
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage storage) {
		if (leftTerm.isReducible())
			return new Sequence(leftTerm.reduce(storage), rightTerm.clone());

		
		return rightTerm.clone();
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return new Sequence(leftTerm.replaceFreeOccurancesOfVariable(paramName, right), rightTerm.replaceFreeOccurancesOfVariable(paramName, right));
	}

	@Override
	public LTerm clone() {
		return new Sequence(leftTerm.clone(), rightTerm.clone());
	}

	@Override
	public String toString() {
		return "(" + leftTerm.toString() + ";" + rightTerm.toString() + ")";
	}

	@Override
	public Set<String> freeVariables() {
		Set<String> ret = leftTerm.freeVariables();
		ret.addAll(rightTerm.freeVariables());
		return ret;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Sequence)) return false;
		Sequence that = (Sequence) o;
		return this.leftTerm.equals(that.leftTerm) && this.rightTerm.equals(that.rightTerm);
	}

}
