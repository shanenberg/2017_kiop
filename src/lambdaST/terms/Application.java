/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.FunctionType;
import lambdaST.types.Type;

public class Application extends LTerm {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((left == null) ? 0 : left.hashCode());
		result = prime * result + ((right == null) ? 0 : right.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (left == null) {
			if (other.left != null)
				return false;
		} else if (!left.equals(other.left))
			return false;
		if (right == null) {
			if (other.right != null)
				return false;
		} else if (!right.equals(other.right))
			return false;
		return true;
	}

	public LTerm left;
	public LTerm right;
	
	
	public Application(LTerm left, LTerm right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	/* T-Abs
	 *     
	 *       E|- left: T2->T1    E|-right: T2
	 *     =============================
	 *             E |- left right: T1
	 */		
	public Type typeOf(Environment environment) {
		Type T2T1 = left.typeOf(environment);
		Type T2 = right.typeOf(environment);
		
		if (T2T1.isFunctionType()) {
			FunctionType functionType = (FunctionType) T2T1;
			if (functionType.leftType.equals(T2)) {
				return functionType.rightType;
			}
			throw new RuntimeException("FunctionType expects: " + functionType.leftType.toString() + " but received " + T2.toString());
		}
		
		throw new RuntimeException("Kaputtt");
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage storage) {
		if (left.isReducible()) {
			return new Application(left.reduce(storage), right.clone());
		} else {
			if (right.isReducible()) {
				return new Application(left.clone(), right.reduce(storage).clone());
			} else if (left.isAbstraction()) {
				return ((Abstraction) left).apply(right);
			} 
		}
		
		throw new RuntimeException("Ganz toller witz hier");
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm repl) {
		return new Application(
				left.replaceFreeOccurancesOfVariable(paramName, repl),
				right.replaceFreeOccurancesOfVariable(paramName, repl));
	}

	@Override
	public LTerm clone() {
		return new Application(left.clone(), right.clone());
	}

	@Override
	public String toString() {
		return "(" + left.toString() + ")" + "(" + right.toString() + ")";
	}

	@Override
	public Set<String> freeVariables() { 
		Set<String> leftVars = left.freeVariables();
		Set<String> rightVars = right.freeVariables();
		
		HashSet<String> ret = new HashSet<String>();
		ret.addAll(leftVars);
		ret.addAll(rightVars);
		return ret;
	}

}
