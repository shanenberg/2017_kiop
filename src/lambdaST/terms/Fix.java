/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package lambdaST.terms;

import java.util.Set;

import lambdaST.Environment;
import lambdaST.Storage;
import lambdaST.types.FunctionType;
import lambdaST.types.Type;

public class Fix extends LTerm {

	public LTerm term;
	
	
	public Fix(LTerm term) {
		super();
		this.term = term;
	}

	@Override
	public Type typeOf(Environment environment) {
		Type t = term.typeOf(environment);
		if (!(t.isFunctionType())) {
			throw new RuntimeException("dummy");
		}
		FunctionType ft = (FunctionType) t;
		
		if (!ft.leftType.equals(ft.rightType)) {
			throw new RuntimeException("dummy2");
		}
		
		return ft.leftType.clone();
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public LTerm reduce(Storage s) {
		
		if (term.isReducible()) {
			return new Fix(term.reduce(s));
		} else {
			Abstraction abs = (Abstraction) term;
			
			return abs.body.replaceFreeOccurancesOfVariable(
					abs.paramName, 
					this.clone());
		}
	}

	@Override
	public LTerm replaceFreeOccurancesOfVariable(String paramName, LTerm right) {
		return new Fix(term.replaceFreeOccurancesOfVariable(paramName, right));
	}

	@Override
	public LTerm clone() {
		return new Fix(term.clone());
	}

	@Override
	public String toString() {
		return "fix(" + term.toString() + ")";
	}

	@Override
	public Set<String> freeVariables() {
		return term.freeVariables();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Fix)) return false;
		Fix param = (Fix) o;
		return param.term.equals(this.term);
	}


}
