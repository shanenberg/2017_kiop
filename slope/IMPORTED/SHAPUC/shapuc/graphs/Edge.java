package shapuc.graphs;

public class Edge<NodeType> {
	NodeType source;
	NodeType target;

	public Edge(NodeType source, NodeType target) {
		super();
		this.source = source;
		this.target = target;
	}

	public NodeType getSource() {
		return source;
	}

	public NodeType getTarget() {
		return target;
	}
	
	public String toString() {
		return source.toString() + "-->" + target.toString();
	}
	
}
