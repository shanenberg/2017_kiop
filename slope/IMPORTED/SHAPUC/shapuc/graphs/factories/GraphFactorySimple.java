package shapuc.graphs.factories;

import shapuc.graphs.Graph;
import shapuc.graphs.simple.SimpleEdge;
import shapuc.graphs.simple.SimpleNode;

public class GraphFactorySimple extends GraphFactory<SimpleNode, SimpleEdge>{

	@Override
	public Graph<SimpleNode, SimpleEdge> createGraph() {
		return new Graph<SimpleNode, SimpleEdge>(this);
	}

	@Override
	public SimpleEdge createEdge(SimpleNode n1, SimpleNode n2) {
		return new SimpleEdge(n1, n2);
	}

	@Override
	public SimpleNode createNode(String identifier) {
		return new SimpleNode(identifier);
	}

}
