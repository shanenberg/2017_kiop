package shapuc.graphs.factories;

import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.grapical.GraphicalNode;

public class GraphFactoryGraphical extends GraphFactory<GraphicalNode, Edge<GraphicalNode>>{

	@Override
	public Graph<GraphicalNode, Edge<GraphicalNode>> createGraph() {
		return new Graph<GraphicalNode, Edge<GraphicalNode>>(this);
	}

	@Override
	public Edge<GraphicalNode> createEdge(GraphicalNode n1, GraphicalNode n2) {
		return new Edge<GraphicalNode>(n1, n2);
	}

	@Override
	public GraphicalNode createNode(String identifier) {
		return new GraphicalNode(identifier);
	}

}

