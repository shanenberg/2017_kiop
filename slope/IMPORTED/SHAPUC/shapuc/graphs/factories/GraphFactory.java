package shapuc.graphs.factories;

import shapuc.graphs.Edge;
import shapuc.graphs.Graph;
import shapuc.graphs.Node;

public abstract class GraphFactory<NodeType extends Node<NodeType>, EdgeType extends Edge<NodeType>> {
	public abstract Graph<NodeType, EdgeType> createGraph();
	
	public abstract EdgeType createEdge(NodeType n1, NodeType n2);
	
	public EdgeType createEdge(Graph<NodeType, EdgeType> g, String ni1, String ni2) {
		NodeType n1 = g.getNode(ni1);
		NodeType n2 = g.getNode(ni2);
		return this.createEdge(n1, n2);
	}
	
	public abstract NodeType createNode(String identifier);
}
