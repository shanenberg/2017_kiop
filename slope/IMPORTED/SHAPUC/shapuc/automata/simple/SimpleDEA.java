package shapuc.automata.simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Set;

import shapuc.util.IdentitySet;
import shapuc.util.Pair;

public class SimpleDEA<StateType, InputType> extends SimpleAutomata<StateType, InputType> {

	StateType currentState;
	public StateType getCurrentState() {
		return currentState;
	}
	
	public void start() {
		if (startState==null) throw new RuntimeException("StartState is null -> set start state first");
		currentState = startState;
	}		
	
	public ArrayList<Pair<InputType, StateType>> nextTransitions() {
		return stateTransitionTable.get(currentState);
	}
	
	public void gotoNext(InputType input) {
		for (Pair<InputType, StateType> transition : nextTransitions()) {
			if (transition.getLeft().equals(input))
				currentState = transition.getRight();
				return;
		}

		throw new RuntimeException("No follow up state found");
	}
	
	public boolean hasFinished() {
		return endStates.contains(currentState);
	}

	public SimpleDEA(IdentityHashMap<StateType, ArrayList<Pair<InputType, StateType>>> stateTransitionTable, StateType currentState,
			IdentitySet<StateType> endStates) {
		super();
		this.stateTransitionTable = stateTransitionTable;
		this.currentState = currentState;
		this.endStates = endStates;
	}
	
}
