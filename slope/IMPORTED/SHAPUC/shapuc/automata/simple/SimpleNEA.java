package shapuc.automata.simple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import shapuc.util.IdentitySet;
import shapuc.util.Pair;

/*
 * Input is handled via equals, states are handled via Identity 
 */
public class SimpleNEA<StateType, InputType> extends SimpleAutomata<StateType, InputType>{

	public InputType getSpontaneousInput() {
		return spontaneousInput;
	}

	public void setSpontaneousInput(InputType spontaneousInput) {
		this.spontaneousInput = spontaneousInput;
	}

	IdentitySet<StateType> currentStates = new IdentitySet<StateType>();
	
	InputType spontaneousInput;
	
	public Set<StateType> getCurrentStates() {
		return currentStates;
	}	
	
	public void start() {
		if (startState==null) throw new RuntimeException("StartState is null -> set start state first");
		currentStates = new IdentitySet<StateType>();
		currentStates.add(startState);
		doSpontaneousTransitionsIfNecessary();
	}		
	
	
	/**
	 * Slightly naive implementation: for spontaneous transitions all states are repeated 
	 * @param input
	 * @return
	 */
	public void gotoNext(InputType input) {
		if (invalidInput(input)) throw new RuntimeException("Invalid NEA input: " + input);
		
		IdentitySet<StateType> _currentStates = currentStates.clone();
		IdentitySet<StateType> _nextCurrentStates = new IdentitySet<StateType>();
		for (StateType aCurrentState : _currentStates) {
			if (input==spontaneousInput) {
				_nextCurrentStates.addAll(nextDirectSpontaneousStates(aCurrentState));
			} else {
				_nextCurrentStates.addAll(nextDirectStates(aCurrentState, input));
			}
		}
		currentStates = _nextCurrentStates;
		doSpontaneousTransitionsIfNecessary();
	}	
	
	private void printCurrentStates() {
		StringBuffer buf = new StringBuffer(super.toString());
		buf.append("\nCurrentStates:\n");
		for (StateType aState : currentStates) {
			buf.append(aState + "--");
		}
		System.out.println(buf.toString());
	}

	private void printStates(IdentitySet<StateType> _currentStates) {
		for (StateType stateType : _currentStates) {
			System.out.println(stateType);
		}
		
	}

	private boolean invalidInput(InputType input) {
		if (input.equals(spontaneousInput)) return false;
		
		IdentitySet<StateType> possibleNext = new IdentitySet<StateType>();
		
		for (StateType stateType : currentStates) {
			if (nextDirectStates(stateType, input).size()>0)
				return false;
		}

		return true;
	}

	public boolean hasSponaneousTransition(StateType aState) {
		return !this.nextDirectStates(aState, spontaneousInput).isEmpty();
	}
	
	public boolean hasStateWithSpontaneousTransition() {
		for (StateType aState : currentStates) {
			if (hasSponaneousTransition(aState)) return true;
		}
		return false;
	}
	
	public IdentitySet<StateType> statesWithSpontaneousTransition() {
		IdentitySet<StateType> ret = new IdentitySet<StateType>();
		
		for (StateType aState : currentStates) {
			if (hasSponaneousTransition(aState)) ret.add(startState);
		}
		
		return ret;
	}	
	
	private void doSpontaneousTransitionsIfNecessary() {
		
		boolean requiredHandling = true;
		
		while (requiredHandling) {
			requiredHandling = false;
			IdentitySet<StateType> _currentStates = currentStates.clone();
			
			for (StateType aState : _currentStates) {
				Collection<? extends StateType> newStates = 
						nextDirectSpontaneousStates(aState);
				
				for (StateType newState : newStates) {
					if (!currentStates.contains(newState)) {
						currentStates.add(newState);
						requiredHandling = true;
					}
				}
			}
		}
	}

	private IdentitySet<StateType> nextDirectStates(StateType aState, InputType input) {
		ArrayList<Pair<InputType, StateType>> transitions = this.allTransitionsLike(aState, input);
		
		IdentitySet<StateType> result = new IdentitySet<StateType>();
		
		for (Pair<InputType, StateType> aTransition : transitions) {
			result.add(aTransition.getRight());
		}
		
		return result;
	}
	
	private IdentitySet<StateType> nextDirectSpontaneousStates(StateType aState) {
		ArrayList<Pair<InputType, StateType>> transitions = this.allTransitionsIdenticalTo(aState, spontaneousInput);
		
		IdentitySet<StateType> result = new IdentitySet<StateType>();
		
		for (Pair<InputType, StateType> aTransition : transitions) {
			result.add(aTransition.getRight());
		}
		
		return result;
	}	

	@Override
	public boolean hasFinished() {
		for (StateType aCurrentState : currentStates) {
			if (endStates.contains(aCurrentState)) return true;
		}
		return false;
	}
	
	public void addSpontaneousTransition(StateType start, StateType end) {
		this.addTransition(start, this.spontaneousInput, end);
	}

	public String toString() {
		
		StringBuffer buf = new StringBuffer(super.toString());
		buf.append("\nCurrentStates:\n");
		for (StateType aState : currentStates) {
			buf.append(aState + "\n");
		}
		buf.append("\nEndStates:\n");
		for (StateType aState : endStates) {
			buf.append(aState + "\n");
		}
		
		buf.append("has spontaneousStates: " + hasStateWithSpontaneousTransition());
		return buf.toString();
	}
}
