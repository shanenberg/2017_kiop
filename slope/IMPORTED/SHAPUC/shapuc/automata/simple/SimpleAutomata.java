package shapuc.automata.simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Set;

import shapuc.util.Pair;
import shapuc.util.IdentitySet;

public abstract class SimpleAutomata<StateType, InputType> {

	protected IdentityHashMap<StateType, ArrayList<Pair<InputType, StateType>>> stateTransitionTable = new IdentityHashMap<StateType, ArrayList<Pair<InputType, StateType>>>();
	protected IdentitySet<StateType> endStates = new IdentitySet<StateType>();

	public abstract boolean hasFinished();
	
	public abstract void gotoNext(InputType input);
	public abstract void start();
	
	public void addState(StateType aState) {
		if (!stateTransitionTable.keySet().contains(aState)) {
			stateTransitionTable.put(aState, new ArrayList<Pair<InputType, StateType>>());
		}
	}
	
	public void addTransition(StateType startState, InputType input, StateType endState) {
//		System.out.println("Transition: ->" + input + "<-");
//		System.out.println("Transition: " + input.getClass());
//if (input.equals("\\")) {
//	new Exception().printStackTrace();
//}
		addState(startState);
		addState(endState);
		stateTransitionTable.get(startState).add(new Pair<InputType, StateType>(input, endState));
	}	
	
	public ArrayList<InputType> allInputsLike(StateType start, InputType input) {
		ArrayList<InputType> result = new ArrayList<InputType>();
		
		for (Pair<InputType, StateType> transition : stateTransitionTable.get(start)) {
			if (transition.getLeft().equals(input))
				result.add(transition.getLeft());
		}
		return result;
	}
	
	public ArrayList<Pair<InputType, StateType>> allTransitionsLike(StateType start, InputType input) {
		ArrayList<Pair<InputType, StateType>> result = new ArrayList<Pair<InputType, StateType>>();
		
		for (Pair<InputType, StateType> transition : stateTransitionTable.get(start)) {
			if (transition.getLeft().equals(input))
				result.add(transition);
		}
		return result;
	}	
	
	public ArrayList<Pair<InputType, StateType>> allTransitionsIdenticalTo(StateType start, InputType input) {
		ArrayList<Pair<InputType, StateType>> result = new ArrayList<Pair<InputType, StateType>>();

		for (Pair<InputType, StateType> transition : stateTransitionTable.get(start)) {
			if (transition.getLeft() == input)
				result.add(transition);
		}
		return result;
	}		
	
	StateType startState;

	public Set<StateType> getEndStates() {
		return endStates;
	}

	public void setEndStates(IdentitySet<StateType> endStates) {
		this.endStates = endStates;
	}

	public StateType getStartState() {
		return startState;
	}

	public void setStartState(StateType startState) {
		addState(startState);
		this.startState = startState;
	}
	
	public void cleanEndStates() {
		endStates = new IdentitySet<StateType>();
	}
	
	public void addEndState(StateType aState) {
		addState(aState);
		endStates.add(aState);
	}
	
	public IdentityHashMap<StateType,ArrayList<Pair<InputType,StateType>>> getStateTransitionTable() {
		return stateTransitionTable;
	}
	
	public String toString() {
		StringBuffer ret = new StringBuffer();
		ret.append("States: \n");
		for (StateType state : stateTransitionTable.keySet()) {
			ret.append("{" + state + "}\n"); 
		}
		ret.append("\n");

		ret.append("Transitions: \n");
		for (StateType state : stateTransitionTable.keySet()) {
			for(Pair<InputType, StateType> transition: stateTransitionTable.get(state)) {
				ret.append( state + " " + transition.getLeft() + " " + transition.getRight() + "\n");
			}
		}

		return ret.toString();
	}	
	
}
