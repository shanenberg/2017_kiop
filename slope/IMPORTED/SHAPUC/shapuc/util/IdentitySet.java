package shapuc.util;

import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Set;

public class IdentitySet<ElementType> implements Set<ElementType>{

	IdentityHashMap<ElementType, Object> elements = new IdentityHashMap<ElementType, Object>();
	
	public int size() {
		return elements.size();
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public boolean contains(Object o) {
		return elements.keySet().contains(o);
	}

	public Iterator<ElementType> iterator() {
		return elements.keySet().iterator();
	}

	public Object[] toArray() {
		return elements.keySet().toArray();
	}

	public <T> T[] toArray(T[] a) {
		return elements.keySet().toArray(a);
	}

	public boolean add(ElementType e) {
		elements.put(e, null);
		return true;
	}

	public boolean remove(Object o) {
		elements.remove(o);
		return false;
	}

	public boolean containsAll(Collection<?> c) {
		return elements.keySet().containsAll(c);
	}

	public boolean addAll(Collection<? extends ElementType> c) {
		for (ElementType elementType : c) {
			this.add(elementType);
		}
		return true;
	}

	public boolean retainAll(Collection<?> c) {
		boolean changed = false;
		for (Object object : c) {
			if (!this.contains(object)) {
				elements.remove(object);
				changed = true;
			}
		}
		return changed;
	}

	public boolean removeAll(Collection<?> c) {
		for (Object elementType : c) {
			this.remove(elementType);
		}
		return true;
	}

	public void clear() {
		elements = new IdentityHashMap<ElementType, Object>();
	}
	
	public IdentitySet<ElementType> clone() {
		IdentitySet<ElementType>  ret = new IdentitySet<ElementType>();
		for(ElementType e: elements.keySet()) {
			ret.add(e);
		}
		return ret;
	}

}
