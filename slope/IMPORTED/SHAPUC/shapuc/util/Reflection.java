/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection {

	/**
	 * Invokes the method (without parameters) and handles the exception.
	 * @param method
	 * @param o
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <RETURNTYPE> RETURNTYPE invokeMethodUnsave(Method method, Object o) {
		try {
			method.setAccessible(true);
			return (RETURNTYPE) method.invoke(o);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("Cannot invoke method " + 
					method.getName() + " in class " + o.getClass().getName());
		}		
	}
	
	/**
	 * Invokes the method (without parameters) and handles the exception.
	 * @param method
	 * @param o
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <RETURNTYPE> RETURNTYPE invokeMethodUnsave(Method method, Object o, Object parameter) {
		try {
			method.setAccessible(true);
			return (RETURNTYPE) method.invoke(o, new Object[] {parameter});
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("Cannot invoke method " + 
					method.getName() + " in class " + o.getClass().getName());
		}		
	}	
	
	public static void invokeVoidMethodUnsave(Method method, Object o, Object parameter) {
		try {
			method.setAccessible(true);
			method.invoke(o, new Object[] {parameter});
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("Cannot invoke method " + 
					method.getName() + " in class " + o.getClass().getName());
		}		
	}		
	
	/* Instantiates the class without parameters */
	public static <Clazz> Clazz instantiateClassUnsave(Class<Clazz> c) {
		try {
			return c.newInstance();
		} catch (Exception ex) {
			throw new RuntimeException("Cannot create instance from class " + 
					c.getName());
		}		
	}

	/* Instantiates the class without parameters */
	@SuppressWarnings("unchecked")
	public static <Clazz> Clazz instantiateClassUnsave(Class<Clazz> c, Object o) {
		try {
			Constructor<?>[] cs = c.getConstructors();
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getParameterTypes().length==1) {
					return (Clazz) cs[i].newInstance(o);
				}
			}
			throw new RuntimeException("Cannot create instance from class " + 
					c.getName() + " constructor with one parameter is missing");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("Cannot create instance from class " + 
					c.getName());
		}		
	}

	/* Instantiates the class without parameters */
	public static void setFieldUnsave(String fieldName, Object value, Object targetObject) {
		try {
			Field f = targetObject.getClass().getField(fieldName);
			f.setAccessible(true);
			f.set(targetObject, value);
		} catch (Exception ex) {
			throw new RuntimeException("Cannot set field " + 
					fieldName + " on class " + targetObject.getClass().getName());
		}		
	}

	public static void setMarkUnsafe(BufferedReader reader) {
		try {
			reader.mark(1);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}

	public static void resetUnsafe(BufferedReader reader) {
		try {
			reader.reset();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}	
	
}
