/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.refvis.examples.visitor01;

import java.util.ArrayList;
import java.util.List;

import shapuc.refvis.TreeNode;

/**
 * Traverses a simple Tree with A,B, and CNodes. It prints out a string that should
 * what objects are traversed.
 * 
 * @author stefan
 *
 */

interface NodeIF extends TreeNode<NodeIF> {
	public String printString(String s);
	public List<NodeIF> getChildren();
}

class NodeImpl implements NodeIF {
	public List<NodeIF> children = new ArrayList<NodeIF>();
	public List<NodeIF> getChildren() {
		return children;
	}	
	public String printString(String s) {
		return s;
	}
}

class NodeA extends NodeImpl {public String aText = "A";}
class NodeB extends NodeImpl {public String bText = "B";}
class NodeC extends NodeImpl {public String cText = "C";}

public class Main {
	public static void main(String[] args) {
		Visitor v = new Visitor();
		NodeA a = new NodeA();
		a.children.add(new NodeB());
		a.children.add(new NodeB());
		a.children.add(new NodeB());
		
		((NodeB) a.children.get(0)).children.add(new NodeA());
		((NodeB) a.children.get(1)).children.add(new NodeB());
		((NodeB) a.children.get(2)).children.add(new NodeC());
	
		v.invokeVisit(a);
		System.out.println(v.writer.toString());
		
	}
}
