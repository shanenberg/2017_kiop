/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.refvis.examples.visitor01;

import java.io.StringWriter;

import shapuc.refvis.custom.VisitorReflective;

public class Visitor extends VisitorReflective<NodeIF, Object>{

	StringWriter writer = new StringWriter();
	
	public Object visit(NodeA a) {
		writer.write(a.printString(a.aText));
		for (NodeIF child : a.getChildren()) {
			this.invokeVisit(child);
		}
		return null;
	}
	
	public Object visit(NodeB b) {
		writer.write(b.printString(b.bText));
		for (NodeIF child : b.getChildren()) {
			this.invokeVisit(child);
		}
		return null;
	}
	
	public Object visit(NodeC c) {
		writer.write(c.printString(c.cText));
		for (NodeIF child : c.getChildren()) {
			this.invokeVisit(child);
		}
		return null;
	}	
	
}
