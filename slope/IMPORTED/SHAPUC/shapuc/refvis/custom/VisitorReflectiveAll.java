/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package shapuc.refvis.custom;

import java.util.List;

import shapuc.refvis.TreeNode;
import shapuc.refvis.exceptions.InvalidChildException;

public class VisitorReflectiveAll<ReturnType> extends VisitorReflectiveAbstract<Object, ReturnType>{

	public ReturnType invokeVisit(Object anObject) {
		if (anObject instanceof List)
			return this.invokeVisit((List) anObject);
		else
			return super.invokeVisit(anObject);
	}
	
	public ReturnType invokeVisit(List listTreeNode) {
		ReturnType ret = null;
		for (Object childObject : listTreeNode) {
//			System.out.println("invokeVisit: " + childObject);
			if (isObjectOfInterest(childObject)) {
				if (childObject instanceof List) {
					ret = (ReturnType) this.invokeVisit((List) childObject);
				} else {
					ret = (ReturnType) this.invokeVisit((Object) childObject);
				}
			}
//			System.out.println("done invokeVisit: " + childObject);
		}
		return ret;
	}
	
	/** 
	 * Returns true if the delivered field should be visitable. Subclasses can override
	 * it to remove elements from the list of fieldsOfInterest.
	 */
	protected boolean isObjectOfInterest(Object anObject) {
		return anObject!=null && (visitingNodeClass().isAssignableFrom(anObject.getClass()) ||
				List.class.isAssignableFrom(anObject.getClass()));
	}		
	
	/** 
	 * This method if overridden by classes that extent TreeNode in order to 
	 * find appropriate fields of interest.
	 * 
	 * @return
	 */
	protected Class visitingNodeClass() {
		return TreeNode.class;
	}

	@Override
	public List<Object> getChildren(Object o) {
		if (o instanceof List) {
//			System.out.println("Am a list");
			return (List) o;
		} else if (o instanceof TreeNode) {
//			System.out.println("getChildren");
			return ((TreeNode) o).getChildren();
		}
		throw new InvalidChildException();
	}
	

}
