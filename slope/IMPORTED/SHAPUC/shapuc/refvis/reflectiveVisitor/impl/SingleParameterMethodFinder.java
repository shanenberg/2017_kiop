/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/
package shapuc.refvis.reflectiveVisitor.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import shapuc.refvis.exceptions.MethodNotFoundException;
import shapuc.refvis.exceptions.VisitorAmbiguityException;

/**
 * Finds the method to be called from the visitor. 
 *   - methodName is the name of the method to be called,
 *   - visitor is the visitor type
 *   - parameterType is the parameter type
 * 
 * @author stefan
 *
 */
public class SingleParameterMethodFinder {
	
	public final String methodName;
	public final Class visitorClass;
	public final Class parameterType;
	
	public SingleParameterMethodFinder(String methodName, Class visitor, Class parameterType) {
		super();
		this.methodName = methodName;
		this.visitorClass = visitor;
		this.parameterType = parameterType;
	}
	
	/**
	 * Used by the visitor in order to find the appropriate method.
	 * @return
	 */
	public Method findMethod() {
		try {
			return visitorClass.getMethod(methodName, parameterType);
		} catch (Exception e) {
			// OK, so far no appropriate Method found....
			Method[] tmp_ms = visitorClass.getMethods();
			ArrayList<Method> methodCandidates = new ArrayList<Method>(); 
			for (int i = 0; i < tmp_ms.length; i++) {

				// Method name is "visit"
				if (tmp_ms[i].getName().equals(methodName))
					// Method has one parameter
					if (tmp_ms[i].getParameterTypes().length == 1)  
						// Parameter is subtype of TreeNode
						if (tmp_ms[i].getParameterTypes()[0].isAssignableFrom(parameterType))
							methodCandidates.add(tmp_ms[i]);
			}
			
			return bestMethodFor(methodCandidates, new Class[]{parameterType});
		}
		
	}

	/**
	 * BestMethodFor determine one method among the method candidates. 
	 * 
	 * If more than one method is found, it throws an ambiguity exception. 
	 * If no method is found, it selects all superclasses and interfaces and recursively calls itself.
	 * 
	 * @param methodCandidates: List of all methods
	 * @param classes
	 * @return
	 */
	private Method bestMethodFor(ArrayList<Method> methodCandidates, Class[] classes) {
		
		ArrayList<Method> bestMethods = new ArrayList(); 

		for (int i = 0; i < classes.length; i++) {
			for (int j = 0; j < methodCandidates.size(); j++) {
				if(methodCandidates.get(j).getParameterTypes()[0]==classes[i])
					bestMethods.add(methodCandidates.get(j));
			}
		}

		if (bestMethods.size()==1) {
			return bestMethods.get(0);
		} else if (bestMethods.size()>1) {
			throw createAbmbiguityException(bestMethods);
		} else if(bestMethods.size()==0) {
			Class[] directSuperTypes = directSuperTypes(classes);
			if (directSuperTypes.length>0)
				return bestMethodFor(methodCandidates, directSuperTypes);
			else 
				throw new MethodNotFoundException("No method " + methodName + " parameter type: " + parameterType.getName() + " for type " + visitorClass.getName());
		}
		return null;
	}

	/**
	 * Creates an array of classes that are the superclass and the directly implemented interfaces
	 * of the passed classes.
	 * 
	 * @param classes
	 * @return
	 */
	private Class[] directSuperTypes(Class[] classes) {
		
		ArrayList<Class> directSupertypes = new ArrayList<Class>();

		for (int i = 0; i < classes.length; i++) {
			Class superclass = classes[i].getSuperclass();
			if (superclass!=null) directSupertypes.add(superclass); // Add Superclass
			directSupertypes.addAll(Arrays.asList(classes[i].getInterfaces())); // Add Superinterfaces
		}
		
		Class[] ret = new Class[directSupertypes.size()];
		return directSupertypes.toArray(ret);
	}

	private VisitorAmbiguityException createAbmbiguityException(ArrayList<Method> bestMethods) {
		return new VisitorAmbiguityException("There is more than one possible visit-candidate for");
	}
}
