/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane;

import slowscane.scanners.KeywordScanner;
import slowscane.scanners.SingleCharScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.FloatLiteralScanner;
import slowscane.scanners.lib.JavaBlockCommentScanner;
import slowscane.scanners.lib.JavaCharLiteralScanner;
import slowscane.scanners.lib.JavaCharLiteralScannerUncoded;
import slowscane.scanners.lib.JavaHexaIntegerLiteralScanner;
import slowscane.scanners.lib.JavaIdentifierScanner;
import slowscane.scanners.lib.JavaIntegerLiteralScanner;
import slowscane.scanners.lib.JavaStringLiteralScanner;

public class TokensLib {
	public final SingleCharScanner WHITESPACE = new SingleCharScanner(' ');
	public final SingleCharScanner TAB = new SingleCharScanner('\t');
	public final SingleCharScanner NEWLINE = new SingleCharScanner('\n');
	public final SingleCharScanner OPEN_CURELY_BRACKET = new SingleCharScanner('{');
	public final SingleCharScanner OPEN_BRACKET = new SingleCharScanner('(');
	public final SingleCharScanner CLOSED_BRACKET = new SingleCharScanner(')');
	public final SingleCharScanner CLOSED_CURLY_BRACKET = new SingleCharScanner('}');
	
	public final SingleCharScanner ANGLE_BRACKET_OPEN = new SingleCharScanner('<');
	public final SingleCharScanner ANGLE_BRACKET_CLOSE = new SingleCharScanner('>');
	
	public final SingleCharScanner CARET = new SingleCharScanner('^');
	public final SingleCharScanner SEMICOLON = new SingleCharScanner(';');
	public final SingleCharScanner EPSILON = new SingleCharScanner('ε');
	public final SingleCharScanner LAMBDA = new SingleCharScanner('λ');
	public final SingleCharScanner COLON = new SingleCharScanner(':');
	public final SingleCharScanner DOT = new SingleCharScanner('.');
	public final SingleCharScanner COMMA = new SingleCharScanner(',');
	public final SingleCharScanner EXCLMARK = new SingleCharScanner('!');
	public final SingleCharScanner OR = new SingleCharScanner('|');
	public final SingleCharScanner EQUALS = new SingleCharScanner('=');
	public final SingleCharScanner QUOTATION_MARK = new SingleCharScanner('"');
	public final SingleCharScanner QUESTION_MARK = new SingleCharScanner('?');

	public final SingleCharScanner PLUS = new SingleCharScanner('+');
	public final SingleCharScanner MINUS = new SingleCharScanner('-');
	public final SingleCharScanner MULT = new SingleCharScanner('*');
	public final SingleCharScanner DIV = new SingleCharScanner('/');
	public final SingleCharScanner HASH = new SingleCharScanner('#');

	public final TokenReader<String> JAVA_IDENTIFIER = new JavaIdentifierScanner();
	public final TokenReader<Integer> JAVA_INTEGER_LITERAL = new JavaIntegerLiteralScanner();
	public final TokenReader<Integer> JAVA_HEXAINT_LITERAL = new JavaHexaIntegerLiteralScanner();
	public final TokenReader<String> JAVA_STRING_LITERAL = new JavaStringLiteralScanner();
	public final TokenReader<String> JAVA_CHARACTER_LITERAL = new JavaCharLiteralScanner();
	public final TokenReader<String> JAVA_CHARACTER_LITERAL_UNCODED = new JavaCharLiteralScannerUncoded();

	public final TokenReader<String> JAVA_BLOCK_COMMENT = new JavaBlockCommentScanner();
	
	public final TokenReader<Float> FLOAT_LITERAL = new FloatLiteralScanner();

	public final TokenReader<String> KEYWORD_SHORT_EQUAL_ARROW = new KeywordScanner("=>");
	public final TokenReader<String> KEYWORD_SHORT_ARROW = new KeywordScanner("->");
	public final TokenReader<String> KEYWORD_ARROW = new KeywordScanner("->");
	public final TokenReader<String> KEYWORD_LONG_ARROW = new KeywordScanner("--->");
	public final TokenReader<String> KEYWORD_PACKAGE = new KeywordScanner("package");
	public final TokenReader<String> KEYWORD_CLASS = new KeywordScanner("class");
	public final TokenReader<String> KEYWORD_VOID = new KeywordScanner("void");
	public final TokenReader<String> KEYWORD_Int = new KeywordScanner("Int");
	public final TokenReader<String> KEYWORD_Hexa = new KeywordScanner("Hexa");
	public final TokenReader<String> KEYWORD_List = new KeywordScanner("List");
	public final TokenReader<String> KEYWORD_String = new KeywordScanner("String");
	public final TokenReader<String> KEYWORD_Unique = new KeywordScanner("Unique");
	public final TokenReader<String> KEYWORD_NULL = new KeywordScanner("NULL");
	public final TokenReader<String> KEYWORD_NotNull = new KeywordScanner("NotNull");
	public final TokenReader<String> KEYWORD_Ref = new KeywordScanner("Ref");
	public final TokenReader<String> KEYWORD_Map = new KeywordScanner("Map");
}
