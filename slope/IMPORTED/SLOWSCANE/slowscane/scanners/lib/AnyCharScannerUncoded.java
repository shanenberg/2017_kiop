package slowscane.scanners.lib;

import java.util.HashMap;

import shapuc.util.SHAPUCCollections;
import slowscane.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;

public class AnyCharScannerUncoded extends TokenReader<String> {

	Character escapingChar;
	
	HashMap<Character, Character> escapeTranslations = new HashMap<Character, Character>();
	
	
	public AnyCharScannerUncoded() {
		super();
		this.escapingChar = null;
	}	
	
	public AnyCharScannerUncoded(Character escapeChar, Character[][] escapedChars) {
		super();
		this.escapingChar = escapeChar;
		escapeTranslations = SHAPUCCollections.translate2DimArrayIntoHashmap(escapedChars);
	}


	@Override
	public Token<String> scanNext(PositionStream<?> stream) {
//System.out.println("pos" + stream.currentPosition);		
		Character currentChar = (Character) stream.next();
		if (currentChar.equals(escapingChar)) {
			Character escapedChar = (Character) stream.next();
//System.out.println("create token (escaped): " +  "" + escapingChar + escapedChar);		
			return this.createToken(stream, "" + escapingChar + escapedChar);
		}
//System.out.println("create token: " + "" + currentChar);		
		return this.createToken(stream, "" + currentChar);
	}

	

}
