/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.scanners.lib;

import slowscane.ScanException;
import slowscane.Token;
import slowscane.TokenType;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;


public class JavaCharLiteralScanner extends TokenReader<String> {

	Character escapingChar = '\\';
	
	Object[][] escapedChars = new Object[][] { 
		new Object[]{'t', '\t'},
		new Object[]{'n', '\n'}
	};
	
	Object[] forbidden = new Object[] {};	
	
	
	{ this.tokenType = new TokenType("SLOPE_JavaCharacterLiteral"); }
	

	public Token<String> scanNext(PositionStream<?> stream) {
		StringBuffer ret = new StringBuffer();
		Object next = (Character) stream.next();

		if (!next.equals('\''))
			throw new ScanException("No Java String Literal", stream.stuckExceptionString());
		ret.append('\'');

		TokenReader<?> anyCharScanner = createAnyCharScanner();
		Token<?> n2 = anyCharScanner.scanNext(stream);

//		System.out.println("n2:" + n2);		
//		System.out.println("n2:" + n2.getValueString());		
//		
//		
//		next = (Character) n2.getValueString().charAt(0);
//			for (int i = 0; i < forbidden.length; i++) {
//				if (next==forbidden[i]) 
//					throw new ScanException("Forbidden token for JavaCharLiteral", stream.stuckExceptionString());
//			}
			
		ret.append(n2.getValueString());

		next = (Character) stream.next();
		if (next.equals('\'')) {
			ret.append('\'');
//System.out.println("token: " + createToken(stream, ret.toString()));
			return createToken(stream, ret.toString());
		}

		throw new ScanException("No Java String Literal", stream.stuckExceptionString());

	}

	public TokenReader<?> createAnyCharScanner() {
		return new AnyCharScanner(escapingChar, escapedChars);
	}
	
	private boolean isEscapeChar(Character next) {
		return next == '\\';
	}
	
	public boolean isConstantToken() {
		return false;
	}	

}
