/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.scanners.lib;

import slowscane.ScanException;
import slowscane.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;


public abstract class NumberLiteralScanner<NumberType extends Number> extends TokenReader<NumberType> {
	
	@Override
	public Token<NumberType> scanNext(PositionStream<?> stream) {
		String s = readNumberStringFromStream(stream);
		
		return createToken(stream, convertStringToNumber(s));
	}

	public abstract NumberType convertStringToNumber(String s);
	
	protected String readNumberStringFromStream(PositionStream<?> stream) {
		StringBuilder ret = new StringBuilder();
		Character next = (Character) stream.next();

		if (!isIntegerStart(next))
			throw new ScanException("No Java Integer Literal", stream.stuckExceptionString());

		ret.append(next);

		while (!stream.isEOF() && isDigit(next = (Character) stream.next())) {
			ret.append(next);
		}

		if (!isDigit(next))
				stream.currentPosition--;
		return ret.toString();
	}
	
	public boolean isIntegerStart(char c) {
		return Character.isDigit(c) && c!='0';
	}
	
	public boolean isDigit(char c) {
		return Character.isDigit(c);
	}
	
}
