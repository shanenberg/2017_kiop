/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.streams;

import java.util.List;

import shapuc.util.SHAPUCCollections;

public class StringBufferStream extends PositionStream<Character> {

	StringBuffer buffer;
	
	@Override
	public int length() {
		return buffer.length();
	}

	public StringBufferStream(StringBuffer buffer) {
		super();
		this.buffer = buffer;
	}
	
	public StringBufferStream(String inString) {
		super();
		this.buffer = new StringBuffer(inString);
	}	

	@Override
	public Character next() {
		Character ret = this.current();
		this.currentPosition++;
		return ret;
	}

	@Override
	public Character current() {
		return buffer.charAt(this.currentPosition);
	}

	@Override
	public List<Character> elements(int from, int to) {
		char[] _ret = new char[to-from+1];
		buffer.getChars(from, to, _ret, 0);
		return SHAPUCCollections.unsaveToList(_ret);
	}
	
	public String subString(int from, int to) {
		char[] _ret = new char[to-from+1];
		buffer.getChars(from, to, _ret, 0);
		return new String(_ret);
	}

	public String stuckExceptionString() {
		int end = Math.max(0, super.currentPosition);
		String pre = buffer.toString().substring(0, end);
		return "\"" + pre + "\">>>\"" + buffer.toString().substring(super.currentPosition) + "\"";
	}	
	
	public String stuckExceptionStringUpTo(int position) {
		String pre = buffer.toString().substring(0, position);
		String post = position+1<=buffer.length()?buffer.toString().substring(position + 1, buffer.length()):"";
		return "\"" + pre + "\">>>\"" + post + "\"";
	}
		
}
