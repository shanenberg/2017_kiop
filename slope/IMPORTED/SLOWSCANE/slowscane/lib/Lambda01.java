/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package slowscane.lib;

import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

/**
 * Defines the scanner for a simple lamda-language, consisting of: ' ', '.',
 * 'λ', <identifier>, '(', (')
 * 
 * Example 1: "λ abc (λx.x)"
 * Example 2: "(λx.x)"
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 */
public class Lambda01 extends ExamplesTestCase {

	public static class Lambda01Tokens extends Tokens {
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; }
		@Token public TokenReader<?> DOT() { return lib.DOT; }
		@Keyword public TokenReader<?> LAMBDA() { return lib.LAMBDA; }
		@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
		@Token public TokenReader<String> JAVA_IDENTIFIER() {	return lib.JAVA_IDENTIFIER; }
	}

	public void test01() {
		StringBufferStream stream = new StringBufferStream("λ abc (λx.x)");

		Scanner<Lambda01Tokens, StringBufferStream> 
			scanner = new Scanner<Lambda01Tokens, StringBufferStream>(new Lambda01Tokens(), stream);
		
		TokenStream res = scanner.scan();
		
		checkValueNName('λ', "LAMBDA", res, 0);
		checkValueNName("abc", "JAVA_IDENTIFIER", res, 1);
		checkValueNName('(', "OBRACKET", res, 2);
		checkValueNName('λ', "LAMBDA", res, 3);
		checkValueNName("x", "JAVA_IDENTIFIER", res, 4);
		checkValueNName('.', "DOT", res, 5);
		checkValueNName("x", "JAVA_IDENTIFIER", res, 6);
		checkValueNName(')', "CBRACKET", res, 7);

		assertEquals(8, res.length());
		
	}
	
	public void test02() {
		StringBufferStream stream = new StringBufferStream("(λx.x)");

		Scanner<Lambda01Tokens, StringBufferStream> 
			scanner = new Scanner<Lambda01Tokens, StringBufferStream>(new Lambda01Tokens(), stream);
		
		TokenStream res = scanner.scan();
		
		checkValueNName('(', "OBRACKET", res, 0);
		checkValueNName('λ', "LAMBDA", res, 1);
		checkValueNName("x", "JAVA_IDENTIFIER", res, 2);
		checkValueNName('.', "DOT", res, 3);
		checkValueNName("x", "JAVA_IDENTIFIER", res, 4);
		checkValueNName(')', "CBRACKET", res, 5);

		assertEquals(6, res.length());
		
	}	

}
