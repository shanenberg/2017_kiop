/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import shapuc.util.Reflection;
import shapuc.util.SHAPUCCollections;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.annotations.TokenLevel;
import slowscane.scanners.TokenReader;

public class Tokens {
	public TokensLib lib = new TokensLib(); 
	
	Class<?>[] annotationTypes = new Class<?>[] {Keyword.class, Token.class, TokenLevel.class};
	
	/**
	 * Reads the TokenTypes from the Tokens. 
	 * 
	 * A TokenType is a method that has the annotation @TokenType and has the return type @ElementScanner
	 * 
	 * Change: I added the TokenLevel. Now, a keyword has the TokenLevel(50) and all others have the 
	 * TokenLeve(100)
	 * 
	 * @return
	 */
	
	public List<TokenReader<?>> getAllTokenTypes() {
		HashMap<Integer, ArrayList<TokenReader<?>>> map = new HashMap<Integer, ArrayList<TokenReader<?>>>();
		
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		
		readAndAddTokenReaders(map);
		
		ArrayList ints = new ArrayList();
		ints.addAll(map.keySet());

		
		Collections.sort(ints, new Comparator() {
			public int compare(Object o1, Object o2) {
				if (((Integer) o1) > ((Integer) o2)) return 1;
				if (((Integer) o1).equals(((Integer) o2))) return 0;
				return -1;
			}
		});
		
		for (Integer i: (ArrayList<Integer>) ints) {
			ret.addAll(map.get(i));
		}
		
//		ret.addAll(ints);
		
//		ret.addAll(this.getKeywordTokenTypes());
//		ret.addAll(this.getTokenTypes());
		return ret;
	}	
	
	private void readAndAddTokenReaders(HashMap<Integer, ArrayList<TokenReader<?>>> tokenReaders) {
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			addMethodIfTokenAnnotationWithRightReturnType(tokenReaders, methods[i]);
		}
	}

	private void addMethodIfTokenAnnotationWithRightReturnType
					(HashMap<Integer, ArrayList<TokenReader<?>>> tokenReaders, Method method) {
		// If returnType is not a TokenReader, then don't care
		if (!TokenReader.class.isAssignableFrom(method.getReturnType())) return;

		Annotation tokenType = getTokenAnnotation(method);
		// if there is no tokenType, then don't care
		if (tokenType==null) return;
		
		if (tokenType.annotationType()==TokenLevel.class) {
			TokenLevel l = (TokenLevel) tokenType;
			TokenReader<?> tokenReader = Reflection.<TokenReader<?>>invokeMethodUnsave(method, this);
			HashMap unsafeTokenReaders = (HashMap) tokenReaders;
			SHAPUCCollections.putWithDefaultList(unsafeTokenReaders, (Integer) l.value(), tokenReader, ArrayList.class);
		}
		
	}

	private Annotation getTokenAnnotation(Method method) {
		Annotation[] annotations = method.getAnnotations();
		
		for (int j = 0; j < annotations.length; j++) {
				if(annotations[j].annotationType() == Keyword.class) {
					return new TokenLevel() {
						public Class<? extends Annotation> annotationType() {	return TokenLevel.class; }
						public int value() {
							return 50;
						}
					};
				}
				
				if(annotations[j].annotationType() == Token.class) {
					return new TokenLevel() {
						public Class<? extends Annotation> annotationType() {	return TokenLevel.class; }
						public int value() {
							return 100;
						}
					};
				}

				if(annotations[j].annotationType() == TokenLevel.class) {
					return annotations[j];
				}				
				
		}
		return null;
	}

	private boolean hasTokenAnnotationWithRightType(Method method) {
		if (!TokenReader.class.isAssignableFrom(method.getReturnType())) return false;
		
//		for (int i = 0; i < annotationTypes.length; i++) {
//			if ()
//		}
		
//			
		return hasAnnotationType(Separator.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
//		return false;
	}

	public List<TokenReader<?>> getKeywordTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (fulfillsKeywordConstraints(methods[i])) {
				TokenReader<?> s = Reflection.<TokenReader<?>>invokeMethodUnsave(methods[i], this);
				ret.add(s);
			}
		}
		return ret;
	}	

	/**
	 * Reads the TokenTypes from the Tokens. 
	 * 
	 * A TokenType is a method that is annotation with @TokenType and has the return type @ElementScanner
	 * 
	 * @return
	 */
	public List<TokenReader<?>> getTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (fulfillsTokenConstraints(methods[i])) {
				TokenReader<?> tokenReader = Reflection.<TokenReader<?>>invokeMethodUnsave(methods[i], this);
				ret.add(tokenReader);
			}
		}
		return ret;
	}
	
	public List<TokenReader<?>> getSkippedTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (fulfillsSkippedTokenConstraints(methods[i])) {
				ret.add( Reflection.<TokenReader<?>>invokeMethodUnsave(methods[i], this));
			}
		}
		return ret;
	}	
	
	private boolean fulfillsTokenConstraints(Method method) {
		return hasAnnotationType(Token.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
	}

	private boolean fulfillsSkippedTokenConstraints(Method method) {
		return hasAnnotationType(Separator.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
	}
	
	
	
	private boolean fulfillsKeywordConstraints(Method method) {
		return hasAnnotationType(Keyword.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
	}
	
	private boolean hasAnnotationType(Class<?> annotationClass, Method method) {
		Annotation[] annotations = method.getAnnotations();
		
		for (int j = 0; j < annotations.length; j++) {
			if (annotations[j].annotationType()==annotationClass) {
				return true;
			}
		}
		return false;
	}
}
