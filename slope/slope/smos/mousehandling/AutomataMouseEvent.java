package smos.mousehandling;

import shapuc.graphics.points.Point2D;

public interface AutomataMouseEvent {

	Point2D getPosition();

}
