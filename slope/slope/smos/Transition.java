/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package smos;

public class Transition<STATE_ENUMERATION_TYPE, MESSAGE_ENUMERATION_TYPE>  {
	
	public final boolean isSpontaneous;
	public final STATE_ENUMERATION_TYPE fromState;
	public final  MESSAGE_ENUMERATION_TYPE message;
	public final TransitionCondition transitionCondition;
	public final AutomataCmd transitionCommand;
	public final STATE_ENUMERATION_TYPE toState;
	public final AutomataCmd nextStateCommand;
	
	public Transition(boolean isSpontaneous, MESSAGE_ENUMERATION_TYPE message,
			STATE_ENUMERATION_TYPE fromState, STATE_ENUMERATION_TYPE toState,
			TransitionCondition transitionCondition, AutomataCmd transitionCommand,
			AutomataCmd nextStateCommand) {
		super();
		this.isSpontaneous = isSpontaneous;
		this.message = message;
		this.fromState = fromState;
		this.toState = toState;
		this.transitionCondition = transitionCondition;
		this.transitionCommand= transitionCommand;
		this.nextStateCommand= nextStateCommand;
	}

	public boolean conditionHolds(Context c) {
		if (transitionCondition==null) return true;
		return transitionCondition.doCheck(c);
	}
	
	public void invokeTransitionCmd(Context c) {
		if (transitionCommand==null) return;
		transitionCommand.doCommand(c);
		
	}
	
	public void invokeNextStateCmd(Context c) {
		if (nextStateCommand==null) return;
		nextStateCommand.doCommand(c);
		
	}	
	
	
}
