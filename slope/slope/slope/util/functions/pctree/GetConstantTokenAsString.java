package slope.util.functions.pctree;

import shapuc.refvis.SingleParamDispatcher;
import slope.PC;
import slope.lang.TOKEN.TokenParserConstructor;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.SingleCharScanner;
import slowscane.scanners.StringElementScanner;
import slowscane.scanners.TokenReader;

public class GetConstantTokenAsString extends SingleParamDispatcher<String, TokenReader> {

	public GetConstantTokenAsString() {
		super("getToken");
	}

	public String getToken(TokenReader pc) {
		throw new RuntimeException("Invalid use of IsConstantTokenReader on " + pc.getClass().getName());
	}

	public String getToken(KeywordScanner pc) {
		return pc.keyword;
	}

	public String getToken(SingleCharScanner pc) {
		return Character.toString(pc.character);
	}
}