package slope;

import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.DynamicGrammarParserFactory;

public class DynamicParsing {
	
	public static void main(String[] args) {
		
		System.out.println("Start DynamicParsing");
		
		String grammarString = shapuc.IO.IO.readFileIntoString(args[0]);
		DynamicGrammarParser p = new DynamicGrammarParserFactory().createParserFromString(grammarString);
		
		for (int i = 1; i < args.length; i++) {
			p.parse(args[i]);
			System.out.println("  done: " + args[i]);
		}

		System.out.println("End DynamicParsing");
	}
	
}
