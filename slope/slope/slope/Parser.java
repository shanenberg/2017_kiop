/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import java.util.ArrayList;
import java.util.List;

import shapuc.util.Reflection;
import slope.lang.AND.ANDParser;
import slope.lang.BIND.BINDPC;
import slope.lang.NFOLD.NFOLDParser;
import slope.lang.OPTIONAL.OPTIONALParser;
import slope.lang.OPTIONAL.XXXOPTIONALParserConstructor;
import slope.lang.OR.ORParser;
import slope.lang.SET.NULLParserConstructor;
import slope.lang.SET.SETParserConstructor_NoSetParameter;
import slope.lang.SET.SYSOParserConstructor;
import slope.lang.SET.SetTokenValueParserConstructor;
import slope.lang.TOKEN.TokenParserConstructor;
import slowscane.Tokens;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.SingleCharScanner;
import slowscane.scanners.TokenReader;

public class Parser<TOKENS extends Tokens> {

//	Scanner<_TOKENS, PositionStream<?>> scanner;
//	
//	public _TOKENS token;
	
	/**
	 * Returns a TokenParser. The TokenParser 
	 * @param elementScanner
	 * @return
	 */
	public <TOKEN_VALUE_TYPE> TokenParserConstructor<TOKEN_VALUE_TYPE> TOKEN(
			final TokenReader<TOKEN_VALUE_TYPE> elementScanner) {
		return new TokenParserConstructor<TOKEN_VALUE_TYPE>(elementScanner);
	}
	
	public <TOKEN_VALUE_TYPE> SETParserConstructor_NoSetParameter TOKEN(
			final TokenReader<TOKEN_VALUE_TYPE> elementScanner, Cmd setObject) {
		return new SETParserConstructor_NoSetParameter(TOKEN(elementScanner), setObject);
	}	
	
	public <TOKEN_VALUE_TYPE> SetTokenValueParserConstructor<TOKEN_VALUE_TYPE> TOKEN(
			final TokenReader<TOKEN_VALUE_TYPE> elementScanner, SetCmd<TOKEN_VALUE_TYPE> setObject) {
		return new SetTokenValueParserConstructor<TOKEN_VALUE_TYPE>(TOKEN(elementScanner), setObject);
	}		
	
	
//	public SETParserConstructor_NoSetParameter set(PC<? extends ElementParser<? extends TreeNode>> parserConstructor, Cmd setObject) {
//		return new SETParserConstructor_NoSetParameter(parserConstructor, setObject);
//	}
//	public <TOKEN_VALUE_TYPE> SetTokenValueParserConstructor<TOKEN_VALUE_TYPE> TOKEN(
//			final TokenReader<TOKEN_VALUE_TYPE> elementScanner, SETObject0 set) {
//		return new SetTokenValueParserConstructor<TOKEN_VALUE_TYPE>(TOKEN(elementScanner), set);
//	}		
	
	public TokenParserConstructor<String> TOKEN(String s) {
		final KeywordScanner keywordScanner = new KeywordScanner(s);
		return new TokenParserConstructor<String>(keywordScanner);
	}	

	public SetTokenValueParserConstructor<String> TOKEN(String s, SetCmd<String> set) {
		return new SetTokenValueParserConstructor<String>(TOKEN(s), set);
	}

	public SetTokenValueParserConstructor<Character> TOKEN(Character s, SetCmd<Character> set) {
		return new SetTokenValueParserConstructor<Character>(TOKEN(s), set);
	}
	
	public SetTokenValueParserConstructor<Character> TOKEN(char s, SetCmd<Character> set) {
		return new SetTokenValueParserConstructor<Character>(TOKEN(s), set);
	}	
	
	public TokenParserConstructor<Character> TOKEN(Character c) {
		@SuppressWarnings("unchecked")
		TOKENS tokens = ((TOKENS) Reflection.instantiateClassUnsave(this.getClass().getEnclosingClass()));
		List<TokenReader<?>> tts = tokens.getAllTokenTypes();
		
		for (final TokenReader<?> tokenReader : tts) {
			if (tokenReader instanceof SingleCharScanner) {
				if (c.equals(((SingleCharScanner) tokenReader).character)) {
					return new TokenParserConstructor<Character>((TokenReader<Character>) tokenReader);
				};
			}
		}
		
		throw new ParserDefinitionException("Token " + c + " not defined in Scanner");
		
	}	

	public ParserConstructor_N<ORParser>  OR(PC<?> ... parserConstructors) {
		return new ParserConstructor_N<ORParser>("OR", ORParser.class, parserConstructors);
	}
	
	public ParserConstructor_N<ANDParser> AND(PC<?> ... parserConstructors) {
		return new ParserConstructor_N<ANDParser>("AND", ANDParser.class, parserConstructors);
	}	
	
	public Rule OPTIONAL(final PC<?> optional) {
		return new Rule() { public PC pc() { 				
			return new ParserConstructor_1<OPTIONALParser>("OR", OPTIONALParser.class, optional);
		}};
	}			
	
	public Rule WITH_OPTIONAL(final PC<?> mustBe, final PC<?> optional) {
		return new Rule() { public PC pc() { 				
			return OR(AND(mustBe, optional), mustBe);
		}};
	}		
	
	public Rule NFOLD0(PC<?> parserConstructor) {
		return OPTIONAL(NFOLD(parserConstructor));
	}	
	
	public ParserConstructor_1<NFOLDParser> NFOLD(PC<?> parserConstructor) {
		return new ParserConstructor_1<NFOLDParser>("NFOLD", NFOLDParser.class, parserConstructor);
	}
	
	public SETParserConstructor_NoSetParameter NFOLD(PC<?> parserConstructor, Cmd cmd) {
		return new SETParserConstructor_NoSetParameter(parserConstructor, cmd);
	}

	public <BOUNDED_OBJECT_TYPE> BINDPC<BOUNDED_OBJECT_TYPE> BIND(BOUNDED_OBJECT_TYPE boundedObject, PC parserConstructor) {
		return new BINDPC<BOUNDED_OBJECT_TYPE>(boundedObject, parserConstructor);
	}
//
//	public <TREENODETYPE extends TreeNode> SetTokenValueParserConstructor<TREENODETYPE> set(PC<? extends ElementParser<TREENODETYPE>> parserConstructor, SETObject1<TREENODETYPE> setObject) {
//		return new SetTokenValueParserConstructor<TREENODETYPE>(parserConstructor, setObject);
//	}

	public NULLParserConstructor set(Cmd cmd) {
		return new NULLParserConstructor(cmd);
	}
	
	/** NOTE! This method only makes sense if the passed object is final!!!
	 */
	public <T> NULLParserConstructor result(
			final SetCmd<T> resultSetter,
			final T appl) {
		return new NULLParserConstructor(new ResultSetting<T>(resultSetter, appl));
	};	
	
	/** NOTE! This method only makes sense if the passed object is final!!!
	 */
	public <T> NULLParserConstructor result(
			final SetCmd<T> resultSetter,
			final GetCmd<? extends T> appl) {
		return new NULLParserConstructor(new LateResultSetting<T>(resultSetter, appl));
	};		
	
	public <T> SYSOParserConstructor syso(Object toPrint) {
		return new SYSOParserConstructor(toPrint);
	};		
	
	public SETParserConstructor_NoSetParameter set(PC<? extends ElementParser<? extends TreeNode>> parserConstructor, Cmd setObject) {
		return new SETParserConstructor_NoSetParameter(parserConstructor, setObject);
	}
	
	public <T> ResultSetting<T> setResult(SetCmd<T> setter, T value) {
		return new ResultSetting<T>(setter, value);
	}
	
	public <T> SetCmd<T> addResultSetter(List<T> targetList) {
		return new ResultAdder<T>(targetList);
	}	
//	
//	public <TOKENTYPE> SetValFromTokenReaderParserConstructor<TOKENTYPE> SET(TokenReader<TOKENTYPE> tokenReader, SETObject<TOKENTYPE> so) {
//		return new SetValFromTokenReaderParserConstructor<TOKENTYPE>(tokenReader, so);
//	}

	
}
