/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import java.util.ArrayList;
import java.util.List;

/**
 * ParserConstructorConstructors (PCCs) are returned by methods in Parsers.
 * 
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 */
public abstract class Rule implements PC {

	public final String pcTypeName;
	
//	{
//		/* The third element of the currentThread is the Methodname...
//		 * I know.....this is really, really ugly....that's life.
//		 */
//		
//	}
	
	public abstract PC pc();

    public RootElementParser createParser() {
    	return new RootElementParser(pcTypeName, pc().createParser());
    }
    
    public Rule() {
    	pcTypeName = Thread.currentThread().getStackTrace()[3].getMethodName();
    }    
    public Rule(String ruleName) {pcTypeName = ruleName;}    

	public List<PC> getPCChildren() {
		
		ArrayList<PC> ret = new ArrayList<PC>();

		ret.add(pc());
		
		return ret;
	}    	
	
	public String getPCTypeName() {
		return pcTypeName;
	}
	
}
