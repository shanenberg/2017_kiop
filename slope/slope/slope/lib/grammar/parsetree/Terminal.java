package slope.lib.grammar.parsetree;

import slope.SetCmd;
import slowscane.scanners.TokenReader;

public abstract class Terminal extends Expression {
	public String value;

	public SetCmd<String> valueSetter = new SetCmd<String>() {
		public void set(String v) {
			value = v;
		}
	};
	
	@Override
	public abstract void printTo(StringBuffer buffer);
	
	public abstract TokenReader<String> createTokenReader();
}
