package slope.lib.grammar.parsetree.visitors;

public interface GrammarVisitorNode {
	public void accept(GrammarRuleVisitor g);
}
