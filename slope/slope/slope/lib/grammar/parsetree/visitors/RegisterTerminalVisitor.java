package slope.lib.grammar.parsetree.visitors;

import java.util.ArrayList;

import slope.PC;
import slope.Rule;
import slope.SetCmd;
import slope.lang.SET.SYSOParser;
import slope.lib.SlopeLib;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.ReturnCode;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.TokenRefTerminal;
import slope.lib.grammar.parsetree.TokenSkipped;
import slope.lib.regex.RegExScanner;
import slowscane.scanners.TokenReader;

public class RegisterTerminalVisitor implements GrammarRuleVisitor<Object> {
	
	final DynamicGrammarParser dynamicParser;
	
	public RegisterTerminalVisitor(DynamicGrammarParser p) {
		super();
		this.dynamicParser = p;
	}

	public Object visit(final Alternative e) {
//System.out.println("ALTERNATIVE");		
			for (Expression expression : e.expressions) {
				expression.acceptVisitor(this);
			}		
			return null;
	}

	public Object visit(final BracketExpression e) {
//System.out.println("BRACKET");		
			e.expression.acceptVisitor(this);
			return null;
	}

	public Object visit(final Concatenation e) {
//System.out.println("CONCATENATION");		
			for (Expression expression : e.expressions) {
				expression.acceptVisitor(this);
			}		
			return null;
	}

	public Object visit(final MultiplePlus e) {
//System.out.println("MULTIPLEPLUS");				
		e.expression.acceptVisitor(this);
		return null;
	}

	public Object visit(final MultipleStar e) {
//System.out.println("MULTIPLESTAR");				
		e.expression.acceptVisitor(this);
		return null;
	}

	public Object visit(final NonTerminal e) {
//System.out.println("NONTERMINAL");				
		return null;
	}

	public Object visit(final Optional e) {
//System.out.println("OPTIONAL");				
					e.expression.acceptVisitor(this);
					return null;
	}

//	public Object visit(ReturnCode e) {
//		throw new RuntimeException("not yet implemented");
//	}
	
	public Object visit(final LibraryTerminal e) {
//		SlopeLib.getInstance().putLibIntoDynamicParser(dynamicParser, libName, tokenName);
//System.out.println("register " + e.libTokenName);		
//		dynamicParser.addTokenIfNotKnown(e.libTokenName, e.createTokenReader());
		e.registerTokenReadertoDynamicParser(dynamicParser);
		return null;
	}	
	
	public Object visit(final StringTerminal e) {	
		dynamicParser.addTokenIfNotKnownFront(e.value, e.createTokenReader());
		return null;
	}	


	public Rule visit(final TokenRefTerminal e) {
//		SlopeLib.
//System.out.println("TokenRefTerminal");				
//		
//		throw new RuntimeException("Not yet implemented");
//		final TokenReader r = e.createTokenReader();		
//		SlopeLib.getInstance().putLibIntoDynamicParser(dynamicParser, e.)
		return null;
	}
	
	public Rule visit(final BuiltInTerminal e) {
		final TokenReader r = e.createTokenReader();		
		dynamicParser.addTokenIfNotKnownBack(e.terminalName, r);		
//System.out.println("BuiltInTerminal");				
//		throw new RuntimeException("Not yet implemented");
		return null;
	}	

}
