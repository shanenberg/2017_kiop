package slope.lib.grammar.parsetree.visitors;

import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.ReturnCode;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.Token;
import slope.lib.grammar.parsetree.TokenRefTerminal;
import slope.lib.grammar.parsetree.TokenSkipped;

public interface GrammarRuleWalker {
	public void visit(Alternative a);
	public void visit(BracketExpression e);
	public void visit(Concatenation e);
	public void visit(LibraryTerminal e);
	public void visit(MultiplePlus e);
	public void visit(MultipleStar e);
	public void visit(NonTerminal e);
	public void visit(Optional e);
//	public void visit(ReturnCode e);
	public void visit(StringTerminal e);
	public void visit(TokenRefTerminal e);
}
