package slope.lib.grammar.parsetree.visitors;

import java.util.ArrayList;

import slope.PC;
import slope.Rule;
import slope.SetCmd;
import slope.lang.SET.SYSOParser;
import slope.lib.SlopeLib;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.ReturnCode;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.TokenRefTerminal;
import slope.lib.grammar.parsetree.TokenSkipped;
import slope.lib.regex.RegExScanner;
import slowscane.scanners.TokenReader;

public class Walker implements GrammarRuleVisitor<Rule> {
	
	public Rule visit(final Alternative e) {
//System.out.println("ALTERNATIVE");
		return null;
	}

	public Rule visit(final BracketExpression e) {
//System.out.println("BRACKETEXPR");
		return null;
	}

	public Rule visit(final Concatenation e) {
		return null; 
	}

	public Rule visit(final MultiplePlus e) {
		return null;
	}

	public Rule visit(final MultipleStar e) {
		return null;
	}

	public Rule visit(final NonTerminal e) {
		return null;
	}

	public Rule visit(final Optional e) {
		return null;
	}

//	public Rule visit(ReturnCode e) {
//		return null;
//	}
//	
	public Rule visit(final LibraryTerminal e) {
		return null;
	}	
	
	public Rule visit(final StringTerminal e) {
		return null;
	}	


	public Rule visit(final TokenRefTerminal e) {
		return null;
	}
	
	public Rule visit(final BuiltInTerminal e) {
		return null;
	}	

}
