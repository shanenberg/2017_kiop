package slope.lib.grammar.parsetree.visitors;

import java.util.ArrayList;

import slope.PC;
import slope.Rule;
import slope.SetCmd;
import slope.lang.SET.SYSOParser;
import slope.lib.SlopeLib;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.MultipleStar;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.ReturnCode;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;
import slope.lib.grammar.parsetree.TokenRefTerminal;
import slope.lib.grammar.parsetree.TokenSkipped;
import slope.lib.regex.RegExScanner;
import slowscane.scanners.TokenReader;

public class XXXRegisterTerminalVisitor implements GrammarRuleVisitor<Object> {

	final DynamicGrammarParser dynamicParser;

	public XXXRegisterTerminalVisitor(DynamicGrammarParser p) {
		super();
		this.dynamicParser = p;
	}

	public Object visit(final Alternative e) {
		for (Expression expression : e.expressions) {
			expression.acceptVisitor(this);
		}
		return null;
	}

	public Object visit(final BracketExpression e) {
		e.expression.acceptVisitor(this);
		return null;
	}

	public Object visit(final Concatenation e) {
		for (Expression expression : e.expressions) {
			expression.acceptVisitor(this);
		}
		return null;
	}

	public Object visit(final MultiplePlus e) {
		e.expression.acceptVisitor(this);
		return null;
	}

	public Object visit(final MultipleStar e) {
		e.expression.acceptVisitor(this);
		return null;
	}

	public Object visit(final NonTerminal e) {
		return null;
	}

	public Object visit(final Optional e) {
		e.expression.acceptVisitor(this);
		return null;
	}
//
//	public Object visit(ReturnCode e) {
//		return null;
//	}

	public Object visit(final LibraryTerminal e) {
		dynamicParser.addTokenIfNotKnownBack(e.libTokenName, e.createTokenReader());
		return null;
	}

	public Object visit(final StringTerminal e) {
		dynamicParser.addTokenIfNotKnownBack(e.value, e.createTokenReader());
		return null;
	}

	public Rule visit(final TokenRefTerminal e) {
		return null;
	}

	public Rule visit(final BuiltInTerminal e) {
		final TokenReader r = e.createTokenReader();
		dynamicParser.addTokenIfNotKnownBack(e.terminalName, r);
		return null;
	}

}
