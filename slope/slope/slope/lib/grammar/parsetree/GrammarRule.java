package slope.lib.grammar.parsetree;

import slope.PC;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.DynamicRule;
import slope.lib.grammar.parsetree.visitors.CreateParserRuleVisitor;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;
import slope.lib.grammar.parsetree.visitors.GrammarVisitorNode;

public class GrammarRule implements GrammarVisitorNode{
	public String ruleName;
	public Expression expression;
//	public ReturnCode returnCode;
	
	public void accept(GrammarRuleVisitor g) {
		expression.acceptVisitor(g);
		
	}
	
	public Rule createParserRule(final DynamicGrammarParser p, final SetCmd result) {
		
		CreateParserRuleVisitor ruleCreationVisitor = new CreateParserRuleVisitor(result, p);
		
		final Rule r = expression.acceptVisitor(ruleCreationVisitor);
		
		return r;
	}	
}
