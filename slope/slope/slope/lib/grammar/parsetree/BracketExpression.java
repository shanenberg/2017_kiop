package slope.lib.grammar.parsetree;

import java.util.ArrayList;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public class BracketExpression extends UnaryOperator {
	public void printTo(StringBuffer buffer) {
		buffer.append("(");
		expression.printTo(buffer);
		buffer.append(")");
	}
	
	public <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor) {
		return visitor.visit(this);
	}	
}
