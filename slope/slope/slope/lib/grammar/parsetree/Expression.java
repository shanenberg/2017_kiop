package slope.lib.grammar.parsetree;

import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.parsetree.visitors.GrammarRuleVisitor;

public abstract class Expression {
	
	public abstract void printTo(StringBuffer buffer);
	
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		printTo(buffer);
		return buffer.toString();
	}
	
	public abstract <RETURN_TYPE> RETURN_TYPE acceptVisitor(GrammarRuleVisitor<RETURN_TYPE> visitor);
	
}