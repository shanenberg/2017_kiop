package slope.lib.grammar;

import slope.PC;
import slope.Rule;

public abstract class DynamicRule extends Rule {

	public DynamicRule(String ruleName) {
		super(ruleName);
	}

}
