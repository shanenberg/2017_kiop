package slope.lib.grammar;

import java.util.ArrayList;

import slope.Cmd;
import slope.GetCmd;
import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.parsetree.*;
import slope.lib.regex.SimpleRegExScanner;
import slope.lib.regex.ext.tree.KleenePlus;
import slope.lib.regex.ext.tree.LiteralGroup;
import slope.lib.regex.simple.tree.Alternative;
import slope.lib.regex.simple.tree.BracketExpression;
import slope.lib.regex.simple.tree.Concatenation;
import slope.lib.regex.simple.tree.Kleene;
import slope.lib.regex.simple.tree.Literal;
import slope.lib.regex.simple.tree.RegEx;
import slope.lib.regex.simple.tree.UnaryRegEx;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.annotations.TokenLevel;
import slowscane.lib.Lambda01.Lambda01Tokens;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.JavaStringLiteralScanner;

/*
   TOKENS:
     <ABC> 'a'|'b'|'c'.
 */

public class GrammarParsingWithTokens extends GrammarParsing {

	@Token public TokenReader<?> TOKEN_OR() { return lib.OR; }
	@Token public TokenReader<?> ANGLE_BRACKET_OPEN() { return lib.ANGLE_BRACKET_OPEN; }
	@Token public TokenReader<?> ANGLE_BRACKET_CLOSE() { return lib.ANGLE_BRACKET_CLOSE; }
	@Keyword public TokenReader<?> KEYWORD_TOKENS() { return new KeywordScanner("TOKENS:"); }
	@Keyword public TokenReader<?> KEYWORD_SKIPPED() { return new KeywordScanner("SKIPPED"); }
	@TokenLevel(50) public TokenReader<String> JAVA_CHARACTER_LITERAL() { return lib.JAVA_CHARACTER_LITERAL_UNCODED; }

	@TokenLevel(50) public TokenReader<?> QUESTION_MARK() { return lib.QUESTION_MARK; }
	@TokenLevel(50) public TokenReader<?> PLUS() { return lib.PLUS; }
	@TokenLevel(50) public TokenReader<?> MINUS() { return lib.MINUS; }

	
	
	public class GrammarWithTokensParser extends GrammarParsing.Grammar01Parser {

		public Rule Grammar(final SetCmd<Grammar> setCmd) { return new Rule() { public PC pc() {
			final Grammar g = new Grammar();
			
			SetCmd<String> setGrammarName = new SetCmd<String>() {
				public void set(String value) { 
					g.name = value; }};
			
			SetCmd<String> setStartRule = new SetCmd<String>() {
					public void set(String value) { g.startRuleName = value; }};
				
			SetCmd<ArrayList<slope.lib.grammar.parsetree.Token>> setTokenDeclarations = new SetCmd<ArrayList<slope.lib.grammar.parsetree.Token>>() {
						public void set(ArrayList<slope.lib.grammar.parsetree.Token> value) { g.tokens = value; }};

			return 
				AND(
					TOKEN(JAVA_IDENTIFIER(), setGrammarName),
					TOKEN(KEYWORD_Start()), TOKEN(JAVA_IDENTIFIER(), setStartRule),
					TOKEN(KEYWORD_RULES()),
				    GrammarRules(addResultSetter(g.grammarRules)),
					OPTIONAL(TOKEN_DECLARATIONS(setTokenDeclarations)),
//					syso("set result: " + g),
					result(setCmd, g)
				);
		}};}	
		
		/*
		 *  			Terminal -> "\"" Identifier "\"".
		 */		
		public Rule TERMINAL(final SetCmd<Expression> set) {return new Rule() { public PC pc() { 
			final ResultObject<Terminal> result = new ResultObject<Terminal>();
			
			return 
				AND(
				  OR(
						  SLOPETOKEN_TERMINAL(result.setter),
						  BUILTIN_TERMINAL(result.setter),
						  STRING_TERMINAL(result.setter), 
						  TOKENREF_TERMINAL(result.setter)
				  ),
				  result(set, result.getter)
				);
		}};}	
		
		public Rule STRING_TERMINAL(final SetCmd<Terminal> set) {return new Rule() { public PC pc() { 
			final StringTerminal t = new StringTerminal();
			SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {
				t.value = value;}};

			return 
				AND(
				  TOKEN(JAVA_STRING_LITERAL(), setName),
				  result(set, t)
				);
		}};}		
		
		public Rule BUILTIN_TERMINAL(final SetCmd<Terminal> set) {return new Rule() { public PC pc() { 
			final BuiltInTerminal t = new BuiltInTerminal();
			SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {
				t.terminalName = value;}};

			return 
				AND(
				  TOKEN("<#"),
				  TOKEN(JAVA_IDENTIFIER(), setName),
				  TOKEN("#>"),
				  result(set, t)
				);
		}};}
		
		public Rule TOKENREF_TERMINAL(final SetCmd<Terminal> set) {return new Rule() { public PC pc() { 
			
			final TokenRefTerminal t = new TokenRefTerminal();
			SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {t.name = value;}};

			return 
				AND(
				  TOKEN(ANGLE_BRACKET_OPEN()),
				  TOKEN(JAVA_IDENTIFIER(), setName),
				  TOKEN(ANGLE_BRACKET_CLOSE()),
				  result(set, t)
				);
		}};}
		
		public Rule SLOPETOKEN_TERMINAL(final SetCmd<Terminal> set) {return new Rule() { public PC pc() { 
			final LibraryTerminal t = new LibraryTerminal();
			SetCmd<String> setLibName = new SetCmd<String>() {public void set(String value) {t.libraryName = value;}};
			SetCmd<String> setTokenName = new SetCmd<String>() {public void set(String value) {t.libTokenName = value;}};

			return 
				AND(
				  TOKEN(ANGLE_BRACKET_OPEN()),
				  TOKEN(JAVA_IDENTIFIER(), setLibName),
				  TOKEN(JAVA_IDENTIFIER(), setTokenName),
				  TOKEN(ANGLE_BRACKET_CLOSE()),
				  result(set, t)
				);
		}};}				
		
		public Rule TOKEN_DECLARATIONS(
			final SetCmd<ArrayList<slope.lib.grammar.parsetree.Token>> setCmd) { return new Rule() { public PC pc() {

			final ArrayList<slope.lib.grammar.parsetree.Token> tokensDeclarationList = new ArrayList<slope.lib.grammar.parsetree.Token>();
			SetCmd<slope.lib.grammar.parsetree.Token> addToTokenList = addResultSetter(tokensDeclarationList);

			return 
				AND(
					TOKEN(KEYWORD_TOKENS()),
					NFOLD(
						TOKEN_DECLARATION(addToTokenList)),
					result(setCmd, tokensDeclarationList)
				);
		}};}	
		
		public Rule TOKEN_DECLARATION(final SetCmd<slope.lib.grammar.parsetree.Token> setCmd) { return new Rule() { public PC pc() {

			final slope.lib.grammar.parsetree.Token resultToken = new slope.lib.grammar.parsetree.Token();
			final ResultObject<RegEx> regex = new ResultObject<RegEx>();
			final SetCmd<Object> setAll = new SetCmd<Object>() {
				public void set(Object value) {	
					resultToken.regExString = regex.result.toString();
					setCmd.set(resultToken);}
			};
			
			return 
				OR(SKIPPED_TOKEN_DECLARATION(setCmd), UNSKIPPED_TOKEN_DECLARATION(setCmd));
		}};}	
		
		
		public Rule UNSKIPPED_TOKEN_DECLARATION(final SetCmd<slope.lib.grammar.parsetree.Token> setCmd) { return new Rule() { public PC pc() {

			final slope.lib.grammar.parsetree.Token resultToken = new slope.lib.grammar.parsetree.Token();
			final SetCmd<String> setTokenName = new SetCmd<String>() {public void set(String value) {resultToken.name = value;}};
			final ResultObject<RegEx> regex = new ResultObject<RegEx>();
			final SetCmd<Object> setAll = new SetCmd<Object>() {
				public void set(Object value) {	
					resultToken.regExString = regex.result.toString();
					setCmd.set(resultToken);}
			};
			
			return 
				AND(
					TOKEN(ANGLE_BRACKET_OPEN()),
					TOKEN(JAVA_IDENTIFIER(), setTokenName),
					TOKEN(ANGLE_BRACKET_CLOSE()),
					REGEX(regex.setter),
					TOKEN('.')
					, result(setAll, new Object())
				);
		}};}	
		

		public Rule SKIPPED_TOKEN_DECLARATION(final SetCmd<slope.lib.grammar.parsetree.Token> setCmd) { return new Rule() { public PC pc() {
			final slope.lib.grammar.parsetree.TokenSkipped resultToken = new slope.lib.grammar.parsetree.TokenSkipped();
			final ResultObject<RegEx> regex = new ResultObject<RegEx>();
			
			final SetCmd<String> setTokenName = new SetCmd<String>() {
				public void set(String value) {
//					System.out.println("SET RESULTNAME: " + value);
					resultToken.name = value;}};
				
			final SetCmd<Object> setAll = new SetCmd<Object>() {
				public void set(Object value) {	
//					System.out.println("Skipped token");
					resultToken.regExString = regex.result.toString();
					setCmd.set(resultToken);}
			};
			
			return 
				AND(
					TOKEN(KEYWORD_SKIPPED()),
					TOKEN(ANGLE_BRACKET_OPEN()),
					TOKEN(JAVA_IDENTIFIER(), setTokenName),
//					syso("FOUND"),
					TOKEN(ANGLE_BRACKET_CLOSE()),
					REGEX(regex.setter),
					TOKEN('.')
					, result(setAll, new Object())
				);
		}};}		
		
	
/*************************************************
 * REGEX HERE		
 ************************************************/
		public Rule REGEX(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final ResultObject<RegEx> regex = new ResultObject<RegEx>();
			
			final SetCmd<RegEx> resetHierarchySetter = new SetCmd<RegEx>() {
				public void set(RegEx alternative) {
					RegEx r = regex.getResult();
					if (!(r instanceof Alternative)) {
						regex.result = alternative;
						((Alternative) alternative).expressions.add(0, r);
					} else {
						((Alternative) r).expressions.addAll(((Alternative) alternative).expressions);
					}
				}
			};
			
			return
				AND(
					REGEX_Concat(regex.setter),
					OPTIONAL(REGEX_Alternative(resetHierarchySetter)),
					result(resultSetter, regex.getter)
				);
		}};}
		
		public Rule REGEX_Alternative(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final ResultObject<Alternative> result = ResultObject.create(new Alternative());
			SetCmd<RegEx> alternativeAdder = addResultSetter(result.result.expressions);
			
			return
				AND(
					NFOLD(
							AND(
								TOKEN('|'),	
								NFOLD(REGEX_Concat(alternativeAdder))
							)
					),
					result(resultSetter, (GetCmd)  result.getter)
				);
		}};}		

		public Rule REGEX_Concat(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final Concatenation concat = new Concatenation();
			SetCmd<RegEx> concatAdder = addResultSetter(concat.expressions);
			Cmd replaceConcatIfNecessary = new Cmd() {
				public void doCmd() {
					if (concat.expressions.size()==1) {
						resultSetter.set(concat.expressions.get(0));
					} else {
						resultSetter.set(concat);
					}
				}
			};
			
			return 
				AND(
					NFOLD(REGEX_SimpleExpression(concatAdder)),
					set(replaceConcatIfNecessary)
				);
		}};}		
	
		public Rule REGEX_SimpleExpression(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() { 

			final ResultObject<RegEx> result = new ResultObject<RegEx>();
			SetCmd<UnaryRegEx> kleenePushup = new SetCmd<UnaryRegEx>() {
				public void set(UnaryRegEx value) {
					RegEx r = result.getResult();
					result.result = value;
					value.expression = r;
				}
			};
			
			return 
				AND(
					OR(
						REGEX_BracketExpression(result.setter), 
						REGEX_Literal(result.setter)),
					OPTIONAL(REGEX_POST_UNARYOPERATOR(kleenePushup)),
					result(resultSetter, result.getter)
				);			
		}};}	
		
		public Rule REGEX_BracketExpression(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
			final BracketExpression bracketExpression = new BracketExpression();
			SetCmd<RegEx> set = new SetCmd<RegEx>() {
				public void set(RegEx value) {bracketExpression.expression = value;}};
			
			return 
				AND(
					TOKEN('('), REGEX(set), TOKEN(')'),
					result(resultSetter, bracketExpression));			
		}};}
		
		public Rule REGEX_POST_UNARYOPERATOR(final SetCmd<UnaryRegEx> result) { return new Rule() { public PC pc() { 
			return 
				OR(
					AND( TOKEN(KLEENE()), result(result, new Kleene())),	
					AND( TOKEN(PLUS()), result(result, new KleenePlus())),	
					AND( TOKEN(QUESTION_MARK()), result(result, new slope.lib.regex.ext.tree.Optional()))
				);
		}};}
	
	public Rule REGEX_Literal(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
		final ResultObject<RegEx> result = new ResultObject<RegEx>();
			
		return 
			AND(
				OR(
					REGEX_SimpleLiteral(result.setter), REGEX_LiteralGroup(result.setter)
				),
				result(resultSetter, result.getter));			
	}};}	
	
	public Rule REGEX_SimpleLiteral(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
		final Literal literal = new Literal();
		SetCmd<String> set = new SetCmd<String>() {
			public void set(String value) {	literal.character = value;} };
			
		return 
			AND(
				TOKEN(JAVA_CHARACTER_LITERAL(), set),
				result(resultSetter, literal));			
	}};}	
	
	public Rule REGEX_LiteralGroup(final SetCmd<RegEx> resultSetter) { return new Rule() { public PC pc() {
		final LiteralGroup literal = new LiteralGroup();
		SetCmd<RegEx> setLeft = new SetCmd<RegEx>() {
			public void set(RegEx value) {
				literal.start = (slope.lib.regex.simple.tree.Literal) value;
			}
		};
		
		SetCmd<RegEx> setRight = new SetCmd<RegEx>() {
			public void set(RegEx value) {
				literal.end = (slope.lib.regex.simple.tree.Literal) value;
			}
		};		
			
		return 
			AND(
				TOKEN(OCURLY_BRACKET()),
				REGEX_SimpleLiteral(setLeft),
				TOKEN(MINUS()),
				REGEX_SimpleLiteral(setRight),
				TOKEN(CCURLY_BRACKET()),
				result(resultSetter, literal));			
	}};}	
			
		
		
		
	}
		
}
