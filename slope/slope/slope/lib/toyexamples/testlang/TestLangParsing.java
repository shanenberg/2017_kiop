/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.toyexamples.testlang;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class TestLangParsing extends slowscane.Tokens {
		
	     @Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }
	     @Separator public TokenReader<?> NEWLINE() { return lib.NEWLINE;}
	     
	     @Keyword public TokenReader<String> Int() { return lib.KEYWORD_Int; }
	     @Token public TokenReader<String> JAVA_IDENTIFIER() { return lib.JAVA_IDENTIFIER; }
		 @Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
	     
	     
		
//		@Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }
//		@Separator public TokenReader<?> NEWLINE() { return lib.NEWLINE;}
//		@Keyword public TokenReader<String> Int() { return lib.KEYWORD_Int; }
//		@Token public TokenReader<String> JAVA_IDENTIFIER() { return lib.JAVA_IDENTIFIER; }
//		
		public class TestLangParser extends Parser<TestLangParsing> {
			
			public Rule KEYWORD_Int() { return new Rule() { public PC pc() { return
					TOKEN(Int());
					}};}
			
			public Rule IDENTIFIER() { return new Rule() { public PC pc() { return
					TOKEN(JAVA_IDENTIFIER());
					}};}
			
			public Rule EXPRESSION() { return new Rule() { public PC pc() { return
					OR(KEYWORD_Int(), IDENTIFIER(), N_TIMES_BRACKET_EXPRESSION());
					}};}
			
			public Rule BRACKET_EXPRESSION() { return new Rule() { public PC pc() { return
					AND(TOKEN(OBRACKET()), EXPRESSION(), TOKEN(CBRACKET()));
					}};}
			
			public Rule N_TIMES_BRACKET_EXPRESSION() { return new Rule() { public PC pc() { return
					NFOLD(BRACKET_EXPRESSION());
					}};}
//			
//			Rule KEYWORD_Int() { return new Rule() { public PC pc() { return
//					TOKEN(Int());
//			}};}
//			
//			Rule IDENTIFIER() { return new Rule() { public PC pc() { return
//					TOKEN(JAVA_IDENTIFIER());
//			}};}
//			
//			Rule EXPRESSION() { return new Rule() { public PC pc() { return
//					OR(KEYWORD_Int(), IDENTIFIER(), N_TIMES_BRACKET_EXPRESSION());
//			}};}
//			
//			Rule BRACKET_EXPRESSION() { return new Rule() { public PC pc() { return
//					AND(TOKEN(OBRACKET()), EXPRESSION(), TOKEN(CBRACKET()));
//			}};}
//			
//			Rule N_TIMES_BRACKET_EXPRESSION() { return new Rule() { public PC pc() { return
//					NFOLD(BRACKET_EXPRESSION());
//			}};}			
//			
		}
//		

}