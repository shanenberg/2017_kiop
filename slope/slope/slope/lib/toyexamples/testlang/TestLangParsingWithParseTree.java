/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.toyexamples.testlang;

import slope.PC;
import slope.Parser;
import slope.Rule;
import slope.SetCmd;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class TestLangParsingWithParseTree extends slowscane.Tokens {
		@Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }
		@Separator public TokenReader<?> NEWLINE() { return lib.NEWLINE;}
		@Keyword public TokenReader<String> Int() { return lib.KEYWORD_Int; }
		@Token public TokenReader<String> JAVA_IDENTIFIER() { return lib.JAVA_IDENTIFIER; }
		@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
		
		/* The parser classes */
		public class TestLangParser extends Parser<TestLangParsingWithParseTree> {

			public Rule KEYWORD_Int(final SetCmd<Expression> resultSetter) { 
				  // defines that after a successful parsing, the resultSetter receives a new IntExpression.				
				  return new Rule() { public PC pc() { return
				TOKEN(Int(), setResult(resultSetter, new IntExpression()));
				}};}	
			
			public Rule IDENTIFIER(final SetCmd<Expression> resultSetter) {

				// Defines the command object that sets the token string to the return object and sets the result object
				final SetCmd<String> setValue = new SetCmd<String>() { public void set(String value) { 
				    IdentifierExpression expr = new IdentifierExpression();
				       expr.identifier = value;
				    resultSetter.set(expr);}};
									
				return new Rule() { public PC pc() { return
				TOKEN(JAVA_IDENTIFIER(), setValue);
				}};}
			
			public Rule N_TIMES_BRACKET_EXPRESSION(final SetCmd<Expression> resultSetter) { 

				final NFOLDBracketExpression expr = new NFOLDBracketExpression();

				return new Rule() { public PC pc() { return
				NFOLD(BRACKET_EXPRESSION(addResultSetter(expr.bracketExpressions)), setResult(resultSetter, expr));
				}};}
			
			public Rule BRACKET_EXPRESSION(final SetCmd<BracketExpression> resultSetter) { 
				final BracketExpression expr = new BracketExpression();
								
				final SetCmd<Expression> setChild = new SetCmd<Expression>() { public void set(Expression value) {
				expr.childExpression = value;}};				
								
				return new Rule() { public PC pc() { return
				AND(TOKEN(OBRACKET()), EXPRESSION(setChild), TOKEN(CBRACKET()), set(setResult(resultSetter, expr)));
				}};}

			
			public Rule EXPRESSION(final SetCmd<Expression> result) { return new Rule() { public PC pc() { return
					OR(KEYWORD_Int(result), IDENTIFIER(result), N_TIMES_BRACKET_EXPRESSION(result));
					}};}
					
		}

}