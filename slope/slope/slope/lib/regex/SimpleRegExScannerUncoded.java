package slope.lib.regex;

import shapuc.automata.simple.SimpleAutomata;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.regex.simple.RegExParsing;
import slope.lib.regex.simple.RegExParsing.RegExParser;
import slope.lib.regex.simple.RegExParsingUncoded;
import slope.lib.regex.simple.tree.RegEx;
import slowscane.ScanException;
import slowscane.Token;
import slowscane.TokenType;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.AnyCharScanner;
import slowscane.scanners.lib.JavaCharLiteralScanner;
import slowscane.scanners.lib.JavaCharLiteralScannerUncoded;
import slowscane.streams.PositionStream;

public class SimpleRegExScannerUncoded extends TokenReader<String>{

	String regex;
	SimpleAutomata<Object, String> automata;
	
	public SimpleRegExScannerUncoded(String regex) {
		this.regex = regex;
		this.automata = createAutomata();
	}
	
	private SimpleAutomata<Object, String> createAutomata() {
		RegExParsing tokens = new RegExParsingUncoded();
		ResultObject<RegEx> result = new ResultObject<RegEx>();
		Parsing<RegExParsing> parsing = new Parsing<RegExParsing>(tokens, regex);

		parsing.parseWithParseTreeConstruction(tokens.new RegExParser().REGEX(result.setter));
		return result.result.toAutomata();
	}

	@Override
	public Token<String> scanNext(PositionStream<?> stream) {
		boolean hadAnAccept = false;
		int lastFinishPoint = stream.currentPosition;
		String lastAcceptanceString = null;

		AnyCharScanner charLiteralScanner;
		String next;
		
		createAutomata();
		automata.start();
		

		StringBuffer buf = new StringBuffer();
		while(true) {
			try {
				charLiteralScanner = new AnyCharScanner();
				next = charLiteralScanner.scanNext(stream).getValueString();

				automata.gotoNext(next);
				buf.append(next);
				
				if (automata.hasFinished()) {
					lastFinishPoint = stream.currentPosition;
					lastAcceptanceString = buf.toString();
					hadAnAccept = true;
				}
			} catch (Exception ex) {
				if (hadAnAccept) {
					stream.currentPosition = lastFinishPoint;
					return createToken(stream, lastAcceptanceString);
				} else {
					throw new ScanException("Simple RegEx scanner cannot handle word", buf.toString());
				}
			}
		}
	}

	protected TokenType getTokenType() {
		return new RegExTokenType("RegExToken", regex);
	}
	
}
