package slope.lib.regex;

import shapuc.automata.simple.SimpleAutomata;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.regex.ext.RegExExtendedParsing;
import slope.lib.regex.simple.RegExParsing;
import slope.lib.regex.simple.RegExParsing.RegExParser;
import slope.lib.regex.simple.tree.RegEx;
import slowscane.ScanException;
import slowscane.Token;
import slowscane.TokenType;
import slowscane.scanners.TokenReader;
import slowscane.scanners.lib.AnyCharScanner;
import slowscane.scanners.lib.JavaCharLiteralScanner;
import slowscane.streams.PositionStream;
import slowscane.streams.TokenStream;

public class RegExScanner extends TokenReader<String>{

	public String regex;
	SimpleAutomata<Object, String> automata;
	
	public RegExScanner(String regex, String name) {
		this.regex = regex;
		this.automata = createAutomata();
		this.tokenType = new TokenType(name);
	}
	
	private SimpleAutomata<Object, String> createAutomata() {
		RegExExtendedParsing tokens = new RegExExtendedParsing();
		ResultObject<RegEx> result = new ResultObject<RegEx>();
		Parsing<RegExParsing> parsing = new Parsing<RegExParsing>(tokens, regex);

		parsing.parseWithParseTreeConstruction(tokens.new RegExExtParser().REGEX(result.setter));
		return result.result.toAutomata();
	}

	@Override
	public Token<String> scanNext(PositionStream<?> stream) {
//System.out.println("REGEX SCANNER: " + tokenType.getTokenTypeName());		
		boolean hadAnAccept = false;
		int lastFinishPoint = stream.currentPosition;
		String lastAcceptanceString = null;

		AnyCharScanner charLiteralScanner;
		String next;
		
		createAutomata();
		automata.start();
		

		StringBuffer buf = new StringBuffer();
		while(true) {
			try {
				charLiteralScanner = new AnyCharScanner();
				next = charLiteralScanner.scanNext(stream).getValueString();
//System.out.println("scan" + next);
				automata.gotoNext(next);
				buf.append(next);
				
				if (automata.hasFinished()) {
//System.out.println("hasFinished");
					lastFinishPoint = stream.currentPosition;
					lastAcceptanceString = buf.toString();
					hadAnAccept = true;
				}
			} catch (Exception ex) {
				if (hadAnAccept) {
					stream.currentPosition = lastFinishPoint;
					return createToken(stream, lastAcceptanceString);
				} else {
					throw new ScanException("Simple RegEx scanner cannot handle word", buf.toString());
				}
			}
		}
	}
	
	public TokenType getTokenType() {
		return super.getTokenType();
	}
	
	protected boolean tokenIsOfRightKind(Token<?> token) {
//		System.out.println("REGEX Check: " + this.getTokenType());		
//		System.out.println("REGEX Check: " + token.getTokenType());		
		return token.getTokenType().equals(getTokenType());
	}	
//	
//	protected TokenType getTokenType() {
//		return tokenType;
//	}	
	
//	@SuppressWarnings("unchecked")
//	public Token<String> parseNext(TokenStream<?> tokenstream) {
//		Token<?> nextToken = tokenstream.next();
//		System.out.println("NEXTTOKEN: " + nextToken);		
//		System.out.println("NEXTTOKEN: " + tokenIsOfRightKind(nextToken));		
//		if (!tokenIsOfRightKind(nextToken))
//			throw new ScanException(this.getClass().getName() + ": Next token not as expected", tokenstream.stuckExceptionString());
//		return (Token<String>) nextToken;
//	}		

}
