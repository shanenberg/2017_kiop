package slope.lib.regex.ext.tree;

import shapuc.automata.simple.SimpleNEA;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.regex.simple.tree.Kleene;
import slope.lib.regex.simple.tree.UnaryRegEx;

public class KleenePlus extends UnaryRegEx {

	@Override
	public void printTo(StringBuffer buffer) {
		expression.printTo(buffer);
		buffer.append("+");
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		// Konstrukt a Concatenation of expression and Expression*
		
		slope.lib.regex.simple.tree.Concatenation c = 
				new slope.lib.regex.simple.tree.Concatenation();
		
		c.expressions.add(expression);
		c.expressions.add(new Kleene(expression));
		
		c.toNEA(automata, startNode, endNode);
	}

}
