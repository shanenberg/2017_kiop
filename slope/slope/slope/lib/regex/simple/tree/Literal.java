package slope.lib.regex.simple.tree;

import java.util.ArrayList;
import java.util.List;

import shapuc.automata.simple.SimpleAutomata;
import shapuc.automata.simple.SimpleNEA;

public class Literal extends RegEx {
	// the character includes leading and closing ' -- 
	// I remove it when writing to the automata
	public String character; 

	public Literal(String character) {
		super();
		this.character = character;
	}
	
	public Literal() {
		super();
		this.character = character;
	}	

	public void printTo(StringBuffer buffer) {
		buffer.append(character);
	}

	public String unquotedString() {
		return character.substring(1, character.length()-1);
	}
	
	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		Object myStart = new Object();
		Object myEnd = new Object();
		automata.addTransition(myStart, unquotedString(), myEnd);
		
		automata.addSpontaneousTransition(startNode, myStart);
		automata.addSpontaneousTransition(myEnd, endNode);
	}

	public List<Literal> literalsUpTo(Literal end) {
		ArrayList<Literal> ret = new ArrayList<Literal>();
		
		Character startChar = (Character) unquotedString().charAt(0);
		Character endChar = end.unquotedString().charAt(0);
		
		for (int i = startChar.charValue(); i <= endChar.charValue(); i++) {
			ret.add(new Literal("'" + ((char) i) + "'"));
		}
		return ret;
	}
	

}
