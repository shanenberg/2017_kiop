package slope.lib.regex.simple.tree;

import shapuc.automata.simple.SimpleNEA;

public class Alternative extends Multiplicity {
	
	@Override
	public void printTo(StringBuffer buffer) {
		for (RegEx regEx : expressions) {
			buffer.append(regEx);
			if (expressions.get(expressions.size()-1)!=regEx) {
				buffer.append("|");
			}
		}
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		
		for (RegEx regEx : expressions) {
			Object myStart = new Object();
			Object myEnd = new Object();
			automata.addSpontaneousTransition(startNode, myStart);
			automata.addSpontaneousTransition(myEnd, endNode);
			regEx.toNEA(automata, myStart, myEnd);
		}
	}
}
