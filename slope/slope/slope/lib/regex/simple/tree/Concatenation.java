package slope.lib.regex.simple.tree;

import shapuc.automata.simple.SimpleNEA;

public class Concatenation extends Multiplicity {
	
	@Override
	public void printTo(StringBuffer buffer) {
		for (RegEx regEx : expressions) {
			buffer.append(regEx);
		}
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		
		Object currentStart = startNode;
		
		for (RegEx regEx : expressions) {
			Object myStart = new Object();
			Object myEnd = new Object();
			automata.addSpontaneousTransition(currentStart, myStart);
			regEx.toNEA(automata, myStart, myEnd);
			
			currentStart=myEnd;
		}
		
		automata.addSpontaneousTransition(currentStart, endNode);

	}
}
