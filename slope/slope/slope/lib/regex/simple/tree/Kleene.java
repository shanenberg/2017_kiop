package slope.lib.regex.simple.tree;

import shapuc.automata.simple.SimpleNEA;

public class Kleene extends UnaryRegEx {
	
	public Kleene() {}
	
	public Kleene(RegEx expression) {
		this();
		this.expression = expression;
	}
	
	public void printTo(StringBuffer buffer) {
		expression.printTo(buffer);
		buffer.append("*");
	}

	@Override
	public void toNEA(SimpleNEA<Object, String> automata, Object startNode, Object endNode) {
		// Create outer nodes for 0 times
		Object myStart = new Object();
		Object myEnd = new Object();
		automata.addSpontaneousTransition(startNode, myStart);
		automata.addSpontaneousTransition(myEnd, endNode);
		automata.addSpontaneousTransition(myStart, myEnd); // From start to end -> 0 times
		
		// Create inner nodes for 0 times
		Object myInnerStart = new Object();
		Object myInnerEnd = new Object();
		automata.addSpontaneousTransition(myStart, myInnerStart);
		automata.addSpontaneousTransition(myInnerEnd, myEnd);
		automata.addSpontaneousTransition(myInnerEnd, myInnerStart); // From inner end to inner start -> n times
		
		expression.toNEA(automata, myInnerStart, myInnerEnd);
		
	}
}
