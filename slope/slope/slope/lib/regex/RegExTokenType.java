package slope.lib.regex;

import slowscane.TokenType;

public class RegExTokenType extends TokenType {

	String regexString;
	
	public RegExTokenType(String tokenTypeName, String regexString) {
		super(tokenTypeName);
		this.regexString = regexString;
	}

}
