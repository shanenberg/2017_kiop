/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.treenodecompression;

import java.util.List;

import slope.RootElementTreeNode;
import slope.TreeNode;
import slope.TreeNodeVisitor_void;
import slope.lang.AND.ANDTreeNode;
import slope.lang.BIND.BINDTreeNode;
import slope.lang.NFOLD.NFOLDTreeNode;
import slope.lang.OPTIONAL.OPTIONALTreeNode;
import slope.lang.OR.ORTreeNode;
import slope.lang.SET.NULLTreeNode;
import slope.lang.SET.SETTreeNode_NoParameter;
import slope.lang.SET.SYSOTreeNode;
import slope.lang.SET.SetTokenValueTreeNode;
import slope.lang.TOKEN.TokenTreeNode;

public class RunSetObjects implements TreeNodeVisitor_void{

	public void visit(TreeNode n) {
//System.out.println(n);
		this.visitAll(n.getChildren());
	}

	public void visit(RootElementTreeNode n) {
//System.out.println(n);
		this.visit(n.treeNode);
	}	
	
	public void visit(NULLTreeNode n) {
//System.out.println(n);
		n.runSetObject();
	}	
	
	public void visit(SYSOTreeNode n) {
//System.out.println(n);
		n.runSetObject();
	}		

	
	public void visit(ANDTreeNode n) {
//System.out.println(n);
		this.visitAll(n.getChildren());
	}

	public void visit(ORTreeNode n) {
//System.out.println(n);
		this.visitAll(n.getChildren());
//		this.visit(n.treeNode);
	}

	public void visit(BINDTreeNode<?> n) {
//System.out.println(n);
		this.visit(n.treeNode);
		
	}

	public void visit(NFOLDTreeNode n) {
//System.out.println(n);
		this.visitAll(n.getChildren());
		
	}

	public void visit(SetTokenValueTreeNode<?> n) {
//System.out.println(n);
		this.visit(n.treeNode);
		n.runSetObject();
	}
	
	public void visit(SETTreeNode_NoParameter n) {
//System.out.println(n);
		this.visit(n.treeNode);
		n.runSetObject();
	}	

	public void visit(TokenTreeNode<?> n) {
		
	}
	
	protected void visitAll(List<TreeNode> treeNodes) {
		for (TreeNode treeNode : treeNodes) {
			treeNode.accept(this);
		}
	}

	public void visit(OPTIONALTreeNode n) {
//System.out.println(n);
		if (n.treeNode!=null)
			this.visit(n.treeNode);
		
	}
	


}
