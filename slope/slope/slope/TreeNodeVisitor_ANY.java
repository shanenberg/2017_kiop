/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import slope.lang.AND.ANDTreeNode;
import slope.lang.BIND.BINDTreeNode;
import slope.lang.NFOLD.NFOLDTreeNode;
import slope.lang.OPTIONAL.OPTIONALTreeNode;
import slope.lang.OR.ORTreeNode;
import slope.lang.SET.NULLTreeNode;
import slope.lang.SET.SETTreeNode_NoParameter;
import slope.lang.SET.SetTokenValueTreeNode;
import slope.lang.TOKEN.TokenTreeNode;

public interface TreeNodeVisitor_ANY<ReturnType> {
	public ReturnType visit(TreeNode n);
	public ReturnType visit(TreeNodeImpl_1 n);
		
	public ReturnType visit(ANDTreeNode n);
	public ReturnType visit(ORTreeNode n);
	public ReturnType visit(OPTIONALTreeNode n);
	public ReturnType visit(BINDTreeNode<?> n);
	public ReturnType visit(NFOLDTreeNode n);
	
	public ReturnType visit(NULLTreeNode n);
	public ReturnType visit(SetTokenValueTreeNode<?> n);
	public ReturnType visit(SETTreeNode_NoParameter n);
	
	public ReturnType visit(TokenTreeNode<?> n);
}
