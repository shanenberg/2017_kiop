/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package sloc.parsing;

import sloc.CSVColumnConstraint;
import sloc.ColumnDefinition;
import sloc.ColumnType;
import sloc.Header;
import sloc.columnTypes.HexaIntType;
import sloc.columnTypes.IntType;
import sloc.columnTypes.ListType;
import sloc.columnTypes.MapType;
import sloc.columnTypes.RefType;
import sloc.columnTypes.StringType;
import slope.Cmd;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.Rule;
import slope.SetCmd;
import slowscane.lib.CSV;

public class CVSHeaderParserTokens extends CSV.CSVHeaderTokens {

		public class CVSHeaderParser extends Parser<CVSHeaderParserTokens> {
			public Rule START(final SetCmd<Header> resultSetter) { 
				
				final Header header = new Header();

				return new Rule() { public PC pc() { 				
				return 
					AND(
					  NFOLD(COLUMNDEFINITION(addResultSetter(header.columnDefinitions))),
					  set(setResult(resultSetter, header)));
			}};}

			Rule COLUMNDEFINITION(final SetCmd<ColumnDefinition> resultSetter) { return new Rule() { public PC pc() { 
				
				final ColumnDefinition def = new ColumnDefinition();
				SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {
					def.name = value;}};
				SetCmd<ColumnType> setType = new SetCmd<ColumnType>() {public void set(ColumnType value) {
					def.type = value;}};
					
				return 
		          AND(
				    TOKEN(IDENTIFIER(), setName), 
					TOKEN(COLON()), 
                    COLUMN_TYPE(setType), 
                    set(setResult(resultSetter, def))
				  );
			}};}

			Rule COLUMN_TYPE(final SetCmd<ColumnType> setType) { return new Rule() { public PC pc() { 
				return 
				OR(
					COLUMN_TYPE_PRIMITIVE(setType));
			}};}
			
			Rule COLUMN_TYPE_PRIMITIVE(final SetCmd<ColumnType> setType) { return new Rule() { public PC pc() { 
				
				return 
				OR(
					TOKEN(INT(), setResult(setType, new IntType())),
					TOKEN(STRING(), setResult(setType, new StringType())), 
					TOKEN(HEXA(), setResult(setType, new HexaIntType())), 
					MAP_TYPE(setType), 
					LIST_TYPE(setType), 
					REF_TYPE(setType));
			}};}
			
			Rule MAP_TYPE(final SetCmd<ColumnType> setType) { return new Rule() { public PC pc() { 
				
				final MapType mapType = new MapType();
				SetCmd<ColumnType> setKeyType = new SetCmd<ColumnType>() {
					public void set(ColumnType val) { mapType.setKeyType(val); }};
				SetCmd<ColumnType> setValueType = new SetCmd<ColumnType>() {
						public void set(ColumnType val) { mapType.setValueType(val); }};
						
				return 
						AND(
							TOKEN(MAP()), 
							TOKEN('('),
							COLUMN_TYPE(setKeyType),
							TOKEN(':'),
							COLUMN_TYPE(setValueType),
							TOKEN(')'),
							set(setResult(setType, mapType)));
			}};}			
			
			Rule LIST_TYPE(final SetCmd<ColumnType> setType) { return new Rule() { public PC pc() { 
				
				final ListType listType = new ListType();
				SetCmd<ColumnType> setRef = new SetCmd<ColumnType>() {
						public void set(ColumnType val) { listType.setComponentType(val); }};
						
				return 
						AND(
							TOKEN(LIST()), 
							TOKEN('('),
							COLUMN_TYPE(setRef),
							TOKEN(')'),
							set(setResult(setType, listType)));
			}};}			
			
			
			Rule REF_TYPE(final SetCmd<ColumnType> setType) { return new Rule() { public PC pc() { 
				
				final RefType refType = new RefType();
				
				
				Cmd setRef = new Cmd() {
						public void doCmd() { setType.set(refType); }};

				SetCmd<String> setName = new SetCmd<String>() {public void set(String value) {
							refType.setRefName(value);}};
						
						
				return 
					set(
						AND(
							TOKEN(REF()), TOKEN('('), TOKEN(IDENTIFIER(), setName), TOKEN(')')
						)	
						, setRef);
			}};}			
			
			Rule COLUMN_CONSTRAINT(final SetCmd<CSVColumnConstraint> addConstraint) { return new Rule() { public PC pc() { 
				
//				SETObject_NoParameter setInt = new SETObject_NoParameter() {
//					public void set() { setType.set(new CSVIntType()); }};
//				SETObject_NoParameter setString = new SETObject_NoParameter() {
//						public void set() { setType.set(new CSVStringType()); }};
				
				return 
				AND(
					TOKEN('{'),
					TOKEN('}'));
//				OR(
//					result(TOKEN("Unique"), setInt),
//					result(TOKEN("Not Null"), setString));
			}};}				
		}
			
	}

