/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package sloc.parsing;

import java.util.List;

import sloc.Header;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slowscane.lib.CSV;


	public class CSVBodyParserTokens extends CSV.CSVBodyTokens {
		public class CSVBodyParser extends Parser<CVSHeaderParserTokens> {
			
//			public Rule SIMPLE_TYPE(final SetCmd<Integer> resultSetter) { return new Rule() { public PC pc() {
//				return 
//					OR(Integer(resultSetter), String(resultSetter));
//			}};}
			
			public Rule Integer(final SetCmd<Integer> resultSetter) { return new Rule() { public PC pc() { 
				return 
					OR(
					  TOKEN(INTEGER(), resultSetter),
					  TOKEN(NULL(), setResult(resultSetter, null)));
			}};}
			
			public Rule HexaInt(final SetCmd<Integer> resultSetter) { return new Rule() { public PC pc() { 
				return 
					OR(
					  TOKEN(HEXAINT(), resultSetter),
					  TOKEN(NULL(), setResult(resultSetter, null)));
			}};}

			public Rule String(final SetCmd<String> resultSetter) { return new Rule() { public PC pc() { 
				return 
					OR(
					  TOKEN(STRING(),  resultSetter),
					  TOKEN(NULL(), setResult(resultSetter, null))
					);
			}};}	
			
//			public Rule Map(final SetCmd<String> resultSetter) { return new Rule() { public PC pc() { 
//				return 
//					OR(
//					  TOKEN(STRING(),  resultSetter),
//					  TOKEN(NULL(), setResult(resultSetter, null))
//					);
//			}};}	
			
			
		}
	}
	


