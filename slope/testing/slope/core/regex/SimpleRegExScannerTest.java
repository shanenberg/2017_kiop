package slope.core.regex;

import java.util.ArrayList;

import junit.framework.TestCase;
import slope.lib.regex.SimpleRegExScannerUncoded;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

public class SimpleRegExScannerTest extends TestCase {
	
	public void test2() {
		TokenStream r;
		r = scan("'\n'('\n')*", "\n");
		assertEquals(1, r.length());
		assertEquals("\n", r.get(0).getValueString());
	}	
	
	public void test1() {
		TokenStream r;
		scan("'a'|'b'", "a");
		r = scan("'a''b'", "ab");
		r = scan("('a'('a''b')*)", "aabab");
		assertEquals(1, r.length());
		assertEquals("aabab", r.get(0).getValueString());
	}

	private TokenStream scan(String regex, String input) {
		SimpleRegExScannerUncoded scanner = new SimpleRegExScannerUncoded(regex);
		ArrayList<TokenReader> scanners = new ArrayList<TokenReader>();
		scanners.add(scanner);
		StringBufferStream stream = new StringBufferStream(input);
		Scanner s = new Scanner(scanners, new ArrayList(), stream);
		TokenStream ret = s.scan();
		return ret;
	}
	
	
}
