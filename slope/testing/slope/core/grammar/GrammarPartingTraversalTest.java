package slope.core.grammar;

import java.util.List;

import junit.framework.TestCase;
import slope.PC;
import slope.ParserConstructor_N;
import slope.Parsing;
import slope.Rule;
import slope.SetCmd;
import slope.lang.TOKEN.TokenParserConstructor;
import slope.lib.grammar.GrammarParsing;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.parsetree.Grammar;
import slope.util.functions.pctree.GetConstantTokenAsString;
import slope.util.functions.pctree.IsConstantTokenReader;
import slope.util.functions.pctree.IsTokenParserConstructor;
import slowscane.scanners.TokenReader;

public class GrammarPartingTraversalTest extends TestCase {
	
	/*
	 * The code shows that all nodes within a parser can be traversed in 
	 * a unique way. All nodes have a method "getPCChildren" that 
	 * returns a parser constructor-node. 
	 */
	public void testTraversal() throws Exception {
		GrammarParsing tokens = new GrammarParsing();
		SetCmd<Grammar> setGrammar = new SetCmd<Grammar>() {
			public void set(Grammar value) {}};
			
		Rule grammar = tokens.new Grammar01Parser().Grammar(setGrammar);

		assertEquals("Grammar", grammar.pcTypeName);
		
		PC and = grammar.getPCChildren().get(0);
		
		assertEquals("AND", and.getPCTypeName());
		
		// THIS IS DAMN STRANGE....WHAT HAPPENS HERE?!?!?!
		// I NEED TO WRITE THE STUFF IN TWO LINES?!?!?!?
		List<PC> andChildren = and.getPCChildren();
		// PC t = and.getPCChildren().get(0);
		
//		assertEquals("TOKEN", andChildren.get(0).getPCTypeName());
//		printTokenIfAvailable(andChildren.get(0));
//		
//		assertEquals("TOKEN", andChildren.get(1).getPCTypeName());
//		printTokenIfAvailable(andChildren.get(1));
//		
//		
//		assertEquals("TOKEN", andChildren.get(2).getPCTypeName());
//		printTokenIfAvailable(andChildren.get(2));
//
//		assertEquals("TOKEN", andChildren.get(3).getPCTypeName());
//		printTokenIfAvailable(andChildren.get(3));
//
//		assertEquals("GrammarRules", andChildren.get(4).getPCTypeName());
//
//		PC grammarRules = andChildren.get(4);
//		List<PC> grammarRulesChildren = grammarRules.getPCChildren();
//
//		assertEquals("NFOLD", grammarRulesChildren.get(0).getPCTypeName());
//
//		PC nfold = grammarRulesChildren.get(0);
//		List<PC> nfoldChildren = nfold.getPCChildren();
//	
//		
//		assertEquals("GrammarRule", nfoldChildren.get(0).getPCTypeName());
//
//		PC grammarRule = nfoldChildren.get(0);
//		List<PC> grammarRuleChildren = grammarRule.getPCChildren();
//		
//		assertEquals("AND", grammarRuleChildren.get(0).getPCTypeName());
//		
//		List<PC> andChildren2 = grammarRuleChildren.get(0).getPCChildren();
//
//		assertEquals("NON_TERMINAL", andChildren2.get(0).getPCTypeName());
//		assertEquals("TOKEN", andChildren2.get(1).getPCTypeName());
//		printTokenIfAvailable(andChildren2.get(1));
//		
//		assertEquals("EXPRESSION", andChildren2.get(2).getPCTypeName());
//
//		assertEquals("TOKEN", andChildren2.get(3).getPCTypeName());
//		printTokenIfAvailable(andChildren2.get(3));
//
//		List<PC> nonTerminalChildren = andChildren2.get(0).getPCChildren();
//		
//		assertEquals("TOKEN", nonTerminalChildren.get(0).getPCTypeName());
//		printTokenIfAvailable(nonTerminalChildren.get(0));
//
//		List<PC> expressionChildren = andChildren2.get(2).getPCChildren();
//
//		assertEquals("WITH_OPTIONAL", expressionChildren.get(0).getPCTypeName());
//		
//		// ACHTUNG: IHR WISST, DASS WITH_OPTIONAL 2 Kinder hat -> also die Info ausnutzen
//		// Ich überspringer hier den ersten OR, dann den AND-Knoten.
//		
//		// so seltsam kann Java sein....ich muss es in 2 Zeilen schreiben....
//		List<PC> expressionWithOptionalChildren = expressionChildren.get(0).getPCChildren();
//		expressionWithOptionalChildren = expressionWithOptionalChildren.get(0).getPCChildren();
//		expressionWithOptionalChildren = expressionWithOptionalChildren.get(0).getPCChildren();
//		
//		assertEquals("WITH_OPTIONAL", expressionWithOptionalChildren.get(0).getPCTypeName());
//		assertEquals("NFOLD", expressionWithOptionalChildren.get(1).getPCTypeName());
//		
//		List<PC> expressionWithOptionalWithOptionalChildren = expressionWithOptionalChildren.get(0).getPCChildren();
//		expressionWithOptionalWithOptionalChildren = expressionWithOptionalWithOptionalChildren.get(0).getPCChildren();
//		expressionWithOptionalWithOptionalChildren = expressionWithOptionalWithOptionalChildren.get(0).getPCChildren();
//		
//		assertEquals("NFOLD", expressionWithOptionalWithOptionalChildren.get(0).getPCTypeName());
//		assertEquals("RETURN_CODE", expressionWithOptionalWithOptionalChildren.get(1).getPCTypeName());
//	
//		// and so on and so on....
	}

	private void printTokenIfAvailable(PC pc) {
//		if (new IsTokenParserConstructor().invoke(pc)) {
//			TokenParserConstructor tpc = (TokenParserConstructor) pc;
//			TokenReader tr = tpc.elementScanner;
//			if (new IsConstantTokenReader().invoke(tr)) {
//				System.out.println(new GetConstantTokenAsString().invoke(tr));
//			}
//		}
	}
	

	
}
