package slope.core.grammar;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.SetCmd;
import slope.lib.grammar.GrammarParsing;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.parsetree.Alternative;
import slope.lib.grammar.parsetree.BracketExpression;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.GrammarRule;
import slope.lib.grammar.parsetree.NonTerminal;
import slope.lib.grammar.parsetree.Optional;
import slope.lib.grammar.parsetree.StringTerminal;
import slope.lib.grammar.parsetree.Terminal;

public class GrammarParseTreeTests extends TestCase {
	
	public void test09Grammar() {
		Grammar g = parseGrammar(
				"MyGrammar"
				+ "   Start: MyRule"
				+ "   Rules:"
				+ "     MyRule-> <# StringLiteral #>.");
		
		assertEquals(g.name, "MyGrammar");
		assertEquals(g.startRuleName, "MyRule");
		assertEquals(1, g.grammarRules.size());
	}	
	
	public void test08Grammar() {
		Grammar g = parseGrammar(
				"MyGrammar"
				+ "   Start: MyRule"
				+ "   Rules:"
				+ "     MyRule->a.");
		
		assertEquals(g.name, "MyGrammar");
		assertEquals(g.startRuleName, "MyRule");
		assertEquals(1, g.grammarRules.size());
	}

	public void test07Rule() {
		GrammarRule n;
		n = parseGrammarRule("a -> b c.");
		assertEquals("a", n.ruleName);
		assertTrue(n.expression instanceof Concatenation);
		assertTrue(((Concatenation) n.expression).expressions.get(0) instanceof NonTerminal);
		NonTerminal nt = (NonTerminal) ((Concatenation) n.expression).expressions.get(0);
		assertEquals("b", nt.name);
		
		assertTrue(((Concatenation) n.expression).expressions.get(1) instanceof NonTerminal);
		NonTerminal nt2 = (NonTerminal) ((Concatenation) n.expression).expressions.get(1);
		assertEquals("c", nt2.name);
	}

	public void test08UnaryOperators() {
		Expression n;
		n = (Optional) parseExpression("(abc|def)?");
		Optional o = (Optional) n;
		assertTrue(o.expression instanceof BracketExpression);
		assertEquals("(abc|def)?", n.toString());
	}		
	
	
	public void test07Alternative() {
		Alternative n;
		n = (Alternative) parseExpression("abc | def");
		assertTrue(n.expressions.size()==2);
		assertTrue(n.expressions.get(0) instanceof NonTerminal);
		assertEquals("abc", ((NonTerminal) n.expressions.get(0)).name); 
		assertTrue(n.expressions.get(1) instanceof NonTerminal);
		assertEquals("def", ((NonTerminal) n.expressions.get(1)).name); 

		n = (Alternative) parseExpression("abc | def | hij | klm");
		assertTrue(n.expressions.size()==4);
		assertTrue(n.expressions.get(0) instanceof NonTerminal);
		assertEquals("abc", ((NonTerminal) n.expressions.get(0)).name); 
	
		assertTrue(n.expressions.get(3) instanceof NonTerminal);
		assertEquals("klm", ((NonTerminal) n.expressions.get(3)).name); 
	
	}		

	public void test06Concat() {
		Concatenation n;
		n = (Concatenation) parseConcatenation("abc abc");
		assertTrue(n instanceof Concatenation);
		assertEquals(2, n.expressions.size());
		assertTrue(n.expressions.get(0) instanceof NonTerminal);
		assertTrue(n.expressions.get(1) instanceof NonTerminal);

		n = (Concatenation) parseConcatenation("abc (abc asd)");
		assertTrue(n instanceof Concatenation);
		assertEquals(2, n.expressions.size());
		assertTrue(n.expressions.get(1) instanceof BracketExpression);
	
		n = (Concatenation) parseConcatenation("abc (abc asd) sdfsd");
		assertTrue(n instanceof Concatenation);
		assertEquals(3, n.expressions.size());
		assertTrue(n.expressions.get(0) instanceof NonTerminal);
		assertTrue(n.expressions.get(1) instanceof BracketExpression);
		assertTrue(n.expressions.get(2) instanceof NonTerminal);
	}		
	
	public void test05Expression() {
		Expression n;
		n = parseExpression("abc");
		assertTrue(n instanceof NonTerminal);
		assertEquals("abc", ((NonTerminal) n).name);

		n = parseExpression("\"abc\"");
		assertTrue(n instanceof Terminal);

		n = parseExpression("(afds)");
		assertTrue(n instanceof BracketExpression);
	
	}		
	
	public void test04BracketExpression() {
		BracketExpression n;
		n = parseBracketExpression("(abc)");
		assertTrue(n.expression instanceof NonTerminal);
	}		
	
	public void test03SimpleAtomicExpression() {
		Expression n;
		n = parseATOMIC("\"asd\"");
		assertEquals(StringTerminal.class, n.getClass());
		assertEquals("\"asd\"", ((Terminal) n).value);
	}		

	public void test02Terminal() {
		Terminal n;
		n = parseTerminal("\"asd\"");
		assertEquals("\"asd\"", n.value);
	}		
	
	public void test01NonTerminal() {
		NonTerminal n;
		n = parseNonTerminal("asd");
		assertEquals("asd", n.name);
	}	
	
	private GrammarRule parseGrammarRule(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<GrammarRule> grammarRule = new ResultObject<GrammarRule>() {
			
		};

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().GrammarRule(grammarRule.setter));
		return grammarRule.result;
	}		
	
	
	private Grammar parseGrammar(String s) {
		GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();
		ResultObject<Grammar> grammarResult = new ResultObject<Grammar>() {
			
		};

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new GrammarWithTokensParser().Grammar(grammarResult.setter));
		return grammarResult.result;
	}		
	
	private Expression parseConcatenation(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<Expression> epxression = new ResultObject<Expression>();

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().CONCAT((epxression.setter)));
		return  epxression.result;
	}		
	
	private Expression parseExpression(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<Expression> epxression = new ResultObject<Expression>();

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().EXPRESSION(epxression.setter));
		return epxression.result;
	}	
	
	private BracketExpression parseBracketExpression(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<Expression> bracket = new ResultObject<Expression>();

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().BRACKET_EXPRESSION(bracket.setter));
		return (BracketExpression) bracket.result;
	}

	private Expression parseATOMIC(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<Expression> expr = new ResultObject<Expression>();

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().SIMPLE_ATOMICEXPRESSION(expr.setter));
		return expr.result;
	}	
	
	private Terminal parseTerminal(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<Expression> nonTerminalResult = new ResultObject<Expression>();

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().TERMINAL(nonTerminalResult.setter));
		return (Terminal) nonTerminalResult.result;
	}	
	
	private NonTerminal parseNonTerminal(String s) {
		GrammarParsing tokens = new GrammarParsing();
		ResultObject<Expression> nonTerminalResult = new ResultObject<Expression>();

		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().NON_TERMINAL(nonTerminalResult.setter));
		return (NonTerminal) nonTerminalResult.result;
	}		
}
