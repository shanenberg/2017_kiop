package slope.core.grammar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.RootElementTreeNode;
import slope.TreeNode;
import slope.lang.NFOLD.NFOLDTreeNode;
import slope.lib.SlopeLib;
import slope.lib.grammar.DynamicGrammarParser;
import slope.lib.grammar.DynamicGrammarParserFactory;
import slope.lib.grammar.GrammarParsingWithTokens;
import slope.lib.grammar.parsetree.BuiltInTerminal;
import slope.lib.grammar.parsetree.Concatenation;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.LibraryTerminal;
import slope.lib.grammar.parsetree.MultiplePlus;
import slope.lib.grammar.parsetree.NonTerminal;

public class ExampleTests extends TestCase {

	public final String grammarFolder = System.getProperty("user.dir") + File.separator + "examples" + File.separator;	
	
	
	public void testInit() throws Exception {
		System.out.println("####################################################");
		System.out.println("Testing GrammarFiles");
		for (String grammarFileName : grammarFileNames()) {
try {		
			String grammarString = readGrammarFile(grammarFileName);
			Grammar g = parseGrammar(grammarFileName, grammarString);
			DynamicGrammarParserFactory f = new DynamicGrammarParserFactory();
			DynamicGrammarParser dynamicGrammar = f.createParserFromString(grammarString);

			testDynamicGrammar(grammarFileName, dynamicGrammar);
} catch (Exception ex) {
	System.err.println("FAIL: " + grammarFileName + ".");
	throw ex;
}
		}
		System.out.println("######## GrammarFiles done ##########################");
	}
	private void testDynamicGrammar(String grammarFileName, DynamicGrammarParser dynamicGrammar) {
		File f = new File(grammarFolder + (grammarFileName.substring(0, grammarFileName.length() - ".slopegrammar".length())) + ".slopetests");
		if (!f.exists()) return;
		
		Grammar testingWordsGrammar = SlopeLib.getInstance().loadLib("GT");
		
		ResultObject resultObject = new ResultObject();
		DynamicGrammarParser testWordsParsing = testingWordsGrammar.createParser();
		testWordsParsing.parseWithResultObjectCreation(shapuc.IO.IO.readFileIntoString(f), resultObject.setter);
		
 		MultiplePlus px = (MultiplePlus) resultObject.result;

 		for (Expression ex : px.values) { 			
	 		NonTerminal p2 = (NonTerminal) ex;
	 		Concatenation p3 = (Concatenation) p2.result;
	 		BuiltInTerminal p4 = (BuiltInTerminal) p3.expressions.get(2);
	 		NonTerminal p5 = (NonTerminal) p3.expressions.get(0);
	 		LibraryTerminal p6 = (LibraryTerminal) p5.result;
	 		

	 		System.out.println("     " +p6.value + " : "+ p4.value.substring(1, p4.value.length()-1));
	 		
	 		try {
	 			dynamicGrammar.parse(p4.value.substring(1, p4.value.length()-1));
	 		} catch (Exception exc) {
	 			System.err.println();
	 			System.err.println("failed: " + p4.value.substring(1, p4.value.length()-1));
	 			throw new RuntimeException(exc);
	 		}
	 		
		}
 		
 		
		
		
		
	}

	public Grammar parseGrammar(String grammarName, String grammarString) throws Exception {
		try {
			ResultObject<Grammar> result = new ResultObject<Grammar>();
			GrammarParsingWithTokens tokens = new GrammarParsingWithTokens();

			Parsing<GrammarParsingWithTokens> p = new Parsing<GrammarParsingWithTokens>(tokens, grammarString);
			p.parse(tokens.new GrammarWithTokensParser().Grammar(result.setter));

			System.out.println("DONE: " + grammarName + ".");

			return result.result;
		} catch (Exception ex) {
			System.err.println("FAIL: " + grammarName + ".");
			throw ex;
		}
	}
	
	public ArrayList<String> grammarFileNames() {
		ArrayList<String> ret = new ArrayList<String>();
		
//		String grammarFolder = System.getProperty("user.dir") + File.separator + "examples" + File.separator;		
		File folder = new File(grammarFolder);
		
		File[] listOfFiles = folder.listFiles();
		
		for (File file : listOfFiles) {
		    if (file.isFile() && file.getName().endsWith(".slopegrammar")) {
		    	ret.add(file.getName());
		    }
		}		
		
		Collections.sort(ret, new Comparator<String>() {
			public int compare(String o1, String o2) {return o1.compareTo(o2);}});
		
		return ret;
	}
	
	public String readGrammarFile(String grammarFileName) throws Exception {
		return shapuc.IO.IO.readFileIntoString(grammarFolder + grammarFileName);
	}

}
