package slope.core.grammar;
/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/


import junit.framework.TestCase;
import slope.Cmd;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.*;
import slope.lib.grammar.GrammarParsing.Grammar01Parser;
import slope.lib.grammar.parsetree.Expression;
import slope.lib.grammar.parsetree.Grammar;
import slope.lib.grammar.parsetree.GrammarRule;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

/*
 Rule -> a="a" {^Rule element: a}
 */
public class GrammarParsingTests extends TestCase {
/*
Separators: 
  " ";"\n";"\t"
Keywords:
  "LA"
 */
	public void test05Grammar() {
//		parseGrammar(
//				"Grammar " +
//				  "Tokens: "
//				  "Start: Rules " + 
//				  "Rules: " +
//				  "  GrammarRules -> Rule+." 
//				  "  GrammarRule -> NonTerminal \"->\" Expression."
//				  "  ReturnCode -> \"{\" \"^\" (ObjectReturn | LAReturn) \"}\"." 
//				  + 
//				  "  ObjektReturn -> TypeName (propertyName \":\" localVariable)+." + 
//				  "  LAReturn -> \"LA\" \"(\" identifier \",\" " +
//				  "              identifier \"(\" identifier identifier \")\" \")\"." +
//				  "  Expression -> SimpleExpression+ (ReturnCode)? " + 
//				  "              ((\"|\" (SimpleExpression)+ (ReturnCode)?)*." +
//				  "  SimpleExpression -> (BracketExpression | Terminal | NonTerminal)." + 
//				  "  BracketExpression -> \"(\" Expression \")\"."
//		);
		
	}
	public void test05Terminal() {
		parseTerminal("\"asdasd\"");
	}
	
	
	public void test04Grammar() {
		parseGrammar(
				"MyGrammar "
				+ "   Start: MyRule "
				+ "   Rules: "
				+ "     MyRule->a.");
		
		parseGrammar(
				"MyGrammar "
				+ "Start: MyRule "
				+ "Rules: "
				+ "  MyRule->AnotherRule. "
				+ "  AnotherRule->a.");

		parseGrammar(
				"MyBGrammar "
				+ "Start: Start "
				+ "Rules: "
				+ "  Start-> \"a\" X  \"b\"."
				+ "  X->\"c\" | \"b\" X.");
		
	}	
	
	public void test03Rules() {
		parseRules("a -> a b c.");
		parseRules("SimpleRules -> a. AnotherSimpleRules -> a.");
	}	
	
	public void test02Rule() {
		parseRule("SimpleRules -> a.");
		parseRule("SimpleRules -> a b.");
		parseRule("SimpleRules -> a b c.");
		parseRule("SimpleRules -> (a b)? (c c)+.");
	}	
	
	public void test01SimpleExpression() {
		parseSimpleExpression("a");
		parseSimpleExpression("a?");
		parseSimpleExpression("a*");
		parseSimpleExpression("(a)");
		parseSimpleExpression("(a)?");
		parseSimpleExpression("(a)*");
		parseSimpleExpression("(a)+");
		parseSimpleExpression("\"a\"*");
		
		parseSimpleExpression("(a | b)");
		parseSimpleExpression("(a | b | c)");
		parseSimpleExpression("(a | b | \"c\")");
		
		parseSimpleExpression("(a b | c)");
		parseSimpleExpression("(a | b c)");
		parseSimpleExpression("(a | b c d)");
		parseSimpleExpression("(a | b c d*)");
		parseSimpleExpression("(a | b c d+)+");

		parseSimpleExpression("(a? | b* c d*)?");
	}	

	private void parseGrammar(String s) {
		GrammarParsing tokens = new GrammarParsing();
		
		SetCmd<Grammar> grammar = new SetCmd<Grammar>() {
			public void set(Grammar value) {}};
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parse(tokens.new Grammar01Parser().Grammar(grammar));
	}		
	
	private void parseRules(String s) {
		GrammarParsing tokens = new GrammarParsing();
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		
		SetCmd<GrammarRule> setGrammarRule = new SetCmd<GrammarRule>() {
			public void set(GrammarRule value) {}};
			
		p.parse(tokens.new Grammar01Parser().GrammarRules(setGrammarRule));
	}	
	
	private void parseRule(String s) {
		GrammarParsing tokens = new GrammarParsing();

		SetCmd<GrammarRule> setGrammarRule = new SetCmd<GrammarRule>() {
			public void set(GrammarRule value) {}};
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		p.parse(tokens.new Grammar01Parser().GrammarRule(setGrammarRule));
	}
	
	
	public void parseSimpleExpression(String s) {
		GrammarParsing tokens = new GrammarParsing();
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		ResultObject<Expression> result = new ResultObject<Expression>();
		
		p.parse(tokens.new Grammar01Parser().SIMPLE_EXPRESSION(result.setter));
	}		
	
	private void parseTerminal(String s) {
		GrammarParsing tokens = new GrammarParsing();
		
		@SuppressWarnings("rawtypes")
		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
		ResultObject<Expression> result = new ResultObject<Expression>();
		
		p.parse(tokens.new Grammar01Parser().TERMINAL(result.setter));
	}		
	
//	public void parseGrammar(String s) {
//		GrammarParsing tokens = new GrammarParsing();
//		
//		@SuppressWarnings("rawtypes")
//		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
//		p.parse(tokens.new Grammar01Parser().Grammar());
//	}	
	
//	public Object createGrammar(String s) {
//		GrammarParsing tokens = new GrammarParsing();
//		ResultObject<GrammarRule> result = new ResultObject<GrammarRule>();
//		
//				@SuppressWarnings("rawtypes")
//		Parsing<GrammarParsing> p = new Parsing<GrammarParsing>(tokens, s); 
//		p.parseWithParseTreeConstruction(tokens.new Grammar01Parser().GrammarRule(result.setter));
//		return result.result;
//	}	
	

}
