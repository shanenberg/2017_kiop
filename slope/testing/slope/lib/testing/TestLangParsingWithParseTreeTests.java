/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lib.testing;

import junit.framework.TestCase;
import slope.Parsing;
import slope.ResultObject;
import slope.lib.toyexamples.testlang.BracketExpression;
import slope.lib.toyexamples.testlang.Expression;
import slope.lib.toyexamples.testlang.IntExpression;
import slope.lib.toyexamples.testlang.NFOLDBracketExpression;
import slope.lib.toyexamples.testlang.TestLangParsingWithParseTree;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

public class TestLangParsingWithParseTreeTests extends TestCase {
	
	
	public TokenStream<?> scan(String inputString) {
		Tokens tokens = new TestLangParsingWithParseTree();
		StringBufferStream stringBufferStream = new StringBufferStream(inputString);
		Scanner<?, StringBufferStream> scanner = 
		    new Scanner<Tokens, StringBufferStream>(tokens, stringBufferStream);
		return scanner.scan();		
	}

	public Expression check(String s) {
		final ResultObject<Expression> res = new ResultObject<Expression>();
		TestLangParsingWithParseTree tokens = new TestLangParsingWithParseTree();
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new TestLangParser().EXPRESSION(res.setter));
		return res.result;
	}
	
	public void testParse() {
		  Expression ex = check("Int");
		  assertTrue(ex instanceof IntExpression);
		  
		  ex = check("(Int)");
		  assertTrue( ex instanceof NFOLDBracketExpression);
		  
		  NFOLDBracketExpression nbex = (NFOLDBracketExpression) ex;
		  
		  assertEquals(1,  nbex.bracketExpressions.size());
		  assertTrue(nbex.bracketExpressions.get(0) instanceof BracketExpression);

		  BracketExpression bex = (BracketExpression) nbex.bracketExpressions.get(0);
		  assertTrue(bex.childExpression instanceof IntExpression);
	}

}