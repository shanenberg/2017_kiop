package slope.lib.testing;
/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/


import junit.framework.TestCase;
import slope.Cmd;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.automata.ext.Automata;
import slope.lib.automata.ext.AutomataParsing;
import slope.lib.automata.ext.Transition;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class AutomataParsingTests extends TestCase {

	public Automata createAutomata(String s) {
		AutomataParsing tokens = new AutomataParsing();
		ResultObject<Automata> result = new ResultObject<Automata>();
		
				@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new AutomataParser().START(result.setter));
		return result.result;
	}	
	
	public void test01() {
		Automata a1 = createAutomata(
		  "QAGameAutomata(automataStart)\n"
		  + "  automataStart --->  ε/-/initialize ---> initialized.");
		
		assertEquals("QAGameAutomata", a1.name);
		assertEquals("automataStart", a1.startStateName);
		assertEquals(1, a1.transitions.size());
		assertEquals("automataStart", a1.transitions.get(0).startStateName);
		assertEquals("ε", a1.transitions.get(0).inMsg);
		assertEquals("-", a1.transitions.get(0).conditionMsg);
		assertEquals("initialize", a1.transitions.get(0).outMsg);
		assertEquals("initialized", a1.transitions.get(0).nextStateName);
		
		
		
		createAutomata("QAGameAutomata(automataStart)"
				  + "  automataStart --->  ε/-/initialize ---> initialized,"
				  + "  initialized --->  ε/-/- ---> createNewQuestion,"
				  + "  createNewQuestion --->  ε/-/setNextQuestion ---> askingQuestion.");
		
	}
}
