/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package sloc.testing;

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import sloc.CSVStorageElement;
import sloc.Condition;
import sloc.readers.CSVReaderFactory;
import sloc.readers.CSVReaderFactoryFactory;

public class StorageElementMap extends TestCase {

	public static class test03_result {
		public int mapid;
		public String name;
		public String imageLocation;
		public java.util.Map<String, Integer> elements = new HashMap<String, Integer>();	
	};	
	
	public void test03_INT_STRING_LIST() {

		
		final String input = 
			"mapid:Int		name:String			imageLocation:String		elements:Map(String:Int)\n" + 
			"1	\"Bundesländer\"		\"./Bundesländer.png\"		{(\"Nordrhein-Westfalen\":1),(\"Hessen\":2)}";
		
		CSVReaderFactory factory = CSVReaderFactoryFactory.createStringReaderFactory(input);

		CSVStorageElement<test03_result> se = new CSVStorageElement<test03_result>(factory, test03_result.class);
		List<test03_result> result = se.getAll();
		assertEquals(1, result.size());
		
		// testet, ob auch beim zweiten Aufruf das gleiche Ergebnis rauskommt
		result = se.getAll();
		assertEquals(1, result.size());		
		
		test03_result r1 = se.getFirst(new Condition<test03_result>() {
			public boolean test(test03_result t) {
				return t.mapid == 1;
			}});
		
//		assertEquals(42, r1.id.intValue());
//		assertTrue(r1.aMap.containsKey(1));
//		assertTrue(r1.aMap.get(1).equals(10));
//		assertTrue(r1.aMap.get(2).equals(20));
	}	
	
	
	public static class test02_result {
		  public Integer id;
		  public String name;
		  public Map<Integer, Integer> aMap = new HashMap<Integer, Integer>();
	};	
	
	public void test02_INT_STRING_LIST() {

		
		final String input = 
			"id:Int		aMap:Map(Int:Int)\n" + 
			"42			{(1:10),(2:20)}\n" +
			"43			{(3:3),(4:4)}";
		
		CSVReaderFactory factory = CSVReaderFactoryFactory.createStringReaderFactory(input);

		CSVStorageElement<test02_result> se = new CSVStorageElement<test02_result>(factory, test02_result.class);
		List<test02_result> result = se.getAll();
		assertEquals(2, result.size());
		
		// testet, ob auch beim zweiten Aufruf das gleiche Ergebnis rauskommt
		result = se.getAll();
		assertEquals(2, result.size());		
		
		test02_result r1 = se.getFirst(new Condition<test02_result>() {
			public boolean test(test02_result t) {
				return t.id.intValue()==42;
			}});
		
		assertEquals(42, r1.id.intValue());
		assertTrue(r1.aMap.containsKey(1));
		assertTrue(r1.aMap.get(1).equals(10));
		assertTrue(r1.aMap.get(2).equals(20));
	}	
	
	public static class test01_result {
		  public Integer id;
		  public String name;
		  public List<Integer> aList;
	};	
	
	public void test01_INT_STRING_LIST() {

		
		final String input = 
			"id:Int		name:String		aList:List(Int)\n" + 
	    "42			\"Stefan1\"		{1}\n" +
	    "43			\"Stefan2\"		{2}\n" +
	    "44			\"Stefan3\"		{3}\n";
		
		CSVReaderFactory factory = CSVReaderFactoryFactory.createStringReaderFactory(input);

		CSVStorageElement<test01_result> se = new CSVStorageElement<test01_result>(factory, test01_result.class);
		List<test01_result> result = se.getAll();
		assertEquals(3, result.size());
		
		// testet, ob auch beim zweiten Aufruf das gleiche Ergebnis rauskommt
		result = se.getAll();
		assertEquals(3, result.size());		
		
		test01_result r1 = se.getFirst(new Condition<test01_result>() {
			public boolean test(test01_result t) {
				return t.id.intValue()==42;
			}});
		
		assertEquals(42, r1.id.intValue());
	}
}
